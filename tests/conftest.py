import pytest
import requests
from requests import Response
# from selenium import webdriver
# from selenium.webdriver.chrome.service import Service as ChromeService
import os
import allure
# from pages.main_page import MainPage
# from api.users_api import UsersAPI
# from api.resourse_api import ResourceAPI
# from selenium.webdriver.chrome.options import Options
from pyvirtualdisplay import Display

from tests.pages.article_page import ArticlePage
from tests.pages.authorization_page import AuthorizationPage
from tests.pages.navigation_elements import SideBarElements
from playwright.sync_api import Page, Playwright

if os.environ.get("ENVIRONMENT") != 'local':
    display = Display(visible=False, size=(1920, 1080))
    display.start()


# @pytest.hookimpl(tryfirst=True, hookwrapper=True)
# def pytest_runtest_makereport(item):
#     outcome = yield
#     rep = outcome.get_result()
#     if rep.when == 'call' and rep.failed:
#         try:
#             if 'browser' in item.fixturenames:
#                 browser = item.funcargs['browser']
#             else:
#                 return
#             allure.attach(
#                 browser.get_screenshot_as_png(),
#                 name='Screenshot',
#                 attachment_type=allure.attachment_type.PNG
#             )
#         except Exception as e:
#             print('Fail to take screenshot: {}'.format(e))


# @pytest.fixture(scope="function")
# def browser():
#     print("\nstart chrome browser for test..")
#     options = Options()
#     if os.environ.get("ENVIRONMENT") != 'local':
#         options.page_load_strategy = 'eager'
#         options.add_argument("start-maximized")
#         options.add_argument("disable-infobars")
#         options.add_argument("--disable-extensions")
#         options.add_argument("--disable-gpu")
#         options.add_argument("--disable-dev-shm-usage")
#         options.add_argument("--no-sandbox")
#         options.add_argument('--ignore-certificate-errors')
#         options.binary_location = r'/usr/bin/chromium'
#         browser = webdriver.Chrome(options=options)
#     else:
#         browser = webdriver.Chrome(service=ChromeService(executable_path=os.getcwd() + "/drivers/chromedriver.exe"))
#     browser.maximize_window()
#     yield browser
#     print("\nquit browser..")
#     browser.quit()


@pytest.fixture
def page(playwright: Playwright) -> Page:
    if os.environ.get("ENVIRONMENT") != 'local':
        browser = playwright.chromium.launch(headless=False)
        context = browser.new_context(viewport={'width': 1440, 'height': 900})
    else:
        browser = playwright.chromium.launch(headless=False, args=["--start-maximized"])
        context = browser.new_context(no_viewport=True)
    page = context.new_page()
    yield page
    context.close()
    browser.close()


class ApiClient:

    def __init__(self, base_address) -> None:
        self.base_address: str = base_address

    @allure.step('Making GET request to "{path}"')
    def get_request(self, path="/", params=None, headers=None) -> Response:
        url: str = f"{self.base_address}{path}"
        return requests.get(url=url, params=params, headers=headers)

    @allure.step('Making POST request to "{path}"')
    def post_request(self, path="/", params=None, data=None, json=None, headers=None) -> Response:
        url: str = f"{self.base_address}{path}"
        return requests.post(url=url, params=params, data=data, json=json, headers=headers)

    @allure.step('Making DELETE request to "{path}"')
    def delete_request(self, path="/", params=None, headers=None) -> Response:
        url: str = f"{self.base_address}{path}"
        return requests.delete(url=url, params=params, headers=headers)

    @allure.step('Making PUT request to "{path}"')
    def put_request(self, path="/", params=None, data=None, json=None, headers=None) -> Response:
        url: str = f"{self.base_address}{path}"
        return requests.put(url=url, params=params, data=data, json=json, headers=headers)

    @allure.step('Making PATCH request to "{path}"')
    def patch_request(self, path="/", params=None, data=None, json=None, headers=None) -> Response:
        url: str = f"{self.base_address}{path}"
        return requests.patch(url=url, params=params, data=data, json=json, headers=headers)


@pytest.fixture
def api_reqres() -> ApiClient:
    return ApiClient(base_address="https://ru.crw083.ru")


# @pytest.fixture
# def main_page(browser, api_reqres) -> MainPage:
#     return MainPage(browser, api_reqres)
#
#
# @pytest.fixture
# def users_api(api_reqres) -> UsersAPI:
#     return UsersAPI(api_reqres)
#
#
# @pytest.fixture
# def resource_api(api_reqres) -> ResourceAPI:
#     return ResourceAPI(api_reqres)

# # # # # # # # # #


# @pytest.fixture
# def article(browser) -> ArticlePage:
#     return ArticlePage(browser)
#
#
# @pytest.fixture
# def authorization(browser) -> AuthorizationPage:
#     return AuthorizationPage(browser)
#
#
# @pytest.fixture
# def side_bar(browser) -> SideBarElements:
#     return SideBarElements(browser)

@pytest.fixture
def article(page) -> ArticlePage:
    return ArticlePage(page)


@pytest.fixture
def authorization(page) -> AuthorizationPage:
    return AuthorizationPage(page)


@pytest.fixture
def side_bar(page) -> SideBarElements:
    return SideBarElements(page)


@pytest.fixture
def authorization_data(request):
    request.cls.login_simple_user_1 = "Shadowman"
    request.cls.password_simple_user_1 = "23429987Q"
