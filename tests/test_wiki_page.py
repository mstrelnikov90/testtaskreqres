import allure
import pytest


@pytest.mark.usefixtures("authorization_data")
@allure.feature('Просмотр информации в статьях и истории правок статей')
class TestViewInformationEditHistory:

    @pytest.mark.xdist_group(name="user_tests")
    @allure.title('Переход на вкладку "Читать"')
    def test_move_to_reading_article(self, article, side_bar, authorization):
        """
        Шаги 1-2
        """

        with allure.step("Открытие браузера и заглавной страницы, авторизация под пользователем с ролью user"):
            article.open()
            authorization.authorization(self.login_simple_user_1, self.password_simple_user_1)

        with allure.step("Открытие случайной страницы"):
            side_bar.move_to_random_article()

        with allure.step("Проверка, что открыта вкладка 'Читать', отображается информация по истории"):
            article.assert_that_article_present()

    @pytest.mark.xdist_group(name="user_tests")
    @allure.title('Переход на вкладку "История"')
    def test_move_to_history_article(self, side_bar, article, authorization):
        """
        Шаг 3
        """

        with allure.step("Открытие браузера и заглавной страницы, авторизация под пользователем с ролью user"):
            article.open()
            authorization.authorization(self.login_simple_user_1, self.password_simple_user_1)

        with allure.step("Открытие случайной страницы, переход на вкладку 'История'"):
            side_bar.move_to_random_article()
            article.move_to_top_article_links('История')

        with allure.step("Проверка, что открыта вкладка 'История', отображается информация по истории"):
            article.assert_that_history_article_present()
