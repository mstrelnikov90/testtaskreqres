import time
import allure
from typing import NoReturn
from selenium.webdriver.common.action_chains import ActionChains
from .base_page import BasePage
from .locators import (TopBarLocators, AuthorizationPageLocators, CreateAccountLocators, ArticlePageLocators)


class AuthorizationPage(BasePage):
    # def __init__(self, browser) -> None:
    #     self.action = ActionChains(browser)
    #     page_url: str = "index.php?title=Служебная:Вход&returnto=Википедия%3AСодержание"
    #     super().__init__(browser, "/w/" + page_url)

    def __init__(self, page):
        page_url: str = "index.php?title=Служебная:Вход&returnto=Википедия%3AСодержание"
        super().__init__(page, "/w/" + page_url)

    @allure.step("Авторизация под пользователем {login}")
    def authorization(self, login: str, password: str) -> NoReturn:
        self.create_screenshot()
        # if self.is_element_present(ArticlePageLocators.WELCOME_POPUP):
        #     time.sleep(2)
        #     self.waiting_element_and_click(ArticlePageLocators.CLOSE_WELCOME_CREATE_POPUP_BUTTON)
        time.sleep(2)
        self.waiting_element_and_click(TopBarLocators.SIGN_IN_BUTTON)
        time.sleep(2)
        self.scroll_on_page(-600)
        # self.clear_element(AuthorizationPageLocators.LOGIN_INPUT)
        self.element_send_keys(AuthorizationPageLocators.LOGIN_INPUT, login)
        self.element_send_keys(AuthorizationPageLocators.PASSWORD_INPUT, password)
        self.waiting_element_and_click(AuthorizationPageLocators.SUBMIT_BUTTON)
        time.sleep(2)
        self.scroll_on_page(-300)
        if self.is_element_present(AuthorizationPageLocators.SKIP_BUTTON):
            self.waiting_element_and_click(AuthorizationPageLocators.SKIP_BUTTON)
        self.create_screenshot()

    @allure.step("Создание аккаунта")
    def create_account(self, account_name, account_pass):
        self.create_attachment(f"Login: {account_name}, Password: {account_pass}", "Данные для авторизации")
        self.element_send_keys(CreateAccountLocators.ACCOUNT_NAME_INPUT, account_name)
        self.element_send_keys(CreateAccountLocators.PASSWORD_INPUT, account_pass)
        self.element_send_keys(CreateAccountLocators.CONFIRM_PASSWORD_INPUT, account_pass)
        self.scroll_to_element(CreateAccountLocators.CONFIRM_PASSWORD_INPUT)
        time.sleep(2)
        self.scroll_to_element(CreateAccountLocators.AGREE_POLITICS_CHECKBOX)
        self.move_to_element_and_click(CreateAccountLocators.AGREE_POLITICS_CHECKBOX)
        self.move_to_element_and_click(CreateAccountLocators.AGREE_PERSONAL_DATA_CHECKBOX)
        self.move_to_element_and_click(CreateAccountLocators.AGREE_NO_PROHIBITIONS_CHECKBOX)
        self.create_screenshot()
        self.waiting_element_and_click(CreateAccountLocators.CONFIRM_CREATE_ACCOUNT_BUTTON)
        self.create_screenshot()

    def assert_that_account_created(self, account_name):
        self.assert_that_element_present(ArticlePageLocators.WELCOME_ARTICLE_TITLE_AFTER_CREATE_ACCOUNT, f"Страница Добро пожаловать, {account_name}")

    @allure.step("Авторизация под пользователем {login} после смены пароля")
    def authorization_after_edit_password(self, login: str, password: str) -> NoReturn:
        if self.is_element_present(AuthorizationPageLocators.SIGN_IN_TITLE):
            self.clear_element(AuthorizationPageLocators.LOGIN_INPUT)
            self.element_send_keys(AuthorizationPageLocators.LOGIN_INPUT, login)
            self.element_send_keys(AuthorizationPageLocators.PASSWORD_INPUT, password)
            self.waiting_element_and_click(AuthorizationPageLocators.SUBMIT_BUTTON)
            self.create_screenshot()

    def assert_that_authorization_completed(self):
        """ Проверка, что авторизация выполнена успешно """
        self.assert_that_element_present(TopBarLocators.ACCOUNT_NAME, "Имя пользователя в верхней панели")
        self.assert_that_element_present(TopBarLocators.ALERTS, "Кнопка 'Оповещения' в верхней панели")
        self.assert_that_element_present(TopBarLocators.NOTIFICATIONS, "Кнопка 'Уведомления' в верхней панели")
        self.waiting_element_and_click(TopBarLocators.ACCOUNT_NAME)
        self.assert_that_element_present(TopBarLocators.WATCHLIST, "Кнопка 'Список наблюдения' в верхней панели")

    def assert_that_user_not_authorized(self):
        """ Проверка, что пользователь не авторизован """
        time.sleep(2)
        self.assert_that_element_present(TopBarLocators.CREATE_ACCOUNT, "Кнопка 'Создать УЗ'")
        self.assert_that_element_present(TopBarLocators.SIGN_IN_BUTTON, "Кнопка 'Войти'")
