import time
import allure
from typing import NoReturn
import pytest
from requests import Response
from selenium.webdriver import Keys
# from selenium.webdriver.common.action_chains import ActionChains
from .base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from .locators import (ArticlePageLocators, TopBarLocators, NewPagesLocators, HistoryArticleLocators,
                       DiscussionPageLocators, WatchListLocators, AlertsPageLocators,
                       UploadFileArticleLocators, ProtectPageLocators,
                       SearchResultsLocators, DeleteUndeleteArticleLocators, SideBarLocators, CodeEditorArticleLocators,
                       VisualEditorArticleLocators, RenamePageLocators, MainPageLocators, CategoryPageLocators,
                       RecentChangesLocators, BlockUserLocators)


class ArticlePage(BasePage):
    # def __init__(self, browser) -> None:
    #     self.action = ActionChains(browser)
    #     page_url: str = "Заглавная_страница"
    #     super().__init__(browser, BasePage.endpoint + page_url)

    def __init__(self, page):
        page_url: str = "Заглавная_страница"
        super().__init__(page, BasePage.endpoint + page_url)

    @allure.step("Переход на вкладку {link} в верхней части статьи")
    def move_to_top_article_links(self, link):
        if link == 'Править':
            self.waiting_element_and_click(ArticlePageLocators.EDIT_ARTICLE)
            time.sleep(15)
            if not self.is_element_present(VisualEditorArticleLocators.TEXT_INPUT_ALL_ARTICLE):
                self.waiting_element_and_click(ArticlePageLocators.EDIT_ARTICLE)
        elif link == 'Править код':
            self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
            time.sleep(2)
            if not self.is_element_present(ArticlePageLocators.EDIT_CODE_ARTICLE):
                self.create_screenshot()
                self.move_to_element_and_click(SideBarLocators.INSTRUMENTS)
                time.sleep(2)
            self.waiting_element_and_click(ArticlePageLocators.EDIT_CODE_ARTICLE)
            time.sleep(7)
        elif link == 'История':
            self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
            time.sleep(2)
            if not self.is_element_present(ArticlePageLocators.HISTORY_ARTICLE):
                self.create_screenshot()
                self.move_to_element_and_click(SideBarLocators.INSTRUMENTS)
                time.sleep(2)
            self.waiting_element_and_click(ArticlePageLocators.HISTORY_ARTICLE)
            time.sleep(1)
        elif link == 'Обсуждение':
            self.waiting_element_and_click(ArticlePageLocators.TALK_ARTICLE)
        elif link == 'Читать':
            self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
            time.sleep(2)
            if not self.is_element_present(ArticlePageLocators.READ_ARTICLE):
                self.create_screenshot()
                self.move_to_element_and_click(SideBarLocators.INSTRUMENTS)
                time.sleep(2)
            self.waiting_element_and_click(ArticlePageLocators.READ_ARTICLE)
            time.sleep(2)
        elif link == 'Статья':
            self.waiting_element_and_click(ArticlePageLocators.ARTICLE_BUTTON)
            time.sleep(2)
        elif link == 'Список наблюдения':
            self.waiting_element_and_click(ArticlePageLocators.ADD_TO_WATCHLIST_BUTTON)
            time.sleep(2)
        elif link == 'Участник':
            self.waiting_element_and_click(ArticlePageLocators.MEMBER_BUTTON)
            time.sleep(2)
        elif link == 'Создать':
            self.scroll_on_page(-500)
            self.waiting_element_and_click(ArticlePageLocators.CREATE_ARTICLE)
            time.sleep(15)
        elif link == 'Посмотреть на Викискладе':
            self.waiting_element_and_click(ArticlePageLocators.VIEW_ON_COMMONS_BUTTON)
        self.create_screenshot()

    @allure.step("Переход на случайную незащищенную статью, защита страницы")
    def protect_page(self, environment) -> NoReturn:
        timer = 0
        while self.is_element_present(SideBarLocators.UNPROTECT_ARTICLE):
            self.open_page('/wiki/Служебная:Случайная_страница', environment)
            time.sleep(5)
            timer += 5
            if timer > 30:
                pytest.skip("Незащищенных статей не обнаружено")
                break
        self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
        time.sleep(2)
        self.waiting_element_and_click(SideBarLocators.PROTECT_ARTICLE)
        time.sleep(2)
        if self.is_element_present(ProtectPageLocators.OPEN_LIST_ROLES_EDITING):
            self.scroll_to_element(ProtectPageLocators.OPEN_LIST_ROLES_EDITING)
        else:
            self.scroll_on_page(400)
        self.waiting_element_and_click(ProtectPageLocators.OPEN_LIST_ROLES_EDITING)
        self.waiting_element_and_click(ProtectPageLocators.SELECT_ONLY_ADMIN_ROLE_EDITING)
        self.waiting_element_and_click(ProtectPageLocators.OPEN_LIST_DATE_EDITING)
        self.waiting_element_and_click(ProtectPageLocators.SELECT_1_HOUR_DATE_EDITING)
        time.sleep(2)
        self.move_to_element_and_click(ProtectPageLocators.OPEN_RENAMING_CHECKBOX)
        self.scroll_to_element(ProtectPageLocators.OPEN_LIST_ROLES_RENAMING)
        self.waiting_element_and_click(ProtectPageLocators.OPEN_LIST_ROLES_RENAMING)
        self.waiting_element_and_click(ProtectPageLocators.SELECT_ONLY_ADMIN_ROLE_RENAMING)
        self.waiting_element_and_click(ProtectPageLocators.OPEN_LIST_DATE_RENAMING)
        self.waiting_element_and_click(ProtectPageLocators.SELECT_1_HOUR_DATE_RENAMING)
        self.scroll_to_element(ProtectPageLocators.SUBMIT_BUTTON)
        self.waiting_element_and_click(ProtectPageLocators.SUBMIT_BUTTON)
        self.create_screenshot()

    def assert_that_protected_page(self) -> NoReturn:
        self.scroll_to_element(HistoryArticleLocators.PROTECTED_TEXT)
        self.assert_that_element_present(HistoryArticleLocators.PROTECTED_TEXT, "Текст об установке защиты")
        if self.is_element_present(ArticlePageLocators.TEXT_ABOUT_AUTOREVIEW):
            self.assert_that_element_present(ArticlePageLocators.TEXT_ABOUT_AUTOREVIEW, "Текст об отпатрулированной правке")
        else:
            timer = 0
            while not self.is_element_present(ArticlePageLocators.TEXT_ABOUT_AUTOREVIEW):
                time.sleep(20)
                self.reload()
                timer += 20
                if timer > 600:
                    break
            self.assert_that_element_present(ArticlePageLocators.TEXT_ABOUT_AUTOREVIEW, "Текст об отпатрулированной правке")

    def assert_that_page_marks_as_autoreview(self) -> NoReturn:
        self.scroll_to_element(ArticlePageLocators.BLOCK_CORRECTION_ON_PAGE_HISTORY)
        timer = 0
        while not self.is_element_present(ArticlePageLocators.TEXT_ABOUT_AUTOREVIEW):
            time.sleep(10)
            self.reload()
            timer += 10
            if timer > 60:
                break
        time.sleep(2)
        self.create_attachment(f"Время ожидания автопатрулирования: {timer}", "Время ожидания автопатрулирования")
        self.assert_that_element_present(ArticlePageLocators.TEXT_ABOUT_AUTOREVIEW, "Текст об отпатрулированной правке")
        self.assert_that_element_present(ArticlePageLocators.USER_IN_CORRECTION_AUTOREVIEW, "Текст о наименовании пользователя, сделавшего автоматическую правку")

    def assert_that_page_marks_as_cascade_protection(self) -> NoReturn:
        self.scroll_to_element(ArticlePageLocators.BLOCK_CORRECTION_ON_PAGE_HISTORY)
        timer = 0
        while not self.is_element_present(ArticlePageLocators.TEXT_ABOUT_AUTOREVIEW):
            time.sleep(20)
            self.reload()
            timer += 20
            if timer > 600:
                break
        self.assert_that_element_present(ArticlePageLocators.TEXT_ABOUT_AUTOREVIEW, "Текст об отпатрулированной правке")
        self.assert_that_element_present(ArticlePageLocators.USER_IN_CASCADE_PROTECTION, "Текст о наименовании пользователя, сделавшего каскадную защиту")
        self.assert_that_element_present(ArticlePageLocators.TEXT_ABOUT_CASCADE_PROTECT, "Текст о наименовании каскадной защиты")

    def assert_that_change_for_fast_rollback(self) -> NoReturn:
        self.scroll_to_element(ArticlePageLocators.BLOCK_CORRECTION_ON_PAGE_HISTORY)
        self.assert_that_element_present(ArticlePageLocators.TEXT_FOR_WAIT_CHECKING, "Текст о том, что правка ожидает проверки")
        self.assert_that_element_present(ArticlePageLocators.USER_IN_CHANGE_FOR_FAST_ROLLBACK, "Текст о наименовании пользователя, сделавшего правку")

    @allure.step("Выполнить быстрый откат правки последнего участника")
    def delete_fast_rollback_by_last_user(self) -> NoReturn:
        self.scroll_to_element(ArticlePageLocators.BLOCK_CORRECTION_ON_PAGE_HISTORY)
        self.waiting_element_and_click(ArticlePageLocators.TEXT_FOR_FAST_ROLLBACK)
        self.create_screenshot()

    def assert_that_page_cannot_edited(self) -> NoReturn:
        time.sleep(2)
        self.assert_that_element_not_present(ArticlePageLocators.EDIT_CODE_ARTICLE, "Кнопка 'Править код'")
        self.assert_that_element_not_present(ArticlePageLocators.EDIT_ARTICLE, "Кнопка 'Править'")

    @allure.step("Переименовать защищенную страницу")
    def rename_protected_page(self, new_title_name: str) -> NoReturn:
        self.clear_element(RenamePageLocators.NEW_NAME_INPUT)
        self.element_send_keys(RenamePageLocators.NEW_NAME_INPUT, new_title_name)
        self.scroll_on_page(400)
        self.scroll_to_element(RenamePageLocators.CONFIRM_RENAME_BUTTON)
        self.waiting_element_and_click(RenamePageLocators.CONFIRM_RENAME_BUTTON)
        self.create_screenshot()

    @allure.step("Переименование статьи")
    def rename_article(self, new_name, reason, redirect: bool = False):
        self.scroll_on_page(200)
        time.sleep(1)
        self.create_screenshot()
        self.scroll_to_element(RenamePageLocators.NEW_NAME_INPUT)
        time.sleep(1)
        self.create_screenshot()
        self.clear_element(RenamePageLocators.NEW_NAME_INPUT)
        self.element_send_keys(RenamePageLocators.NEW_NAME_INPUT, new_name)
        self.create_screenshot()
        self.element_send_keys(RenamePageLocators.OTHER_REASON_INPUT, reason)
        self.create_screenshot()
        if redirect:
            if not self.is_element_present(RenamePageLocators.LEAVE_REDIRECT_CHECKED_CHECKBOX):
                self.scroll_to_element(RenamePageLocators.LEAVE_REDIRECT_CHECKBOX)
                self.move_to_element_and_click(RenamePageLocators.LEAVE_REDIRECT_CHECKBOX)
        else:
            if self.is_element_present(RenamePageLocators.LEAVE_REDIRECT_CHECKED_CHECKBOX):
                self.scroll_to_element(RenamePageLocators.LEAVE_REDIRECT_CHECKBOX)
                self.move_to_element_and_click(RenamePageLocators.LEAVE_REDIRECT_CHECKBOX)
        self.create_screenshot()
        self.scroll_to_element(RenamePageLocators.ADD_TITLE_TO_WATCHLIST_CHECKBOX)
        self.move_to_element_and_click(RenamePageLocators.ADD_TITLE_TO_WATCHLIST_CHECKBOX)
        self.create_screenshot()
        self.scroll_to_element(RenamePageLocators.CONFIRM_RENAME_BUTTON)
        time.sleep(1)
        self.create_screenshot()
        self.waiting_element_and_click(RenamePageLocators.CONFIRM_RENAME_BUTTON)
        self.create_screenshot()

    @allure.step("Снять защиту")
    def unprotect_page(self, type_protect="") -> NoReturn:
        if self.is_element_present(SearchResultsLocators.SEARCH_RESULTS_PAGE_TITLE):
            self.waiting_element_and_click(SearchResultsLocators.FIRST_LINK_AFTER_DISPLAY_RESULTS)
            time.sleep(2)
        try:
            if self.is_element_present(SideBarLocators.INSTRUMENTS):
                self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
        except TimeoutException:
            pass
        if type_protect == "cascade":
            self.waiting_element_and_click(SideBarLocators.CHANGE_PROTECT_SETTINGS)
            self.scroll_to_element(ProtectPageLocators.CHECKBOX_CASCADE_PROTECT)
            self.waiting_element_and_click(ProtectPageLocators.CHECKBOX_CASCADE_PROTECT)
            self.create_screenshot()
            self.waiting_element_and_click(ProtectPageLocators.SUBMIT_BUTTON)
        else:
            self.waiting_element_and_click(SideBarLocators.UNPROTECT_ARTICLE)
            self.scroll_to_element(ProtectPageLocators.OPEN_LIST_ROLES_EDITING)
            self.waiting_element_and_click(ProtectPageLocators.OPEN_LIST_ROLES_EDITING)
            self.waiting_element_and_click(ProtectPageLocators.SELECT_WITHOUT_PROTECT_EDITING)
            time.sleep(2)
            self.scroll_to_element(ProtectPageLocators.OPEN_RENAMING_CHECKBOX)
            self.move_to_element_and_click(ProtectPageLocators.OPEN_RENAMING_CHECKBOX)
            self.waiting_element_and_click(ProtectPageLocators.OPEN_LIST_ROLES_RENAMING)
            self.waiting_element_and_click(ProtectPageLocators.SELECT_WITHOUT_PROTECT_RENAMING)
            self.scroll_to_element(ProtectPageLocators.SUBMIT_BUTTON)
            self.waiting_element_and_click(ProtectPageLocators.SUBMIT_BUTTON)
            self.create_screenshot()

    @allure.step("Изменить защиту - установить каскадную защиту")
    def change_protect_page(self, type_protect: str = "") -> NoReturn:
        self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
        self.waiting_element_and_click(SideBarLocators.PROTECT_ARTICLE)
        if type_protect == "cascade":
            self.scroll_to_element(ProtectPageLocators.CHECKBOX_CASCADE_PROTECT)
            time.sleep(2)
            self.move_to_element_and_click(ProtectPageLocators.CHECKBOX_CASCADE_PROTECT)
            self.create_screenshot()
            self.waiting_element_and_click(ProtectPageLocators.SUBMIT_BUTTON)
            time.sleep(5)
        self.create_screenshot()

    def assert_that_unprotected_page(self) -> NoReturn:
        self.assert_that_element_present(HistoryArticleLocators.UNPROTECTED_TEXT, "Текст о снятии защиты")

    @allure.step("Сохранение названия статьи")
    def title_article(self) -> str:
        if self.is_element_present(ArticlePageLocators.CREATE_ARTICLE_TITLE):
            time.sleep(10)
        title_string = ""
        if self.is_element_present(ArticlePageLocators.TITLE_ARTICLE_1):
            title = self.get_text_from_element(ArticlePageLocators.TITLE_ARTICLE_1)
        elif self.is_element_present(ArticlePageLocators.TITLE_ARTICLE_2):
            title = self.get_text_from_element(ArticlePageLocators.TITLE_ARTICLE_2)
        else:
            title = self.get_text_from_element(ArticlePageLocators.TITLE_ARTICLE_3)
        if title != "":
            self.create_attachment(title, "Название статьи")
        if "?" in title:
            title_array = title.split("?")
            for i in title_array:
                title_string += f"{i}%3F"
            title = title_string[:-3]
        if title == "":
            time.sleep(10)
            if self.is_element_present(ArticlePageLocators.CREATE_ARTICLE_TITLE):
                time.sleep(10)
            title_string = ""
            if self.is_element_present(ArticlePageLocators.TITLE_ARTICLE_1):
                title = self.get_text_from_element(ArticlePageLocators.TITLE_ARTICLE_1)
            elif self.is_element_present(ArticlePageLocators.TITLE_ARTICLE_2):
                title = self.get_text_from_element(ArticlePageLocators.TITLE_ARTICLE_2)
            else:
                title = self.get_text_from_element(ArticlePageLocators.TITLE_ARTICLE_3)
            self.create_attachment(title, "Название статьи")
            if "?" in title:
                title_array = title.split("?")
                for i in title_array:
                    title_string += f"{i}%3F"
                title = title_string[:-3]
        return title

    @allure.step("Переход на статью из категории")
    def move_to_random_article_from_category(self):
        self.waiting_element_and_click(CategoryPageLocators.MOVE_TO_FIRST_ARTICLE_FROM_CATEGORY_PAGE_LINK)
        self.create_screenshot()

    @allure.step("Проверка, что открыта указанная страница")
    def assert_that_title_article_present(self, page_title: str | None):
        """ Проверка, что открыта указанная страница """
        current_page_title = self.title_article()
        self.create_attachment(current_page_title, "Название текущей страницы")
        self.create_attachment(page_title, "Название указанной страницы")
        self.create_screenshot()
        assert current_page_title == page_title, f"Страница {page_title} не открыта"
        self.create_screenshot()

    @allure.step("Переход на переименованную статью")
    def move_to_renamed_article(self):
        self.waiting_element_and_click(RenamePageLocators.OLD_LINK_ARTICLE)
        self.waiting_element_and_click(RenamePageLocators.REDIRECT_LINK)
        self.create_screenshot()

    def assert_that_page_renamed(self) -> NoReturn:
        """ Проверка, что отображается текст о переименовании страницы """
        self.assert_that_element_present(RenamePageLocators.RENAMED_TITLE_TEXT, "Уведомление о переименовании страницы")
        self.assert_that_element_present(RenamePageLocators.OLD_LINK_ARTICLE, "Старая ссылка на статью")

    def assert_that_renamed_page_is_present(self) -> NoReturn:
        """ Проверка, что переименованная страница отображается """
        current_article_title = self.title_article()
        assert "Renamed" in current_article_title, "Название не отредактировано"
        self.create_screenshot()

    @allure.step("Переход на следующую случайную страницу, если на предыдущей было создано обсуждение")
    def move_on_random_article_without_discuss(self, env: str = 'auto', wiki: str = None) -> NoReturn:
        timer = 0
        while not self.is_element_present(DiscussionPageLocators.START_DISCUSSION):
            # url = f"https://{env}-ru-7a504.pr22.inno.tech"
            url = f"https://{env}-ru-.pr22.inno.tech"
            if env == 'dev':
                url = f"https://{env}-ru-107d3.pr22.inno.tech"
            elif env == 'preprod':
                url = "https://ru.crw083.ru"
            elif env == 'farm':
                url = f'https://{wiki}.crw083.ru'
            self.browser.get(f"{url}/wiki/Служебная:Случайная_страница")
            time.sleep(5)
            self.waiting_element_and_click(ArticlePageLocators.TALK_ARTICLE)
            timer += 5
            if timer > 30:
                break
        self.waiting_element_and_click(ArticlePageLocators.ARTICLE_BUTTON)
        time.sleep(2)
        self.create_screenshot()

    @allure.step("Переход к секции 'Рекомендации для вас'")
    def move_to_recommendations_section(self):
        self.scroll_to_element(MainPageLocators.RECOMMENDATIONS_SECTION)
        self.waiting_element_and_click(MainPageLocators.RECOMMENDATIONS_SECTION)
        self.create_screenshot()

    @allure.step("Переход к секции 'Смотрите также'")
    def move_to_recommendations_section_from_article(self):
        self.scroll_to_element(ArticlePageLocators.SEE_ALSO_SECTION)
        self.waiting_element_and_click(ArticlePageLocators.SEE_ALSO_SECTION)
        self.create_screenshot()

    @allure.step("Переход на страницу виджета")
    def move_to_recommendations_widget(self):
        self.waiting_element_and_click(MainPageLocators.FIRST_WIDGET_FROM_RECOMMENDATIONS)
        self.create_screenshot()

    @allure.step("Переход на страницу виджета из секции 'Смотрите также'")
    def move_to_recommendations_widget_from_see_also_section(self):
        self.scroll_to_element(ArticlePageLocators.FIRST_WIDGET_FROM_SEE_ALSO_SECTION)
        self.waiting_element_and_click(ArticlePageLocators.FIRST_WIDGET_FROM_SEE_ALSO_SECTION)
        self.create_screenshot()

    @allure.step("Сохранение названия первого виджета рекомендаций")
    def get_name_of_first_recommendation_widget(self):
        widget_title = self.get_attribute_from_element(MainPageLocators.FIRST_WIDGET_FROM_RECOMMENDATIONS, 'title')
        if widget_title == "":
            widget_title = self.get_text_from_element(MainPageLocators.FIRST_WIDGET_FROM_RECOMMENDATIONS_TITLE)
        self.create_attachment(widget_title, "Название первого виджета рекомемндаций")
        self.create_screenshot()
        return widget_title

    @allure.step("Отметка статьи 'Не показывать'")
    def mark_article_as_dont_show(self):
        self.waiting_element_and_click(MainPageLocators.DONT_SHOW_AGAIN_BUTTON)
        self.waiting_element_and_click(MainPageLocators.CONFIRM_SHOW_AGAIN_BUTTON)
        self.create_screenshot()

    @allure.step("Получение текста из статьи")
    def get_text_from_article(self) -> str:
        if self.is_element_present(ArticlePageLocators.FIRST_SECTION_SECOND_BLOCK_TEXT_ARTICLE):
            text = self.get_text_from_element(ArticlePageLocators.FIRST_SECTION_SECOND_BLOCK_TEXT_ARTICLE)
        elif self.is_element_present(ArticlePageLocators.SECOND_SECTION_TEXT_ARTICLE):
            text = self.get_text_from_element(ArticlePageLocators.SECOND_SECTION_TEXT_ARTICLE)
        elif self.is_element_present(ArticlePageLocators.FIRST_SECTION_TEXT_ARTICLE):
            text = self.get_text_from_element(ArticlePageLocators.FIRST_SECTION_TEXT_ARTICLE)
        else:
            text = ""
        if len(text) > 70:
            part_text = text.split()
            if len(part_text) > 14:
                part_text = part_text[:14]
            elif len(part_text) > 8:
                part_text = part_text[:8]
            else:
                part_text = part_text[:5]
            text = " ".join(part_text)
        elif len(text) < 10 and text != "":
            text = self.get_text_from_element(ArticlePageLocators.THIRD_SECTION_TEXT_ARTICLE)
            if len(text) > 70:
                part_text = text.split()
                if len(part_text) > 14:
                    part_text = part_text[:14]
                elif len(part_text) > 8:
                    part_text = part_text[:8]
                else:
                    part_text = part_text[:5]
                text = " ".join(part_text)
        while '[1]' in text:
            index = text.find('[1]')
            first_part = text[:index]
            last_part = text[index + 3:]
            text = first_part + last_part
        while '[2]' in text:
            index = text.find('[2]')
            first_part = text[:index]
            last_part = text[index + 3:]
            text = first_part + last_part
        while '[3]' in text:
            index = text.find('[3]')
            first_part = text[:index]
            last_part = text[index + 3:]
            text = first_part + last_part
        while '[4]' in text:
            index = text.find('[4]')
            first_part = text[:index]
            last_part = text[index + 3:]
            text = first_part + last_part
        self.create_attachment(text, "Полученный текст")
        return text

    @allure.step("Получение всего текста из статьи")
    def get_all_text_from_article(self):
        text = self.get_text_from_element(ArticlePageLocators.FIRST_SECTION_TEXT_ARTICLE)
        self.create_attachment(text, "Полученный текст")
        return text

    @allure.step("Создание обсуждения")
    def create_discuss(self) -> NoReturn:
        self.scroll_to_element(DiscussionPageLocators.START_DISCUSSION)
        self.waiting_element_and_click(DiscussionPageLocators.START_DISCUSSION)
        time.sleep(10)
        self.create_screenshot()

    @allure.step("Добавление темы и описания")
    def add_theme_and_description(self, theme, description) -> NoReturn:
        if self.is_element_present(DiscussionPageLocators.THEME_DISCUSSION_INPUT_NEW):
            self.element_send_keys(DiscussionPageLocators.THEME_DISCUSSION_INPUT_NEW, theme)
        else:
            self.element_send_keys(DiscussionPageLocators.THEME_DISCUSSION_INPUT_OLD, theme)
        time.sleep(5)
        timer = 0
        while self.is_element_present(DiscussionPageLocators.DESCRIPTION_INPUT):
            time.sleep(10)
            timer += 10
            if timer > 30:
                break
        self.element_send_keys(DiscussionPageLocators.DESCRIPTION_INPUT, description)
        self.create_screenshot()
        time.sleep(5)
        self.scroll_to_element(DiscussionPageLocators.CONFIRM_ADDING_THEME)
        self.scroll_on_page(100)
        self.waiting_element_and_click(DiscussionPageLocators.CONFIRM_ADDING_THEME)
        time.sleep(10)
        self.create_screenshot()

    @allure.step("Создание благодарности на вкладке 'История'")
    def move_to_changes_and_gratitude(self):
        self.scroll_to_element(HistoryArticleLocators.GRATITUDE_CHANGES_SIMPLE_USER)
        self.waiting_element_and_click(HistoryArticleLocators.GRATITUDE_CHANGES_SIMPLE_USER)
        time.sleep(2)
        if self.is_element_present(ArticlePageLocators.VISUAL_EDITOR_ARTICLE_TITLE):
            self.back_to_page()
            time.sleep(1)
            self.scroll_to_element(HistoryArticleLocators.GRATITUDE_CHANGES_SIMPLE_USER)
            self.move_to_element_and_click(HistoryArticleLocators.GRATITUDE_CHANGES_SIMPLE_USER)
        self.waiting_element_and_click(HistoryArticleLocators.CONFIRM_PUBLIC_GRATITUDE)
        time.sleep(2)
        if not self.is_element_present(HistoryArticleLocators.GRATEFUL_SIMPLE_USER):
            self.waiting_element_and_click(HistoryArticleLocators.CONFIRM_PUBLIC_GRATITUDE)
        self.create_screenshot()

    @allure.step("Переход к последней созданной статье")
    def move_to_last_created_article(self):
        self.scroll_to_element(NewPagesLocators.MOVE_TO_CREATED_AUTOTEST_ARTICLE)
        self.waiting_element_and_click(NewPagesLocators.MOVE_TO_CREATED_AUTOTEST_ARTICLE)
        self.create_screenshot()

    @allure.step("Редактирование обсуждения")
    def edit_discuss(self, description, separator) -> NoReturn:
        self.create_screenshot()
        self.scroll_on_page(-600)
        self.create_screenshot()
        time.sleep(1)
        self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
        time.sleep(1)
        self.scroll_to_element(ArticlePageLocators.EDIT_CODE_ARTICLE)
        self.waiting_element_and_click(ArticlePageLocators.EDIT_CODE_ARTICLE)
        time.sleep(2)
        self.scroll_to_element(ArticlePageLocators.ADD_TEXT_EDIT_CODE_CREATE_ARTICLE_TEXTAREA)
        text = self.get_text_from_element(ArticlePageLocators.ADD_TEXT_EDIT_CODE_CREATE_ARTICLE_TEXTAREA)
        self.clear_element(ArticlePageLocators.ADD_TEXT_EDIT_CODE_CREATE_ARTICLE_TEXTAREA)
        index_end_of_description: int = text.rfind(description) + len(description)
        first_part_text = text[:index_end_of_description]
        second_part_text = text[index_end_of_description:]
        text = first_part_text + separator + second_part_text
        self.element_send_keys(ArticlePageLocators.ADD_TEXT_EDIT_CODE_CREATE_ARTICLE_TEXTAREA, text)
        self.scroll_to_element(CodeEditorArticleLocators.DESCRIPTION_EDIT_CODE_ARTICLE_INPUT)
        self.element_send_keys(CodeEditorArticleLocators.DESCRIPTION_EDIT_CODE_ARTICLE_INPUT, "autotest-description")
        self.scroll_to_element(CodeEditorArticleLocators.SAVE_EDIT_CODE_ARTICLE_INPUT)
        self.waiting_element_and_click(CodeEditorArticleLocators.SAVE_EDIT_CODE_ARTICLE_INPUT)
        self.create_screenshot()

    @allure.step("Ответить в обсуждении")
    def answer_to_discuss(self, text) -> NoReturn:
        self.scroll_to_element(DiscussionPageLocators.ANSWER_ON_DISCUSSION)
        self.waiting_element_and_click(DiscussionPageLocators.ANSWER_ON_DISCUSSION)
        time.sleep(10)
        try:
            self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, text)
        except NoSuchElementException:
            self.move_to_element_and_click(DiscussionPageLocators.ANSWER_ON_DISCUSSION)
            time.sleep(10)
            self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, text)
        self.create_screenshot()

    @allure.step("Отменить ответ в обсуждении")
    def cancel_answer_to_discuss(self) -> NoReturn:
        self.scroll_to_element(DiscussionPageLocators.CANCEL_ANSWER)
        self.waiting_element_and_click(DiscussionPageLocators.CANCEL_ANSWER)
        self.waiting_element_and_click(DiscussionPageLocators.CONFIRM_CANCEL_BUTTON)
        time.sleep(2)
        self.create_screenshot()

    @allure.step("Сохранить ответ в обсуждении")
    def save_answer_to_discuss(self) -> NoReturn:
        self.scroll_to_element(DiscussionPageLocators.CONFIRM_ANSWER)
        self.waiting_element_and_click(DiscussionPageLocators.CONFIRM_ANSWER)
        time.sleep(5)
        self.create_screenshot()

    @allure.step("Упоминание участника в обсуждении")
    def mention_in_discuss(self, text, member_for_mention) -> NoReturn:
        self.waiting_element_and_click(DiscussionPageLocators.ANSWER_TO_ANSWER_ON_DISCUSSION)
        time.sleep(10)
        self.scroll_to_element(DiscussionPageLocators.ANSWER_INPUT)
        self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, text)
        self.waiting_element_and_click(DiscussionPageLocators.MENTION_MEMBERS)
        self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, member_for_mention)
        time.sleep(2)
        self.waiting_element_and_click(DiscussionPageLocators.SELECT_FIRST_MEMBER_FROM_LIST)
        self.create_screenshot()

    @allure.step("Переход на вкладку 'Визуально', редактирование ответа, переход на вкладку 'Код'")
    def edit_text_answer(self, bold_text, italic_text, underlined_text) -> NoReturn:
        self.waiting_element_and_click(DiscussionPageLocators.MOVE_TO_VISUAL)
        time.sleep(7)
        self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, '\n')
        self.waiting_element_and_click(DiscussionPageLocators.BOLD_TEXT)
        self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, bold_text)
        self.waiting_element_and_click(DiscussionPageLocators.BOLD_TEXT)
        self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, '\n')
        self.waiting_element_and_click(DiscussionPageLocators.ITALIC_TEXT)
        self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, italic_text)
        self.waiting_element_and_click(DiscussionPageLocators.ITALIC_TEXT)
        self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, '\n')
        self.waiting_element_and_click(DiscussionPageLocators.TEXT_STYLE_BUTTON)
        self.waiting_element_and_click(DiscussionPageLocators.UNDERLINED_TEXT)
        self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, underlined_text)
        self.waiting_element_and_click(DiscussionPageLocators.TEXT_STYLE_BUTTON)
        self.waiting_element_and_click(DiscussionPageLocators.UNDERLINED_TEXT)
        self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, '\n')
        self.scroll_on_page(-300)
        self.scroll_to_element(DiscussionPageLocators.SPEC_SYMBOLS_LIST)
        self.waiting_element_and_click(DiscussionPageLocators.SPEC_SYMBOLS_LIST)
        time.sleep(1)
        self.scroll_on_page(-300)
        self.waiting_element_and_click(DiscussionPageLocators.LATIN_SPEC_SYMBOLS)
        time.sleep(2)
        for symbol in range(5):
            self.action.move_to_element(self.browser.find_element(*DiscussionPageLocators.FIRST_LATIN_SYMBOL)).click().perform()
            time.sleep(1)
        self.waiting_element_and_click(DiscussionPageLocators.SPEC_SYMBOLS_LIST)
        time.sleep(1)
        self.waiting_element_and_click(DiscussionPageLocators.MOVE_TO_CODE)
        time.sleep(10)
        self.create_screenshot()

    @allure.step("Добавление ссылки в ответ")
    def add_link_to_answer(self, link) -> NoReturn:
        self.element_send_keys(DiscussionPageLocators.ANSWER_INPUT, '\n')
        self.waiting_element_and_click(DiscussionPageLocators.ADD_LINK)
        time.sleep(1)
        self.element_send_keys(DiscussionPageLocators.ADD_LINK_INPUT, link)
        time.sleep(2)
        self.waiting_element_and_click(DiscussionPageLocators.SELECT_FIRST_LINK_FROM_LIST)
        self.scroll_to_element(DiscussionPageLocators.CONFIRM_ANSWER)
        self.waiting_element_and_click(DiscussionPageLocators.CONFIRM_ANSWER)
        time.sleep(5)
        self.scroll_on_page(-400)
        self.create_screenshot()

    @allure.step("Создание новой темы обсуждения")
    def create_new_theme_of_discuss(self, theme_title, theme_description) -> NoReturn:
        self.waiting_element_and_click(DiscussionPageLocators.ADD_THEME_BUTTON)
        time.sleep(5)
        if self.is_element_present(DiscussionPageLocators.THEME_DISCUSSION_INPUT_NEW):
            self.scroll_to_element(DiscussionPageLocators.THEME_DISCUSSION_INPUT_NEW)
            self.element_send_keys(DiscussionPageLocators.THEME_DISCUSSION_INPUT_NEW, theme_title)
        else:
            self.scroll_to_element(DiscussionPageLocators.THEME_DISCUSSION_INPUT_OLD)
            self.element_send_keys(DiscussionPageLocators.THEME_DISCUSSION_INPUT_OLD, theme_title)
        time.sleep(5)
        try:
            self.element_send_keys(DiscussionPageLocators.DESCRIPTION_INPUT, theme_description)
        except NoSuchElementException:
            time.sleep(2)
            self.move_to_element_and_send_keys(DiscussionPageLocators.DESCRIPTION_INPUT, theme_description)
        self.create_screenshot()

    @allure.step("Подтверждение создания новой темы обсуждения")
    def confirm_create_new_theme_of_discuss(self) -> NoReturn:
        self.scroll_to_element(DiscussionPageLocators.CONFIRM_ADDING_THEME)
        time.sleep(2)
        self.waiting_element_and_click(DiscussionPageLocators.CONFIRM_ADDING_THEME)
        time.sleep(10)
        self.create_screenshot()

    @allure.step("Отмена создания новой темы обсуждения")
    def cancel_creating_new_theme_of_discuss(self) -> NoReturn:
        self.waiting_element_and_click(DiscussionPageLocators.CANCEL_ANSWER)
        time.sleep(2)
        if not self.is_element_present(DiscussionPageLocators.CONFIRM_CANCEL_CREATE_NEW_THEME):
            self.waiting_element_and_click(DiscussionPageLocators.CANCEL_ANSWER)
            time.sleep(2)
        self.waiting_element_and_click(DiscussionPageLocators.CONFIRM_CANCEL_CREATE_NEW_THEME)
        self.create_screenshot()

    @allure.step("Отмена и возврат к созданию новой темы обсуждения")
    def cancel_and_continue_creating_new_theme_of_discuss(self) -> NoReturn:
        self.scroll_to_element(DiscussionPageLocators.CANCEL_ANSWER)
        self.waiting_element_and_click(DiscussionPageLocators.CANCEL_ANSWER)
        time.sleep(2)
        self.waiting_element_and_click(DiscussionPageLocators.NOT_CONFIRM_CANCEL_BUTTON)
        time.sleep(5)
        self.create_screenshot()

    @allure.step("Добавление дополнительного описания при создании новой темы обсуждения")
    def adding_additionally_description_creating_new_theme_of_discuss(self, theme_title: str, additionally_description: str) -> NoReturn:
        self.scroll_to_element(DiscussionPageLocators.OPEN_ADDITIONALLY_DESCRIPTION)
        if self.is_element_present(DiscussionPageLocators.HIDDEN_ADDITIONALLY_DESCRIPTION_INPUT):
            self.waiting_element_and_click(DiscussionPageLocators.OPEN_ADDITIONALLY_DESCRIPTION)
            time.sleep(3)
        self.clear_element(DiscussionPageLocators.ADDITIONALLY_DESCRIPTION_INPUT)
        self.element_send_keys(DiscussionPageLocators.ADDITIONALLY_DESCRIPTION_INPUT, f'/* {theme_title} */ {additionally_description}')
        self.create_screenshot()

    @allure.step("Открытие окна прослушивания статьи")
    def open_article_listening_window(self):
        self.waiting_element_and_click(ArticlePageLocators.OPEN_VOICING_WINDOW_ARTICLE)
        self.create_screenshot()

    @allure.step("Воспроизведение прослушивания статьи")
    def start_listening_article(self):
        self.waiting_element_and_click(ArticlePageLocators.START_LISTENING_ARTICLE)
        self.create_screenshot()

    @allure.step("Переключение варианта озвучки статьи")
    def change_listening_version(self):
        self.waiting_element_and_click(ArticlePageLocators.SELECT_SECOND_VERSION_LISTENING)
        time.sleep(5)
        self.create_screenshot()

    @allure.step("Остановка прослушивания статьи")
    def pause_listening_article(self):
        self.waiting_element_and_click(ArticlePageLocators.PAUSE_LISTENING_ARTICLE)
        self.create_screenshot()

    def assert_that_shortened_listen_present(self):
        """ Проверка, что доступна озвучка сокращенной версии статьи """
        self.assert_that_element_present(ArticlePageLocators.SELECT_SHORT_VERSION_LISTENING_ARTICLE, "Сокращенная версия статьи")

    def assert_that_listening_version_changed(self):
        """ Проверка, что озвучка прослушивания версии изменена """
        self.assert_that_element_present(ArticlePageLocators.PAUSE_LISTENING_ARTICLE, "Кнопка 'Паузы'")

    def assert_that_article_listening_window_present(self):
        """ Проверка, что окно прослушивания статьи отображается """
        self.assert_that_element_present(ArticlePageLocators.START_LISTENING_ARTICLE, "Кнопка воспроизведения прослушивания статьи")
        self.assert_that_element_present(ArticlePageLocators.SELECT_FIRST_VERSION_LISTENING, "Кнопка выбора первого варианта озвучки статьи")
        self.assert_that_element_present(ArticlePageLocators.SELECT_SECOND_VERSION_LISTENING, "Кнопка выбора второго варианта озвучки статьи")

    def assert_that_article_stopped(self):
        """ Проверка, что статья остановлены """
        self.assert_that_element_present(ArticlePageLocators.START_LISTENING_ARTICLE, "Кнопка старта прослушивания")

    def assert_that_count_recommendation_widget_is_correct(self, widget_count: int):
        """ Проверка, что количество виджетов рекомендаций правильное """
        exist_last_recommendation_widget_selector = (By.XPATH, f'//div[@class="recommendations"]//div[@class="article-card"][{widget_count}]//a')
        self.assert_that_element_present(exist_last_recommendation_widget_selector, "Последний возможный виджет")
        above_max_widget_count = widget_count + 1
        not_exist_last_recommendation_widget_selector = (By.XPATH, f'//div[@class="recommendations"]//div[@class="article-card"][{above_max_widget_count}]//a')
        self.assert_that_element_not_present(not_exist_last_recommendation_widget_selector, "Недопустимый по порядку виджет (количество виджетов превышено)")

    def assert_that_saved_history_of_visits(self, history_of_visits: Response, article_id: int):
        """ Проверка, что посещенная статья сохраняется в истории посещения """
        article_id_from_history_of_visits = history_of_visits.json()["hits"]["hits"][0]["_source"]["page_id"]
        self.assert_that_values_equals(str(article_id), str(article_id_from_history_of_visits))

    def assert_that_ruwiki_recommendee_id_saved_in_cookie(self, cookie_name: str, cookie_value):
        """ Проверка, что в Cookie сохранено указанное значение """
        current_cookie_value = self.get_cookie(cookie_name)
        self.assert_that_values_equals(current_cookie_value, cookie_value)

    def assert_that_ruwiki_recommendee_id_in_cookie_is_defferent(self, cookie_name: str, cookie_value):
        """ Проверка, что в Cookie сохранено другое значение """
        current_cookie_value = self.get_cookie(cookie_name)
        self.assert_that_values_not_equals(current_cookie_value, cookie_value)

    def assert_that_recommendations_are_listed_in_order(self):
        """ Проверка, что рекомендации отображаются по порядку """
        if self.is_element_present(MainPageLocators.FIRST_WIDGET_WITH_IMAGE):
            self.assert_that_element_present(MainPageLocators.FIRST_WIDGET_WITH_IMAGE, "Первый виджет с изображением")
            self.assert_that_element_not_present(MainPageLocators.SECOND_WIDGET_WITH_IMAGE, "Второй виджет с изображением")
            self.assert_that_element_present(MainPageLocators.THIRD_WIDGET_WITH_IMAGE, "Третий виджет с изображением")
            self.assert_that_element_not_present(MainPageLocators.FORTH_WIDGET_WITH_IMAGE, "Четвертый виджет с изображением")
        else:
            self.assert_that_element_not_present(MainPageLocators.FIRST_WIDGET_WITH_IMAGE, "Первый виджет с изображением")
            self.assert_that_element_present(MainPageLocators.SECOND_WIDGET_WITH_IMAGE, "Второй виджет с изображением")
            self.assert_that_element_not_present(MainPageLocators.THIRD_WIDGET_WITH_IMAGE, "Третий виджет с изображением")
            self.assert_that_element_present(MainPageLocators.FORTH_WIDGET_WITH_IMAGE, "Четвертый виджет с изображением")

    def assert_that_article_from_widget_is_favourite(self):
        """ Проверка, что статья относится к категории избранных статей """
        self.assert_that_element_present(ArticlePageLocators.FAVOURITE_ARTICLE_CATEGORY, "Статья из категории избранных")

    def assert_that_not_display_articles_as_dont_show_in_recommendations(self, marked_widget_name: str):
        """ Проверка, что отмеченная статья не показывается в секции рекомендаций """
        time.sleep(2)
        widget_title = self.get_attribute_from_element(MainPageLocators.FIRST_WIDGET_FROM_RECOMMENDATIONS, 'title')
        self.assert_that_values_not_equals(widget_title, marked_widget_name)

    def assert_that_not_display_articles_as_dont_show_in_recommendations_from_article(self, marked_widget_name: str):
        """ Проверка, что отмеченная статья не показывается в секции 'Смотрите также' """
        widget_title = self.get_attribute_from_element(ArticlePageLocators.FIRST_WIDGET_FROM_SEE_ALSO_SECTION, 'title')
        self.assert_that_values_not_equals(widget_title, marked_widget_name)

    def assert_that_recommendation_widgets_present_from_see_also_section(self):
        """ Проверка, что виджеты рекомендаций из секции 'Смотрите также' отображаются """
        self.assert_that_element_present(ArticlePageLocators.FIRST_WIDGET_FROM_SEE_ALSO_SECTION, "Первый виджет из секции 'Смотрите также'")

    def assert_that_recommendation_widgets_present(self):
        """ Проверка, что виджеты рекомендаций отображаются """
        self.assert_that_element_present(MainPageLocators.FIRST_WIDGET_FROM_RECOMMENDATIONS, "Первый виджет рекомендаций")
        self.assert_that_element_present(MainPageLocators.SECOND_WIDGET_FROM_RECOMMENDATIONS, "Второй виджет рекомендаций")
        self.assert_that_element_present(MainPageLocators.DONT_SHOW_AGAIN_BUTTON, "Кнопка 'Больше не показывать'")

    def assert_that_present_information_about_editing_article(self) -> NoReturn:
        self.scroll_to_element(WatchListLocators.FIRST_TEXT_ABOUT_EDIT_ARTICLE)
        self.assert_that_element_present(WatchListLocators.FIRST_TEXT_ABOUT_EDIT_ARTICLE, "Информация о последнем изменении статьи")

    def assert_that_article_is_edited(self, text_for_edit_article) -> NoReturn:
        time.sleep(5)
        text_with_edit_1 = self.get_text_from_element(ArticlePageLocators.EDITED_TEXT_ARTICLE_1)
        if self.is_element_present(ArticlePageLocators.EDITED_TEXT_ARTICLE_2):
            text_with_edit_2 = self.get_text_from_element(ArticlePageLocators.EDITED_TEXT_ARTICLE_2)
        else:
            text_with_edit_2 = ""
        if self.is_element_present(ArticlePageLocators.EDITED_TEXT_ARTICLE_3):
            text_with_edit_3 = self.get_text_from_element(ArticlePageLocators.EDITED_TEXT_ARTICLE_3)
        else:
            text_with_edit_3 = ""
        text_with_edit = f"{text_with_edit_1}\n{text_with_edit_2}\n{text_with_edit_3}"
        self.create_attachment(text_for_edit_article, "Текст из статьи")
        self.create_attachment(text_with_edit, "Сравниваемый текст")
        assert text_for_edit_article in text_with_edit, "Статья не отредактирована"
        self.create_screenshot()

    def assert_that_article_present(self) -> NoReturn:
        """ Проверка, что статья отображается """
        if self.is_element_present(ArticlePageLocators.TITLE_ARTICLE_1):
            self.assert_that_element_present(ArticlePageLocators.TITLE_ARTICLE_1, "Название статьи")
        elif self.is_element_present(ArticlePageLocators.TITLE_ARTICLE_2):
            self.assert_that_element_present(ArticlePageLocators.TITLE_ARTICLE_2, "Название статьи")
        else:
            self.assert_that_element_present(ArticlePageLocators.TITLE_ARTICLE_3, "Название статьи")
        if self.is_element_present(ArticlePageLocators.FIRST_SECTION_TEXT_ARTICLE):
            self.assert_that_element_present(ArticlePageLocators.FIRST_SECTION_TEXT_ARTICLE, "Текст статьи")
        elif self.is_element_present(ArticlePageLocators.FIRST_SECTION_SECOND_BLOCK_TEXT_ARTICLE):
            self.assert_that_element_present(ArticlePageLocators.FIRST_SECTION_SECOND_BLOCK_TEXT_ARTICLE, "Текст статьи")
        else:
            self.assert_that_element_present(ArticlePageLocators.SECOND_SECTION_TEXT_ARTICLE, "Текст статьи")

    def assert_that_display_article_actions(self, role=None) -> NoReturn:
        self.assert_that_element_present(ArticlePageLocators.EDIT_ARTICLE, 'Вкладка "Править"')
        self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
        self.assert_that_element_present(ArticlePageLocators.READ_ARTICLE, 'Вкладка "Читать"')
        self.assert_that_element_present(ArticlePageLocators.EDIT_CODE_ARTICLE, 'Вкладка "Править код"')
        self.assert_that_element_present(ArticlePageLocators.HISTORY_ARTICLE, 'Вкладка "История"')
        if role == "user":
            self.assert_that_element_present(SideBarLocators.RENAME_ARTICLE, 'Действие "Переименовать"')
            # Доделать Переименование
            # self.back_to_page()
        elif role == "admin":
            self.assert_that_element_present(SideBarLocators.INSTRUMENTS, 'Вкладка "Инструменты"')
            self.assert_that_element_present(SideBarLocators.RENAME_ARTICLE, 'Действие "Переименовать"')
            self.assert_that_element_present(SideBarLocators.PROTECT_ARTICLE, 'Действие "Защитить"')
            self.assert_that_element_present(SideBarLocators.DELETE_ARTICLE, 'Действие "Удалить"')
            self.assert_that_element_present(SideBarLocators.STABILIZE_ARTICLE, 'Действие "Стабилизовать"')

    def assert_that_display_links_on_header_bar(self, authorization: str) -> NoReturn:
        if authorization == "non-authorization":
            self.assert_that_element_present(TopBarLocators.SIGN_IN_BUTTON, "Ссылка 'Войти'")
            self.assert_that_element_present(TopBarLocators.CREATE_ACCOUNT, "Ссылка 'Создать учетную запись'")
        elif authorization == "user":
            self.assert_that_element_present(TopBarLocators.ACCOUNT_NAME, "Ссылка 'Имя учетной записи'")
            self.assert_that_element_present(TopBarLocators.ALERTS, "Ссылка 'Ваши оповещения'")
            self.assert_that_element_present(TopBarLocators.NOTIFICATIONS, "Ссылка 'Ваши уведомления'")
            self.waiting_element_and_click(TopBarLocators.OPEN_PROFILE_ACTIONS_BUTTON)
            self.assert_that_element_present(TopBarLocators.WATCHLIST, "Ссылка 'Список наблюдения'")
            self.assert_that_element_present(TopBarLocators.DRAFT, "Ссылка 'Черновик'")
            self.assert_that_element_present(TopBarLocators.SETTINGS, "Ссылка 'Настройки'")
            self.assert_that_element_present(TopBarLocators.LOGOUT_BUTTON, "Ссылка 'Выйти'")

    def assert_that_theme_changed(self, theme) -> NoReturn:
        if theme == 'Монобук':
            self.assert_that_element_present(TopBarLocators.MONOBOOK_THEME_LOGO_LINK, "Логотип (Монобук)")
            self.assert_that_element_present(TopBarLocators.MONOBOOK_THEME_PERSONAL_INSTRUMENTS, "Верхняя панель (Монобук)")
            self.assert_that_element_present(SideBarLocators.MONOBOOK_THEME_LEFT_SIDE_BAR, "Левое боковое меню (Монобук)")
        elif theme == 'Рувики':
            self.assert_that_element_present(TopBarLocators.RUWIKI_THEME_LOGO_LINK, "Логотип (тема Рувики)")
            self.assert_that_element_present(TopBarLocators.RUWIKI_THEME_PERSONAL_INSTRUMENTS, "Верхняя панель (тема Рувики)")
            self.assert_that_element_present(SideBarLocators.RUWIKI_THEME_LEFT_SIDE_BAR, "Левое боковое меню (тема Рувики)")
        elif theme == 'Енисей':
            self.assert_that_element_present(TopBarLocators.YENISEY_THEME_LOGO_LINK, "Логотип (тема Енисей)")
            self.assert_that_element_present(TopBarLocators.YENISEY_THEME_PERSONAL_INSTRUMENTS, "Верхняя панель (тема Рувики)")
            self.assert_that_element_present(SideBarLocators.YENISEY_THEME_LEFT_SIDE_BAR, "Левое боковое меню (тема Рувики)")

    def assert_that_discuss_is_created(self) -> NoReturn:
        self.assert_that_element_present(DiscussionPageLocators.DISCUSSION_TITLE, 'Страница Обсуждение')
        text = self.get_text_from_element(DiscussionPageLocators.DISCUSSION_TEXT)
        self.create_attachment(text, 'Текст обсуждения')
        self.assert_that_element_present(DiscussionPageLocators.DISCUSSION_TEXT, 'Текст обсуждения (описание обсуждения)')

    def assert_that_discuss_is_edited(self) -> NoReturn:
        self.assert_that_element_present(DiscussionPageLocators.DISCUSSION_TITLE, 'Страница Обсуждение')
        self.assert_that_element_present(DiscussionPageLocators.DISCUSSION_EDITED_TEXT, 'Текст обсуждения (описание обсуждения)')

    def assert_that_answer_not_saved(self) -> NoReturn:
        self.assert_that_element_not_present(DiscussionPageLocators.ANSWER_SAVED_TEXT, "Сохраненный текст ответа")

    def assert_that_mention_present_in_answer(self) -> NoReturn:
        self.assert_that_element_present(DiscussionPageLocators.TEXT_FROM_ANSWER, "Текст перед упоминанием")
        self.assert_that_element_present(DiscussionPageLocators.MENTION_FROM_ANSWER, "Упоминание пользователя Admin")

    def assert_that_edited_text_link_present_in_answer(self, link) -> NoReturn:
        self.scroll_to_element(DiscussionPageLocators.TEXT_FROM_ANSWER)
        self.assert_that_element_present(DiscussionPageLocators.BOLD_TEXT_FROM_ANSWER, "Текст полужирным шрифтом")
        self.assert_that_element_present(DiscussionPageLocators.ITALIC_TEXT_FROM_ANSWER, "Текст курсивным шрифтом")
        self.scroll_to_element(DiscussionPageLocators.BOLD_TEXT_FROM_ANSWER)
        self.assert_that_element_present(DiscussionPageLocators.UNDERLINED_TEXT_FROM_ANSWER, "Текст подчеркнутым шрифтом")
        if self.is_element_present(DiscussionPageLocators.SPEC_SYMBOLS_FROM_ANSWER):
            self.assert_that_element_present(DiscussionPageLocators.SPEC_SYMBOLS_FROM_ANSWER, "Специальные символы")
        else:
            self.assert_that_element_present(DiscussionPageLocators.SPEC_SYMBOLS_FROM_ANSWER_2, "Специальные символы")
        self.scroll_to_element(DiscussionPageLocators.UNDERLINED_TEXT_FROM_ANSWER)
        self.assert_that_element_present(DiscussionPageLocators.LINK_FROM_ANSWER, f"Ссылка {link}")

    def assert_that_alert_about_mention_present(self) -> NoReturn:
        self.assert_that_element_present(AlertsPageLocators.FIRST_ALERT_ABOUT_MENTION, "Уведомление об упоминании")

    def assert_that_creating_new_theme_canceled(self) -> NoReturn:
        self.scroll_on_page(300)
        self.assert_that_element_not_present(DiscussionPageLocators.NEW_THEME_TITLE, "Название новой темы")
        self.assert_that_element_not_present(DiscussionPageLocators.NEW_THEME_DESCRIPTION, "Описание новой темы")

    def assert_that_new_theme_created(self) -> NoReturn:
        self.scroll_on_page(200)
        self.assert_that_element_present(DiscussionPageLocators.NEW_THEME_TITLE, "Название новой темы")
        self.assert_that_element_present(DiscussionPageLocators.NEW_THEME_DESCRIPTION, "Описание новой темы")

    def assert_that_gratitude_present(self):
        self.assert_that_element_present(HistoryArticleLocators.GRATEFUL_SIMPLE_USER, "Благодарность")

    def assert_that_display_article_link(self, link: str) -> NoReturn:
        if link == 'Содержание':
            self.assert_that_element_present(SideBarLocators.CONTENT_TITLE_ARTICLE, 'Название "Рувики:Содержание"')
            self.waiting_element_and_click(ArticlePageLocators.TALK_ARTICLE)
            time.sleep(2)
            self.assert_that_element_present(ArticlePageLocators.TALK_TITLE_ARTICLE, 'Название "Обсуждение:Википедия:Содержание"')
        elif link == 'Свежие правки':
            self.assert_that_element_present(RecentChangesLocators.RECENT_CHANGES_TITLE, 'Название "Свежие правки"')
        elif link == 'Новые страницы':
            self.assert_that_element_present(NewPagesLocators.NEW_PAGES_TITLE, 'Название "Новые страницы"')
        elif link == 'Справка':
            self.assert_that_element_present(SideBarLocators.REFERENCE_TITLE_ARTICLE, 'Название "Википедия:Справка"')
            # Доделать шаг 8

    def assert_that_display_article_and_discussion_tabs_on_article(self) -> NoReturn:
        self.assert_that_element_present(ArticlePageLocators.TALK_ARTICLE, "Вкладка 'Обсуждение'")

    def assert_that_display_tabs_after_move_to_discussion(self, authorization: str) -> NoReturn:
        self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
        if authorization == "non-authorization":
            self.assert_that_element_present(ArticlePageLocators.READ_ARTICLE, "Вкладка 'Читать'")
            self.assert_that_element_present(ArticlePageLocators.EDIT_CODE_ARTICLE, "Вкладка 'Править код'")
            self.assert_that_element_present(ArticlePageLocators.HISTORY_ARTICLE, "Вкладка 'История'")
        elif authorization == "authorization":
            self.assert_that_element_present(ArticlePageLocators.READ_ARTICLE, "Вкладка 'Читать'")
            self.assert_that_element_present(ArticlePageLocators.EDIT_CODE_ARTICLE, "Вкладка 'Править код'")
            self.assert_that_element_present(ArticlePageLocators.HISTORY_ARTICLE, "Вкладка 'История'")

    def assert_that_upload_file_is_working(self) -> NoReturn:
        try:
            if self.is_element_present(SideBarLocators.INSTRUMENTS):
                self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
        except TimeoutException:
            pass
        self.assert_that_element_present(SideBarLocators.UPLOAD_FILE_ARTICLE, 'Действие Загрузить файл')
        self.waiting_element_and_click(SideBarLocators.UPLOAD_FILE_ARTICLE)
        self.scroll_to_element(UploadFileArticleLocators.UPLOAD_FILE_INPUT)
        self.assert_that_element_present(UploadFileArticleLocators.UPLOAD_FILE_INPUT, 'Форма для загрузки файла')

    def assert_that_history_article_present(self) -> NoReturn:
        self.assert_that_element_present(HistoryArticleLocators.ABOUT_HISTORY_PAGE_LINK, "Ссылка 'О странице истории изменений'")

    def assert_that_added_constructions_to_text(self):
        self.assert_that_element_present(ArticlePageLocators.FIRST_TABLE_ARTICLE, "Таблица")
        self.assert_that_element_present(ArticlePageLocators.FIRST_LIST_ARTICLE, "Список")
        self.assert_that_element_present(ArticlePageLocators.FIRST_TEMPLATE_ARTICLE, "Шаблон")

    @allure.step("Переход к созданию новой статьи")
    def move_to_new_article(self) -> NoReturn:
        if self.is_element_present(NewPagesLocators.MOVE_TO_CREATED_AUTOTEST_ARTICLE):
            self.scroll_to_element(NewPagesLocators.MOVE_TO_CREATED_AUTOTEST_ARTICLE)
            time.sleep(1)
            self.waiting_element_and_click(NewPagesLocators.MOVE_TO_CREATED_AUTOTEST_ARTICLE)
        else:
            self.scroll_to_element(NewPagesLocators.MOVE_TO_NEW_ARTICLE)
            time.sleep(1)
            self.waiting_element_and_click(NewPagesLocators.MOVE_TO_NEW_ARTICLE)
        self.create_screenshot()

    def assert_that_additional_text_are_in_article(self, additional_text) -> NoReturn:
        text_with_edit = self.get_text_from_element(ArticlePageLocators.TEXT_ARTICLE)
        assert additional_text in text_with_edit, "Статья не отредактирована"
        self.create_screenshot()

    def assert_that_possible_creating_non_existent_article(self) -> NoReturn:
        if self.is_element_present(ArticlePageLocators.VIEW_CODE):
            self.waiting_element_and_click(ArticlePageLocators.VIEW_CODE)
            time.sleep(2)
        if self.is_element_present(ArticlePageLocators.ARTICLE_WITHOUT_TEXT):
            self.assert_that_element_present(ArticlePageLocators.ARTICLE_WITHOUT_TEXT, "Отсутствующий текст статьи")
        elif self.is_element_present(ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT):
            self.assert_that_element_present(ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT, "Надпись о том, что статьи нет в РУВИКИ")
        elif self.is_element_present(ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT_FROM_EDIT):
            self.assert_that_element_present(ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT_FROM_EDIT, "Надпись о том, что статьи нет в РУВИКИ")
        else:
            self.assert_that_element_present(ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT_2,
                                             "Надпись о том, что статьи нет в РУВИКИ")
        if self.is_element_present(ArticlePageLocators.EDIT_ARTICLE):
            self.assert_that_element_present(ArticlePageLocators.EDIT_ARTICLE, "Кнопка 'Править'")
        else:
            self.assert_that_element_present(ArticlePageLocators.CREATE_ARTICLE, "Кнопка 'Создать'")

    @allure.step("Переход по добавленной ссылке")
    def move_to_added_link(self) -> NoReturn:
        self.waiting_element_and_click(ArticlePageLocators.FIRST_LINK_TEXT_ARTICLE)
        self.create_screenshot()

    def assert_that_article_created_and_display_headers_and_quotes(self) -> NoReturn:
        self.assert_that_element_present(ArticlePageLocators.FIRST_HEADER_2_ARTICLE, "Заголовок 2")
        self.assert_that_element_present(ArticlePageLocators.FIRST_HEADER_3_ARTICLE, "Заголовок 3")
        self.assert_that_element_present(ArticlePageLocators.FIRST_HEADER_4_ARTICLE, "Заголовок 4")
        self.assert_that_element_present(ArticlePageLocators.FIRST_HEADER_5_ARTICLE, "Заголовок 5")
        self.assert_that_element_present(ArticlePageLocators.FIRST_QUOTES_ARTICLE, "Цитата")

    def assert_that_added_bold_text_image_present_on_review(self) -> NoReturn:
        self.assert_that_element_present(ArticlePageLocators.BOLD_TEXT, "Текст 'Полужирное начертание' полужирным шрифтом")
        self.assert_that_element_present(ArticlePageLocators.AUTOTEST_UPLOADED_IMAGE, "Изображение Ruwiki-image.jpg")

    def assert_that_added_category_redirect_present_on_edited_article(self) -> NoReturn:
        self.assert_that_element_present(ArticlePageLocators.AUTOTEST_CATEGORY, "Категория 'Autotest Category'")
        self.assert_that_element_present(ArticlePageLocators.REDIRECT, "Перенаправление 'Алтай'")

    def assert_that_added_notes_images_to_article(self) -> NoReturn:
        self.scroll_to_element(CodeEditorArticleLocators.DESCRIPTION_EDIT_CODE_ARTICLE_INPUT)
        self.element_send_keys(CodeEditorArticleLocators.DESCRIPTION_EDIT_CODE_ARTICLE_INPUT, "autotest-description")
        self.scroll_to_element(CodeEditorArticleLocators.PREVIEW_INPUT)
        self.waiting_element_and_click(CodeEditorArticleLocators.PREVIEW_INPUT)
        time.sleep(5)
        self.scroll_to_element(ArticlePageLocators.AUTOTEST_NOTES)
        self.assert_that_element_present(ArticlePageLocators.AUTOTEST_NOTES, "Примечание 'notes'")
        self.scroll_on_page(-400)
        self.scroll_to_element(ArticlePageLocators.EXAMPLE_IMAGE)
        self.assert_that_element_present(ArticlePageLocators.EXAMPLE_IMAGE, "Изображение Example.jpg")

    def assert_that_changes_is_canceled(self) -> NoReturn:
        self.assert_that_element_not_present(ArticlePageLocators.SMALL_TEXT, 'Мелкий текст')

    def assert_that_edited_signature_present(self, username):
        signature = self.get_text_from_element(DiscussionPageLocators.USER_SIGNATURE_LINK)
        assert username in signature, "Подпись текущего пользователя не обнаружена"
        self.create_screenshot()

    @allure.step("Удаление статьи")
    def delete_article(self):
        self.scroll_to_element(DeleteUndeleteArticleLocators.DELETE_SECTION)
        self.waiting_element_and_click(DeleteUndeleteArticleLocators.OPEN_DELETE_CAUSE_LIST)
        time.sleep(2)
        if self.is_element_present(DeleteUndeleteArticleLocators.SELECT_TEST_PAGE_CAUSE):
            time.sleep(2)
            self.waiting_element_and_click(DeleteUndeleteArticleLocators.SELECT_TEST_PAGE_CAUSE)
        else:
            time.sleep(2)
            self.waiting_element_and_click(DeleteUndeleteArticleLocators.SELECT_TEST_FILE_CAUSE)
        if self.is_element_present(DeleteUndeleteArticleLocators.DELETE_LINKED_TALK_PAGE):
            self.move_to_element_and_click(DeleteUndeleteArticleLocators.DELETE_LINKED_TALK_PAGE)
        self.move_to_element_and_click(DeleteUndeleteArticleLocators.FOLLOW_PAGE_CHECKBOX)
        self.scroll_to_element(DeleteUndeleteArticleLocators.CONFIRM_DELETE_ARTICLE)
        self.waiting_element_and_click(DeleteUndeleteArticleLocators.CONFIRM_DELETE_ARTICLE)
        self.create_screenshot()

    def assert_that_article_deleted(self):
        """ Проверка, что после удаления страницы отображается текст об удалении статьи """
        if self.is_element_present(DeleteUndeleteArticleLocators.TEXT_ABOUT_DELETED_ARTICLE):
            self.assert_that_element_present(DeleteUndeleteArticleLocators.TEXT_ABOUT_DELETED_ARTICLE, "Текст об удалении статьи")
        else:
            self.assert_that_element_present(DeleteUndeleteArticleLocators.TEXT_ABOUT_DELETED_ARTICLE_AND_LINKED_TALK, "Текст об удалении статьи и соответствующего обсуждения")

    def assert_that_blocked_user_can_not_edit_article(self):
        """ Проверка, что заблокированному пользователю недоступно редактирование статьи в режиме визуального редактирования """
        if self.is_element_present(ArticlePageLocators.REQUEST_UNBLOCK):
            self.assert_that_element_present(ArticlePageLocators.REQUEST_UNBLOCK, "Кнопка 'Запросить разблокировку'")
        else:
            self.assert_that_element_present(ArticlePageLocators.BLOCKED_USER_TEXT, "Надпись о блокировке")

    def assert_that_blocked_user_can_not_edit_code_article(self):
        """ Проверка, что заблокированному пользователю недоступно редактирование статьи в режиме редактора кода """
        if self.is_element_present(ArticlePageLocators.REQUEST_UNBLOCK):
            self.assert_that_element_present(ArticlePageLocators.REQUEST_UNBLOCK, "Кнопка 'Запросить разблокировку'")
        else:
            self.assert_that_element_present(ArticlePageLocators.BLOCKED_USER_TEXT, "Надпись о блокировке")

    @allure.step("Создание запроса на разблокировку")
    def create_request_unblock_user(self, reason: str):
        self.waiting_element_and_click(ArticlePageLocators.REQUEST_UNBLOCK)
        self.go_to_second_window()
        time.sleep(2)
        self.scroll_to_element(ArticlePageLocators.ADD_TEXT_EDIT_CODE_CREATE_ARTICLE_TEXTAREA)
        self.element_send_keys(ArticlePageLocators.ADD_TEXT_EDIT_CODE_CREATE_ARTICLE_TEXTAREA, reason)
        self.scroll_to_element(BlockUserLocators.CONFIRM_CREATE_REQUEST_FOR_UNBLOCK)
        self.waiting_element_and_click(BlockUserLocators.CONFIRM_CREATE_REQUEST_FOR_UNBLOCK)
        self.create_screenshot()

    def assert_that_request_unblock_present(self):
        """ Проверка, что запрос на разблокировку отображается """
        self.assert_that_element_present(BlockUserLocators.REQUEST_ABOUT_UNBLOCK_TITLE, "Просьба о разблокировке")

    @allure.step("Переход к восстановлению страницы через ссылку в статье")
    def move_to_undeleting_page(self):
        if not self.is_element_present(DeleteUndeleteArticleLocators.UNDELETE_ARTICLE_PAGE_TITLE):
            self.waiting_element_and_click(ArticlePageLocators.UNDELETE_ARTICLE_LINK)

    def assert_that_talk_deleted_from_personal_page(self):
        self.scroll_on_page(600)
        if self.is_element_present(DiscussionPageLocators.CLEARED_DISCUSSION_PERSONAL_PAGE_TITLE):
            self.assert_that_element_present(DiscussionPageLocators.CLEARED_DISCUSSION_PERSONAL_PAGE_TITLE, "Чистая страница обсуждения")
        else:
            self.assert_that_element_present(DiscussionPageLocators.CLEARED_DISCUSSION_MEMBER_PAGE_TITLE, "Чистая страница обсуждения")
        self.scroll_on_page(-600)

    @allure.step("Фиксация существования страницы")
    def fixation_exist_page(self):
        if self.is_element_present(ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT_2) or self.is_element_present(
                ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT) or self.is_element_present(ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT_3):
            self.create_screenshot()
            self.create_attachment('not-exist', 'Наличие страницы')
            return 'not-exist'
        else:
            self.create_screenshot()
            self.create_attachment('exist', 'Наличие страницы')
            return 'exist'

    @allure.step("Фиксация существования обсуждения страницы участника")
    def fixation_exist_talk_member_page(self):
        if not self.is_element_present(DiscussionPageLocators.CLEARED_DISCUSSION_PERSONAL_PAGE_TITLE) and not self.is_element_present(DiscussionPageLocators.CLEARED_DISCUSSION_MEMBER_PAGE_TITLE) and self.is_element_present(SideBarLocators.DELETE_ARTICLE):
            self.create_screenshot()
            self.create_attachment('exist', 'Наличие страницы')
            return 'exist'
        else:
            self.create_screenshot()
            self.create_attachment('not-exist', 'Наличие страницы')
            return 'not-exist'

    def assert_that_member_page_present(self):
        """ Проверка, что страница участника открыта """
        self.assert_that_element_present(ArticlePageLocators.ACCOUNT_NAME_ARTICLE_TITLE, 'Страница участника')

    def assert_that_page_not_exist(self):
        """ Проверка, что страницы не существует """
        self.assert_that_element_present(ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT, 'Текст о том, что страницы не существует')

    def assert_that_page_exist(self):
        """ Проверка, что страница участника открыта """
        self.assert_that_element_not_present(ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT, 'Текст о том, что страниа существует')

    def assert_that_bold_font_after_create_article_with_visual_editor_present(self):
        """ Проверка, что отображается текст с примененным полужирным шрифтом """
        self.assert_that_element_present(ArticlePageLocators.BOLD_TEXT_2, 'Текст "Автомобиль " полужирным шрифтом')

    def assert_that_note_after_create_article_with_visual_editor_present(self):
        """ Проверка, что отображается примечание """
        self.assert_that_element_present(ArticlePageLocators.NOTE, 'Примечание "_Автомобили 60-_х"')

    def assert_that_link_after_create_article_with_visual_editor_present(self):
        """ Проверка, что отображается внешняя ссылка """
        self.assert_that_element_present(ArticlePageLocators.EXTERNAL_LINK, 'Внешняя ссылка')

    def assert_that_file_deleted(self):
        """ Проверка, что загруженный файл был удален """
        self.assert_that_element_present(DeleteUndeleteArticleLocators.SUCCESS_DELETE_TITLE, 'Заголовок после успешного удаления')
        self.assert_that_element_present(DeleteUndeleteArticleLocators.TEXT_ABOUT_DELETED_FILE, 'Текст об удалении файла')

    def assert_that_subtitle_after_edit_article_with_visual_editor_present(self):
        """ Проверка, что отображается подзаголовок 1 в статье """
        self.assert_that_element_present(ArticlePageLocators.FIRST_HEADER_3_ARTICLE, 'Подзаголовок 1')

    def assert_that_marked_list_after_edit_article_with_visual_editor_present(self):
        """ Проверка, что отображается маркированный список """
        self.assert_that_element_present(ArticlePageLocators.FIRST_ELEMENT_OF_LIST, 'Первый элемент маркированного списка')
        self.assert_that_element_present(ArticlePageLocators.SECOND_ELEMENT_OF_LIST, 'Второй элемент маркированного списка')

    def assert_that_image_after_edit_article_with_visual_editor_present(self):
        """ Проверка, что изображение отображается """
        self.assert_that_element_present(ArticlePageLocators.UPLOADED_IMAGE, 'Загруженное изображение')

    def assert_that_personal_image_after_edit_article_with_visual_editor_present(self):
        """ Проверка, что собственное изображение отображается """
        self.assert_that_element_present(ArticlePageLocators.UPLOADED_PERSONAL_IMAGE, 'Собственное загруженное изображение')

    def assert_that_spec_symbols_after_edit_article_with_visual_editor_present(self):
        """ Проверка, что специальные символы отображаются """
        self.assert_that_element_present(ArticlePageLocators.SPEC_SYMBOLS, 'Специальные символы')

    def assert_that_replace_words_after_edit_article_with_visual_editor_present(self, first_replace, second_replace):
        first_note = self.get_text_from_element(ArticlePageLocators.FIRST_NOTE)
        second_note = self.get_text_from_element(ArticlePageLocators.SECOND_NOTE)
        all_notes = f"{first_note}\n{second_note}"
        self.create_attachment(all_notes, 'Текст')
        assert first_replace in all_notes, "Замена слов не произведена"
        assert second_replace in all_notes, "Замена слов не произведена"

    @allure.step("Отмена правки с вкладки 'История'")
    def cancel_changes_from_history(self):
        self.waiting_element_and_click(HistoryArticleLocators.CANCEL_FIRST_CHANGE)
        time.sleep(3)
        if self.is_element_present(ArticlePageLocators.WELCOME_POPUP):
            self.waiting_element_and_click(ArticlePageLocators.CLOSE_WELCOME_CREATE_POPUP_BUTTON)
        self.scroll_to_element(CodeEditorArticleLocators.SAVE_EDIT_CODE_ARTICLE_INPUT)
        self.waiting_element_and_click(CodeEditorArticleLocators.SAVE_EDIT_CODE_ARTICLE_INPUT)
        self.create_screenshot()

    @allure.step("Проверка, что статья очищена")
    def assert_that_article_cleared(self):
        text = self.get_text_from_article()
        assert text == "", "Статья не очищена"
        self.create_screenshot()

    def assert_that_article_exist(self):
        self.assert_that_element_present(SideBarLocators.DELETE_ARTICLE, "Кнопка удаления статьи")
        self.assert_that_element_not_present(ArticlePageLocators.DELETED_ARTICLE_PAGE_TEXT, "Текст об удалении статьи")

    @allure.step("Отмена последней правки")
    def reset_changes(self):
        self.scroll_to_element(HistoryArticleLocators.CANCEL_FIRST_CHANGE)
        self.waiting_element_and_click(HistoryArticleLocators.CANCEL_FIRST_CHANGE)
        self.scroll_to_element(CodeEditorArticleLocators.SAVE_EDIT_CODE_ARTICLE_INPUT)
        self.waiting_element_and_click(CodeEditorArticleLocators.SAVE_EDIT_CODE_ARTICLE_INPUT)
        self.create_screenshot()

    def assert_that_edit_cancel(self):
        """ Проверка, что правка отменена """
        self.assert_that_element_present(HistoryArticleLocators.TEXT_ABOUT_CANCEL_FIRST_CHANGE, "Текст о том, что правка отменена")

    @allure.step("Переход по первой кнопке в статье")
    def move_to_button_on_article(self):
        self.scroll_to_element(ArticlePageLocators.FIRST_BUTTON_CREATE_ARTICLE)
        self.waiting_element_and_click(ArticlePageLocators.FIRST_BUTTON_CREATE_ARTICLE)
        self.create_screenshot()

    def assert_that_article_or_creating_article_present(self):
        """ Проверка, что открыта страница создания статьи или статья """
        if self.is_element_present(ArticlePageLocators.CREATE_ARTICLE_TITLE):
            self.assert_that_element_present(ArticlePageLocators.CREATE_ARTICLE, "Кнопка 'Создать'")
            self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
            self.assert_that_element_present(ArticlePageLocators.EDIT_CODE_ARTICLE, "Кнопка 'Создать код'")
        else:
            self.assert_that_display_article_actions()

    @allure.step("Переход по первому изображению в статье")
    def move_to_first_image_on_article(self):
        self.scroll_to_element(ArticlePageLocators.FIRST_IMAGE_ARTICLE)
        self.waiting_element_and_click(ArticlePageLocators.FIRST_IMAGE_ARTICLE)
        self.create_screenshot()

    @allure.step("Переход к описанию файла")
    def move_to_data_about_image(self):
        self.waiting_element_and_click(ArticlePageLocators.MOVE_TO_DATA_ABOUT_IMAGE)
        self.create_screenshot()

    def assert_that_data_about_image_present(self):
        """ Проверка, что открыто описание файла """
        if self.is_element_present(ArticlePageLocators.FILE_DATA_TITLE):
            self.assert_that_element_present(ArticlePageLocators.FILE_DATA_TITLE, 'Страница описания файла')
        else:
            self.assert_that_element_present(ArticlePageLocators.FILE_DATA_TITLE_2, 'Страница описания файла')

    def assert_that_article_added_to_watchlist(self):
        self.assert_that_element_present(ArticlePageLocators.TEXT_ABOUT_ADD_ARTICLE_TO_WATCHLIST, 'Уведомление о добавлении статьи в список наблюдения')

    def assert_that_talk_present(self):
        """ Проверка, что страница 'Обсуждение' открыта """
        self.assert_that_element_present(DiscussionPageLocators.DISCUSSION_PERSONAL_PAGE_TITLE, 'Страница "Обсуждение"')

    def assert_that_text_about_renaming_page_present(self):
        """ Проверка, что во вкладке 'История' отображается запись о переименовании """
        self.assert_that_element_present(HistoryArticleLocators.TEXT_ABOUT_RENAMING_ARTICLE, 'Текст о переименовании страницы')

    def assert_that_edits_automatically_patrolled(self):
        """ Проверка, что последняя правка автоматически отпатрулирована """
        timer = 0
        while not self.is_element_present(HistoryArticleLocators.TEXT_ABOUT_AUTO_PATROL_CHANGES):
            time.sleep(20)
            self.reload()
            timer += 20
            if timer >= 120:
                break
        self.assert_that_element_present(HistoryArticleLocators.TEXT_ABOUT_AUTO_PATROL_CHANGES, 'Текст "автоматически отпатрулирована"')

    def assert_that_untested_version_from_first_record_present(self):
        """ Проверка, что у первой записи отображается отметка 'Непроверенная версия' """
        self.assert_that_element_present(HistoryArticleLocators.UNTESTED_VERSION_FIRST_RECORD_MARK, "Метка 'Непроверенная версия' у первой записи")

    @allure.step("Переход к подтверждению изменений")
    def move_to_review_change(self):
        self.scroll_to_element(HistoryArticleLocators.WAITING_REVIEW_BUTTON)
        self.waiting_element_and_click(HistoryArticleLocators.WAITING_REVIEW_BUTTON)
        self.create_screenshot()

    @allure.step("Подтверждение версии")
    def confirm_version(self):
        self.waiting_element_and_click(ArticlePageLocators.CONFIRM_VERSION_BUTTON)
        self.create_screenshot()

    def assert_that_patrolled_version_mark_present(self):
        """ Проверка, что метки "патрулированная версия" отображается """
        timer = 0
        while not self.is_element_present(HistoryArticleLocators.TEXT_ABOUT_PATROL_CHANGES):
            timer += 20
            time.sleep(20)
            self.reload()
            if timer > 120:
                self.assert_that_element_present(HistoryArticleLocators.TEXT_ABOUT_PATROL_CHANGES, "Текст о патрулировании изменений")
                break
        self.assert_that_element_present(HistoryArticleLocators.TEXT_ABOUT_PATROL_CHANGES, "Текст о патрулировании изменений")

    @allure.step("Переход к скрытию правок")
    def move_to_hiding_edits(self):
        time.sleep(3)
        self.waiting_element_and_click(HistoryArticleLocators.HIDING_EDITS_BUTTON)
        self.scroll_on_page(200)
        time.sleep(1)
        self.create_screenshot()

    @allure.step("Изменение тегов выбранной версии")
    def change_selected_versions_tags(self):
        self.waiting_element_and_click(HistoryArticleLocators.SELECT_LAST_RECORD_VERSION_CHECKBOX)
        self.waiting_element_and_click(HistoryArticleLocators.CHANGE_SELECTED_VERSIONS_TAGS_BUTTON)
        self.create_screenshot()

    @allure.step("Добавление тегов версии")
    def add_versions_mark(self, tag, cause):
        self.move_to_element_and_send_keys(ArticlePageLocators.TAG_INPUT, tag)
        time.sleep(2)
        self.action.send_keys(Keys.SPACE).perform()
        time.sleep(2)
        self.action.send_keys(Keys.ENTER).perform()
        time.sleep(2)
        self.create_screenshot()
        self.scroll_to_element(ArticlePageLocators.ADD_TAG_CAUSE_INPUT)
        self.element_send_keys(ArticlePageLocators.ADD_TAG_CAUSE_INPUT, cause)
        self.create_screenshot()
        self.waiting_element_and_click(ArticlePageLocators.CONFIRM_ADD_TAG_BUTTON)
        self.create_screenshot()

    def assert_versions_tag_edited(self):
        """ Проверка, что отображается текст об изменении тега версии """
        self.assert_that_element_present(ArticlePageLocators.TEXT_ABOUT_ADDED_TAG, "Текст об изменении тега")

    def assert_that_tag_added_to_change(self, mark_title):
        """ Проверка, что тег версии отображается в правке """
        created_mark = (By.XPATH, f'''//ul[@class="mw-contributions-list"][1]//span[@class="mw-tag-markers"]/span[text()='{mark_title}']''')
        self.assert_that_element_present(created_mark, "Метка в записи о правке")

    @allure.step("Удаление тегов версии")
    def remove_versions_mark(self, tag, cause):
        time.sleep(1)
        created_mark = (By.XPATH, f'''//ul[@class="chosen-choices"]/li/span[text()='{tag}']/parent::li/a''')
        self.waiting_element_and_click(created_mark)
        self.scroll_to_element(ArticlePageLocators.ADD_TAG_CAUSE_INPUT)
        self.element_send_keys(ArticlePageLocators.ADD_TAG_CAUSE_INPUT, cause)
        self.waiting_element_and_click(ArticlePageLocators.CONFIRM_ADD_TAG_BUTTON)
        self.create_screenshot()

    def assert_that_page_stabilized_text_present(self):
        """ Проверка, что на вкладке 'История' отображается текст о стабилизации страницы """
        self.assert_that_element_present(HistoryArticleLocators.TEXT_ABOUT_STABILIZED_PAGE, "Текст об установке настроек стабильной версии")
        timer = 0
        while not self.is_element_present(HistoryArticleLocators.TEXT_ABOUT_AUTO_PATROL_CHANGES):
            time.sleep(10)
            self.reload()
            timer += 10
            if timer > 60:
                break
        time.sleep(2)
        self.create_attachment(f"Время ожидания автопатрулирования: {timer}", "Время ожидания автопатрулирования")
        self.assert_that_element_present(HistoryArticleLocators.TEXT_ABOUT_AUTO_PATROL_CHANGES, "Текст об автопатрулировании правки")

    @allure.step("Переход на страницу правки")
    def move_to_stabilize_page_change(self):
        self.scroll_on_page(150)
        self.scroll_to_element(HistoryArticleLocators.TEXT_ABOUT_AUTO_PATROL_CHANGES)
        self.waiting_element_and_click(HistoryArticleLocators.TEXT_ABOUT_AUTO_PATROL_CHANGES)
        self.create_screenshot()

    def assert_that_stable_version_present(self):
        """ Проверка, что отображается надпись 'Стабильная версия' """
        self.assert_that_element_present(ArticlePageLocators.STABLE_VERSION_TEXT, "Текст о том, что текущая версия - стабильная")

    def assert_that_text_about_request_rename_present(self):
        """ Проверка, что на странице переименования страницы участнике отображается текст о запросе на переименование """
        self.assert_that_element_present(RenamePageLocators.REQUEST_RENAME_TEXT_FROM_SECTION, "Текст о запросе на переименование")

    @allure.step("Переименование персональной страницы участника")
    def rename_personal_page(self, new_name, reason):
        self.scroll_on_page(200)
        time.sleep(1)
        self.create_screenshot()
        self.scroll_to_element(RenamePageLocators.NEW_NAME_INPUT)
        time.sleep(1)
        self.create_screenshot()
        self.clear_element(RenamePageLocators.NEW_NAME_INPUT)
        self.element_send_keys(RenamePageLocators.NEW_NAME_INPUT, new_name)
        self.create_screenshot()
        self.element_send_keys(RenamePageLocators.OTHER_REASON_INPUT, reason)
        self.create_screenshot()
        self.scroll_to_element(RenamePageLocators.CONFIRM_RENAME_BUTTON)
        time.sleep(1)
        self.create_screenshot()
        self.waiting_element_and_click(RenamePageLocators.CONFIRM_RENAME_BUTTON)
        self.create_screenshot()

    @allure.step("Переход на переименованную страницу")
    def move_to_renamed_page(self):
        self.waiting_element_and_click(RenamePageLocators.NEW_LINK_ARTICLE)
        self.create_screenshot()

    @allure.step("Переход на следующую случайную страницу, если предыдущая была защищена")
    def move_on_random_article_without_protect(self, environment) -> NoReturn:
        timer = 0
        while not self.is_element_present(SideBarLocators.PROTECT_ARTICLE):
            self.open_page('/wiki/Служебная:Случайная_страница', environment)
            time.sleep(5)
            timer += 5
            if timer > 20:
                break
        time.sleep(2)
        self.create_screenshot()

    @allure.step("Переход на старое название статьи в истории")
    def move_to_old_title_article_from_history(self):
        self.scroll_to_element(HistoryArticleLocators.MOVE_TO_OLD_TITLE_ARTICLE_LINK)
        time.sleep(2)
        self.create_screenshot()
        self.waiting_element_and_click(HistoryArticleLocators.MOVE_TO_OLD_TITLE_ARTICLE_LINK)
        self.create_screenshot()

    @allure.step("Переход на новое название статьи в истории")
    def move_to_new_title_article_from_history(self):
        self.scroll_to_element(HistoryArticleLocators.MOVE_TO_NEW_TITLE_ARTICLE_LINK)
        time.sleep(2)
        self.waiting_element_and_click(HistoryArticleLocators.MOVE_TO_NEW_TITLE_ARTICLE_LINK)
        self.create_screenshot()

    @allure.step("Переход с переименованой статьи на старое название статьи")
    def move_to_old_title_article_from_renamed_page(self):
        self.waiting_element_and_click(ArticlePageLocators.REDIRECT_FROM_LINK)
        self.create_screenshot()

    @allure.step("Переход на случайную страницу с изображением")
    def move_on_random_article_with_images(self, environment):
        timer = 0
        while not self.is_element_present(ArticlePageLocators.FIRST_IMAGE_FROM_ARTICLE):
            self.open_page('/wiki/Служебная:Случайная_страница', environment)
            time.sleep(2)
            timer += 2
            if timer > 50:
                pytest.skip("Изображений в статьях не обнаружено")
                break
        time.sleep(2)
        self.create_screenshot()

    @allure.step("Переход на случайную страницу с изображением из Рувики.Медиа")
    def move_on_random_article_with_images_from_commons(self, environment):
        timer = 0
        while not self.is_element_present(ArticlePageLocators.VIEW_ON_COMMONS_BUTTON):
            self.open_page('/wiki/Служебная:Случайная_страница', environment)
            time.sleep(2)
            timer += 2
            if timer > 60:
                pytest.skip("Изображений в статьях не обнаружено")
                break
        time.sleep(2)
        self.create_screenshot()

    @allure.step("Пропуск теста, если изображение не из Рувики.Медиа")
    def skip_test_if_image_not_from_commons(self, skip: bool = False) -> str:
        if not self.is_element_present(ArticlePageLocators.VIEW_ON_COMMONS_BUTTON):
            if skip:
                pytest.skip("Обнаруженное изображение не из Рувики.Медиа")
            else:
                return "image not from wiki"

    @allure.step("Переход на случайную статью без изображений")
    def move_on_random_article_without_images(self, environment):
        timer = 0
        while self.is_element_present(ArticlePageLocators.FIRST_IMAGE_FROM_ARTICLE):
            self.open_page('/wiki/Служебная:Случайная_страница', environment)
            time.sleep(5)
            timer += 5
            if timer > 50:
                pytest.skip("Статей без изображений не обнаружено")
                break
        time.sleep(2)
        self.create_screenshot()

    @allure.step("Переход на на страницу с ссылками на загруженное изображение")
    def move_to_uploaded_image_links_list_page(self):
        self.waiting_element_and_click(RenamePageLocators.MOVE_TO_LINKS_LIST_PAGE)
        self.create_screenshot()

    @allure.step("Переход к статье с выбранным изображением")
    def move_to_file_linked_article(self):
        self.waiting_element_and_click(ArticlePageLocators.USING_FILE)
        self.waiting_element_and_click(ArticlePageLocators.SELECT_FIRST_FILE_LINKED_ARTICLE)
        self.create_screenshot()

    @allure.step("Переименование файла")
    def rename_file(self, new_name, reason):
        self.clear_element(RenamePageLocators.NEW_NAME_FILE_INPUT)
        self.element_send_keys(RenamePageLocators.NEW_NAME_FILE_INPUT, new_name)
        self.element_send_keys(RenamePageLocators.RENAME_REASON_INPUT, reason)
        self.create_screenshot()
        self.waiting_element_and_click(RenamePageLocators.CONFIRM_RENAME_FILE_BUTTON)
        self.create_screenshot()
