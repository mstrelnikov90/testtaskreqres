# import allure
# from typing import NoReturn
# import json
# from allure_commons.types import AttachmentType
# from selenium.common.exceptions import NoSuchElementException
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.wait import WebDriverWait
#
#
# class BasePage:
#     def __init__(self, browser, url: str, api_reqres=None) -> None:
#         self.browser = browser
#         self.url = url
#         self.api_reqres = api_reqres
#
#     main_page_url: str = "https://ru.crw083.ru/"
#
#     @staticmethod
#     def create_screenshot(func):
#         def wrapper(*args, **kwargs):
#             self = args[0]
#             func(*args, **kwargs)
#             allure.attach(self.browser.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
#         return wrapper
#
#     @create_screenshot
#     def open(self) -> NoReturn:
#         self.browser.get(self.url)
#
#     def reload(self) -> NoReturn:
#         self.browser.refresh()
#
#     def go_to_first_window(self) -> NoReturn:
#         self.browser.switch_to.window(self.browser.window_handles[0])
#
#     def go_to_second_window(self) -> NoReturn:
#         self.browser.switch_to.window(self.browser.window_handles[1])
#
#     def go_to_third_window(self) -> NoReturn:
#         self.browser.switch_to.window(self.browser.window_handles[2])
#
#     def waiting_element_and_click_with_scroll(self, element_for_scroll: tuple, element: tuple) -> NoReturn:
#         self.browser.execute_script("return arguments[0].scrollIntoView(true);", self.browser.find_element(*element_for_scroll))
#         WebDriverWait(self.browser, timeout=30).until(EC.element_to_be_clickable(element)).click()
#
#     def clear_element(self, element: tuple) -> NoReturn:
#         WebDriverWait(self.browser, timeout=20).until(EC.element_to_be_clickable(element)).clear()
#
#     def element_send_keys(self, element: tuple, data) -> NoReturn:
#         self.browser.find_element(*element).send_keys(data)
#
#     def waiting_element_to_visible(self, element: tuple) -> NoReturn:
#         WebDriverWait(self.browser, timeout=30).until(EC.visibility_of_element_located(element))
#
#     def waiting_element_to_invisible(self, element: tuple) -> NoReturn:
#         WebDriverWait(self.browser, timeout=30).until(EC.invisibility_of_element_located(element))
#
#     def scroll_to_element(self, element: tuple) -> NoReturn:
#         self.browser.execute_script("return arguments[0].scrollIntoView(true);", self.browser.find_element(*element))
#
#     @staticmethod
#     def create_attachment(message: str, name_attach: str) -> NoReturn:
#         allure.attach(message, name=name_attach, attachment_type=AttachmentType.JSON)
#
#     def is_element_present(self, element: tuple) -> bool:
#         try:
#             self.browser.find_element(*element)
#         except NoSuchElementException:
#             return False
#         return True
#
#     def assert_status_code(self, expected_status_code: tuple, status_code_from_request: int) -> NoReturn:
#         self.waiting_element_to_visible(expected_status_code)
#         assert self.is_element_present(expected_status_code)
#         assert self.get_attribute(expected_status_code) == str(status_code_from_request)
#
#     def assert_response_body(self, expected_response_body: tuple, response_body_from_request: dict) -> NoReturn:
#         self.waiting_element_to_visible(expected_response_body)
#         assert self.is_element_present(expected_response_body)
#         assert json.loads(self.get_attribute(expected_response_body)) == response_body_from_request
#
#     def assert_response_body_with_changing_values(self, expected_response_body: tuple, response_body_from_request: dict) -> NoReturn:
#         self.waiting_element_to_visible(expected_response_body)
#         assert self.is_element_present(expected_response_body)
#         assert json.loads(self.get_attribute(expected_response_body)).keys() == response_body_from_request.keys()
#
#     def get_attribute(self, element: tuple) -> str:
#         return self.browser.find_element(*element).text


# import time
# import allure
# import os
# import socket
# from typing import NoReturn
# from allure_commons.types import AttachmentType
# from selenium.webdriver.common.action_chains import ActionChains
# from selenium.common.exceptions import (NoSuchElementException, ElementClickInterceptedException, StaleElementReferenceException)
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.wait import WebDriverWait
# from .locators import ArticlePageLocators, DebugBarLocators
# from selenium.webdriver import Keys


# class BasePage:
#     def __init__(self, browser, page: str = "") -> None:
#         self.browser = browser
#         self.action = ActionChains(browser)
#         self.page = page
#
#     endpoint = "/wiki/"
#     wiki_preprod_data_url = "https://data.crw083.ru"
#     wiki_preprod_commons_url = "https://commons.crw083.ru"
#     wiki_preprod_meta_url = "https://meta.crw083.ru"
#     wiki_preprod_url = "https://ru.crw083.ru"
#
#     def create_screenshot(self):
#         """ Создание скриншота """
#         allure.attach(self.browser.get_screenshot_as_png(), name='Screenshot', attachment_type=AttachmentType.PNG)
#
#     @allure.step("Открытие страницы по URL")
#     def open_specified_url(self, full_url):
#         self.browser.get(full_url)
#         self.create_screenshot()
#
#     @allure.step("Открытие страницы")
#     def open(self) -> NoReturn:
#         """ Открытие браузера и главной страницы """
#         url = f"{self.wiki_preprod_url}{self.page}"
#         self.browser.get(url)
#         self.create_attachment(url, 'URL')
#         self.create_screenshot()
#
#     @allure.step("Открытие страницы {page_url}")
#     def open_page(self, page_url: str = ""):
#         """ Открытие браузера и указанной страницы """
#         self.browser.get(self.wiki_preprod_url + page_url)
#         time.sleep(3)
#         if self.is_element_present(ArticlePageLocators.CLOSE_WELCOME_CREATE_POPUP_BUTTON):
#             self.waiting_element_and_click(ArticlePageLocators.CLOSE_WELCOME_CREATE_POPUP_BUTTON)
#         self.create_attachment(self.wiki_preprod_url + page_url, 'URL')
#         self.create_screenshot()
#
#     @allure.step("Открытие заглавной страницы 'Рувики.Данные'")
#     def open_ruwiki_data(self):
#         self.browser.get(self.wiki_preprod_data_url)
#         self.create_attachment(self.wiki_preprod_data_url, 'URL')
#         self.create_screenshot()
#
#     @allure.step("Открытие заглавной страницы 'Рувики.Медиа'")
#     def open_wiki_commons(self):
#         self.browser.get(self.wiki_preprod_commons_url)
#         self.create_attachment(self.wiki_preprod_commons_url, 'URL')
#         self.create_screenshot()
#
#     @allure.step("Открытие заглавной страницы 'Рувики - Мета'")
#     def open_wiki_meta(self):
#         self.browser.get(self.wiki_preprod_meta_url)
#         self.create_attachment(self.wiki_preprod_meta_url, 'URL')
#         self.create_screenshot()
#
#     @allure.step("Открытие страницы {page_url} Рувики.Медиа")
#     def open_wiki_commons_page(self, page_url):
#         self.browser.get(self.wiki_preprod_commons_url + page_url)
#         self.create_attachment(self.wiki_preprod_commons_url + page_url, 'URL')
#         self.create_screenshot()
#
#     @allure.step("Открытие страницы {page_url} Рувики.Данные")
#     def open_wiki_data_page(self, page_url):
#         self.browser.get(self.wiki_preprod_data_url + page_url)
#         self.create_attachment(self.wiki_preprod_data_url + page_url, 'URL')
#         self.create_screenshot()
#
#     def save_url_article(self, article_title: str):
#         """ Сохранение URL статьи во вложения """
#         self.create_attachment(self.wiki_preprod_url + self.endpoint + article_title, "URL Статьи")
#
#     def save_current_ip(self):
#         """ Сохранение текущего IP устройства """
#         hostname = socket.gethostname()
#         local_ip = socket.gethostbyname(hostname)
#         self.create_attachment(local_ip, "IP")
#         return local_ip
#
#     def reload(self) -> NoReturn:
#         """ Перезагрузка браузера """
#         self.browser.refresh()
#
#     def back_to_page(self) -> NoReturn:
#         """ Возврат на предыдущую страницу """
#         self.browser.execute_script("window.history.go(-1)")
#         time.sleep(3)
#
#     def go_to_first_window(self) -> NoReturn:
#         """ Переход на первую вкладку браузера """
#         self.browser.switch_to.window(self.browser.window_handles[0])
#
#     def go_to_second_window(self) -> NoReturn:
#         """ Переход на вторую вкладку браузера """
#         self.browser.switch_to.window(self.browser.window_handles[1])
#
#     def go_to_third_window(self) -> NoReturn:
#         """ Переход на третью вкладку браузера """
#         self.browser.switch_to.window(self.browser.window_handles[2])
#
#     def clear_element(self, element: tuple) -> NoReturn:
#         """ Очистить поле ввода """
#         try:
#             WebDriverWait(self.browser, timeout=20).until(EC.element_to_be_clickable(element)).clear()
#         except ElementClickInterceptedException:
#             time.sleep(2)
#             WebDriverWait(self.browser, timeout=20).until(EC.element_to_be_clickable(element)).clear()
#
#     def element_send_keys(self, element: tuple, data) -> NoReturn:
#         """ Ввод данных в поле ввода """
#         self.browser.find_element(*element).send_keys(data)
#
#     def move_to_element_and_send_keys(self, element: tuple, data) -> NoReturn:
#         """ Наведение на поле ввода и ввод данных """
#         self.action.scroll_to_element(self.browser.find_element(*element)).perform()
#         time.sleep(0.5)
#         self.action.move_to_element(self.browser.find_element(*element)).click().send_keys(data).perform()
#
#     def waiting_element_and_click(self, element: tuple) -> NoReturn:
#         """ Скрытие нижнего служебного меню, ожидание кликабельности элемента и клик по элементу """
#         if self.is_element_present(DebugBarLocators.BOTTOM_BAR):
#             self.browser.find_element(*DebugBarLocators.CONSOLE_SECTION).click()
#             time.sleep(2)
#             while self.is_element_present(DebugBarLocators.BOTTOM_BAR):
#                 time.sleep(2)
#                 self.browser.find_element(*DebugBarLocators.CONSOLE_SECTION).click()
#                 time.sleep(2)
#         try:
#             WebDriverWait(self.browser, timeout=20).until(EC.element_to_be_clickable(element)).click()
#         except ElementClickInterceptedException:
#             time.sleep(2)
#             WebDriverWait(self.browser, timeout=20).until(EC.element_to_be_clickable(element)).click()
#         except StaleElementReferenceException:
#             time.sleep(2)
#             WebDriverWait(self.browser, timeout=20).until(EC.element_to_be_clickable(element)).click()
#
#     def move_to_element_and_click(self, element: tuple) -> NoReturn:
#         """ Скрытие нижнего служебного меню, наведение на элемент и клик по элементу """
#         while self.is_element_present(DebugBarLocators.BOTTOM_BAR):
#             time.sleep(2)
#             self.browser.find_element(*DebugBarLocators.CONSOLE_SECTION).click()
#             time.sleep(2)
#         self.action.move_to_element(self.browser.find_element(*element)).click().perform()
#
#     def waiting_element_to_visible(self, element: tuple) -> NoReturn:
#         """ Ожидание появления элемента """
#         WebDriverWait(self.browser, timeout=20).until(EC.visibility_of_element_located(element))
#
#     def waiting_element_to_invisible(self, element: tuple) -> NoReturn:
#         """ Ожидание исчезновения элемента """
#         WebDriverWait(self.browser, timeout=20).until(EC.invisibility_of_element_located(element))
#
#     def scroll_to_element(self, element: tuple) -> NoReturn:
#         """ Скролл до элемента """
#         time.sleep(2)
#         self.browser.execute_script("return arguments[0].scrollIntoView(true);", self.browser.find_element(*element))
#         time.sleep(2)
#         self.browser.execute_script("return arguments[0].scrollIntoView(true);", self.browser.find_element(*element))
#         time.sleep(2)
#
#     def scroll_on_page(self, value):
#         """
#         Скролл по странице:
#         Положительное значение - скролл вниз
#         Отрицательное значение - скролл вверх
#         """
#         self.browser.execute_script(f'window.scrollBy(0, {value});')
#         time.sleep(2)
#
#     def scroll_with_keys(self, value: int, sideways: str):
#         """ Скролл через клавиши """
#         time.sleep(3)
#         if sideways == 'up':
#             for key in range(value):
#                 self.action.send_keys(Keys.ARROW_UP).perform()
#                 time.sleep(0.1)
#                 self.action.send_keys(Keys.PAGE_UP).perform()
#         elif sideways == 'down':
#             for key in range(value):
#                 self.action.send_keys(Keys.ARROW_DOWN).perform()
#                 time.sleep(0.1)
#                 self.action.send_keys(Keys.PAGE_DOWN).perform()
#         time.sleep(3)
#
#     def scroll_on_page_with_actions(self, value):
#         """
#         Скролл по странице:
#         Положительное значение - скролл вниз
#         Отрицательное значение - скролл вверх
#         """
#         time.sleep(3)
#         self.action.scroll_by_amount(0, value)
#         time.sleep(3)
#
#     @allure.step('Получение значения cookie {cookie_name}')
#     def get_cookie(self, cookie_name):
#         """ Получение Cookie """
#         cookie = self.browser.get_cookie(cookie_name).get('value')
#         self.create_attachment(cookie, f"Значение cookie {cookie_name}")
#         return cookie
#
#     def clear_cookie(self):
#         """ Очистка Cookie """
#         self.browser.delete_all_cookies()
#
#     @staticmethod
#     def create_attachment(message, name_attach: str) -> NoReturn:
#         """ Создание вложения """
#         allure.attach(message, name=name_attach, attachment_type=AttachmentType.JSON)
#
#     def is_element_present(self, element: tuple) -> bool:
#         """ Условие, что элемент есть на странице """
#         try:
#             self.browser.find_element(*element)
#         except NoSuchElementException:
#             return False
#         return True
#
#     def get_text_from_element(self, element: tuple) -> str:
#         """ Получение текста из элемента """
#         return self.browser.find_element(*element).text
#
#     def get_attribute_from_element(self, element: tuple, attribute_name: str) -> str:
#         """ Получение атрибута элемента """
#         return self.browser.find_element(*element).get_attribute(attribute_name)
#
#     @allure.step('Проверка, что {element_name} отображается')
#     def assert_that_element_present(self, element: tuple, element_name: str, timeout: int = 5):
#         """ Проверка, что элемент есть на странице """
#         self.create_screenshot()
#         if not self.is_element_present(element):
#             time.sleep(timeout)
#         assert self.is_element_present(element), f"Элемент {element_name} не отображается"
#         self.create_screenshot()
#
#     @allure.step('Проверка, что {element_name} не отображается')
#     def assert_that_element_not_present(self, element: tuple, element_name: str):
#         """ Проверка, что элемента нет на странице """
#         self.create_screenshot()
#         assert not self.is_element_present(element), f"Элемент {element_name} отображается"
#         self.create_screenshot()
#
#     @allure.step('Проверка, что страница {page_name} открыта')
#     def assert_that_page_present(self, page_name: str | None):
#         """ Проверка, что открыта указанная страница """
#         browser_title = str(self.browser.title)
#         page_name = str(page_name)
#         lower_browser_title = browser_title.lower()
#         if "," in lower_browser_title:
#             lower_browser_title = lower_browser_title[:lower_browser_title.index(",")]
#         lower_page_name = page_name.lower()
#         self.create_attachment(lower_browser_title, "Название текущей страницы")
#         self.create_attachment(lower_page_name, "Название указанной страницы")
#         self.create_screenshot()
#         if lower_page_name in lower_browser_title:
#             assert lower_page_name in lower_browser_title, f"Страница {lower_page_name} не открыта"
#         else:
#             assert lower_browser_title in lower_page_name, f"Страница {lower_page_name} не открыта"
#         self.create_screenshot()
#
#     @allure.step('Проверка, что текст элемента равен эталонному значению {reference}')
#     def assert_that_text_from_element_correct(self, element: tuple, reference):
#         """ Проверка, что текст элемента (compared_element) равен эталонному значению (reference) """
#         compared_element = self.get_text_from_element(element)
#         self.create_attachment(compared_element, "Текст элемента")
#         self.create_attachment(reference, "Эталонное значение")
#         assert reference in compared_element, f"Текст элемента {compared_element} не равен эталонному значению {reference}"
#
#     @allure.step('Проверка, что значения равны')
#     def assert_that_values_equals(self, value1, value2):
#         self.create_attachment(value1, 'Значение 1')
#         self.create_attachment(value2, 'Значение 2')
#         assert value1 == value2, f"Значения {value1} и {value2} не равны"
#
#     @allure.step('Проверка, что значения не равны')
#     def assert_that_values_not_equals(self, value1, value2):
#         self.create_attachment(value1, 'Значение 1')
#         self.create_attachment(value2, 'Значение 2')
#         assert value1 != value2, f"Значения {value1} и {value2} равны"
#
#     def assert_that_article_has_reference_text(self, article_text, reference_text):
#         self.create_attachment(reference_text, 'Эталонный текст')
#         self.create_attachment(article_text, 'Текст из статьи')
#         if article_text != reference_text:
#             return "Текст не совпадает"
#
#     @allure.step('Запись в файл')
#     def write_to_file(self, path, text, mode):
#         self.create_attachment(os.getcwd() + path, 'Путь к файлу')
#         self.create_attachment(text, 'Текст для записи в файл')
#         with open(os.getcwd() + path, mode, encoding="utf-8") as file:
#             file.write(text)
#
#     @allure.step('Очистка файла')
#     def cleaning_file(self, path):
#         self.create_attachment(os.getcwd() + path, 'Путь к файлу')
#         with open(os.getcwd() + path, 'w') as file:
#             file.write("")
#
#     @staticmethod
#     def delete_failed_data_from_file():
#         with open(os.getcwd() + '/text.txt', 'r', encoding="utf-8") as monitoring_file:
#             lines = monitoring_file.readlines()
#             lines = lines[:-1]
#         with open(os.getcwd() + '/text.txt', 'w', encoding="utf-8") as monitoring_file:
#             monitoring_file.writelines(lines)
#
#     @allure.step('Сохранение URL')
#     def return_current_url(self):
#         self.create_screenshot()
#         self.create_attachment(self.browser.current_url, "Текущий URL")
#         return self.browser.current_url
#
#     @allure.step('Сохранение названия страницы')
#     def return_browser_title(self):
#         title = self.browser.title
#         self.create_screenshot()
#         self.create_attachment(title, "Текущее название страницы")
#         return title
#
#     @allure.step('Проверка, что значения в БД присутствуют')
#     def assert_that_value_exist_from_database(self, value_from_db):
#         assert value_from_db != [], "Указанное значение не присутствует в БД"


from playwright.sync_api import Page, expect
from allure_commons.types import AttachmentType
import allure
from typing import NoReturn
import time


class BasePage:

    def __init__(self, page, additional_url) -> None:
        self.page: Page = page
        self.additional_url = additional_url

    endpoint = "/wiki/"
    wiki_preprod_data_url = "https://data.crw083.ru"
    wiki_preprod_commons_url = "https://commons.crw083.ru"
    wiki_preprod_meta_url = "https://meta.crw083.ru"
    wiki_preprod_url = "https://ru.crw083.ru"

    @allure.step("Открытие страницы")
    def open(self) -> NoReturn:
        """ Открытие браузера и главной страницы """
        url = f"{self.wiki_preprod_url}{self.additional_url}"
        self.page.goto(url)
        self.create_attachment(url, 'URL')
        self.create_screenshot()

    @allure.step("Открытие страницы {page_url}")
    def open_page(self, page_url: str = ""):
        """ Открытие браузера и указанной страницы """
        self.page.goto(self.wiki_preprod_url + page_url)
        time.sleep(3)
        self.create_attachment(self.wiki_preprod_url + page_url, 'URL')
        self.create_screenshot()

    def create_screenshot(self) -> NoReturn:
        """ Create screenshot for allure report """
        allure.attach(self.page.screenshot(), name='Screenshot', attachment_type=AttachmentType.PNG)

    @staticmethod
    def create_attachment(attach, attach_name) -> NoReturn:
        """ Create attachments for allure report """
        allure.attach(attach, name=attach_name, attachment_type=AttachmentType.TEXT)

    def wait_for_timeout(self, timeout: float):
        """ Wait for timeout """
        self.page.wait_for_timeout(timeout)

    def get_text_from_element(self, selector: str) -> str:
        """ Получение текста из элемента """
        return self.page.locator(f"xpath={selector}").text_content()

    def get_attribute_from_element(self, selector: str, attribute_name: str) -> str:
        """ Получение атрибута элемента """
        return self.page.locator(f"xpath={selector}").get_attribute(attribute_name)

    def reload(self) -> NoReturn:
        """ Reload page """
        self.page.reload()
        self.wait_for_timeout(1000)
        self.create_screenshot()

    def waiting_element_and_click(self, selector: str):
        """ Waiting display element and click for him """
        self.page.wait_for_selector(f"xpath={selector}").click()
        self.wait_for_timeout(1000)

    def move_to_element_and_click(self, selector: str):
        """ Waiting display element and click for him """
        self.page.wait_for_selector(f"xpath={selector}").click()
        self.wait_for_timeout(1000)

    def element_send_keys(self, selector: str, inserted_value: str):
        """ Insert text into text field """
        self.page.wait_for_selector(f"xpath={selector}").click()
        self.page.fill(f"xpath={selector}", inserted_value)
        self.wait_for_timeout(1000)

    def press_key(self, selector: str, key: str):
        """ Press key """
        self.page.locator(f"xpath={selector}").first.press(key)

    def hover_on_element(self, selector: str):
        """ Hover on element """
        self.page.hover(f"xpath={selector}")

    def scroll_to_element(self, selector: str):
        """ Scroll to element """
        self.page.locator(f"xpath={selector}").scroll_into_view_if_needed()

    def scroll_on_page(self, value: float):
        """ Scroll on page """
        self.page.mouse.wheel(0.0, value)

    def is_element_present(self, selector: str) -> bool:
        """ Method for if-else constructions """
        return self.page.locator(f"xpath={selector}").is_visible()

    @allure.step('Проверка, что {element_name} отображается')
    def assert_that_element_present(self, selector: str, element_name: str):
        """ Assertion that element visible """
        expect(self.page.locator(f"xpath={selector}"), f"Элемент '{element_name}' не отображается").to_be_visible()
        self.create_screenshot()

    @allure.step('Проверка, что {element_name} отображается')
    def assert_that_element_not_present(self, selector: str, element_name: str):
        """ Assertion that element visible """
        expect(self.page.locator(f"xpath={selector}"), f"Элемент '{element_name}' не отображается").not_to_be_visible()
        self.create_screenshot()

    @allure.step('Проверка, что {element_name} включен')
    def assert_that_element_checked(self, selector: str, element_name: str):
        """ Assertion that element checked """
        expect(self.page.locator(f"xpath={selector}"), f"Элемент '{element_name}' не включен").to_be_checked()
        self.create_screenshot()

    def waiting_for_disappear_loader(self, loader: str, max_waiting_timeout: int):
        """ Waiting loader """
        timer = 0
        while self.is_element_present(loader):
            self.wait_for_timeout(5000)
            timer += 5
            if timer > max_waiting_timeout:
                break
        self.wait_for_timeout(1000)
        while self.is_element_present(loader):
            self.wait_for_timeout(5000)
            timer += 5
            if timer > max_waiting_timeout:
                break
