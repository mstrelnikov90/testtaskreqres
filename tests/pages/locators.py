# from selenium.webdriver.common.by import By


# class MainPageLocators:
#
#     SELECT_LIST_USERS = "//div[@class='endpoints']/ul/li[1]"
#     SELECT_SINGLE_USER = "//div[@class='endpoints']/ul/li[2]"
#     SELECT_SINGLE_USER_NOT_FOUND = "//div[@class='endpoints']/ul/li[3]"
#     SELECT_LIST_RESOURCE = "//div[@class='endpoints']/ul/li[4]"
#     SELECT_SINGLE_RESOURCE = "//div[@class='endpoints']/ul/li[5]"
#     SELECT_SINGLE_RESOURCE_NOT_FOUND = "//div[@class='endpoints']/ul/li[6]"
#     SELECT_CREATE_USER = "//div[@class='endpoints']/ul/li[7]"
#     SELECT_UPDATE_PUT_USER = "//div[@class='endpoints']/ul/li[8]"
#     SELECT_UPDATE_PATCH_USER = "//div[@class='endpoints']/ul/li[9]"
#     SELECT_DELETE_USER = "//div[@class='endpoints']/ul/li[10]"
#     SELECT_REGISTER_SUCCESSFUL_USER = "//div[@class='endpoints']/ul/li[11]"
#     SELECT_REGISTER_UNSUCCESSFUL_USERS = "//div[@class='endpoints']/ul/li[12]"
#     SELECT_LOGIN_SUCCESSFUL_USER = "//div[@class='endpoints']/ul/li[13]"
#     SELECT_LOGIN_UNSUCCESSFUL_USER = "//div[@class='endpoints']/ul/li[14]"
#     SELECT_DELAYED_RESPONSE = "//div[@class='endpoints']/ul/li[15]"
#     RESPONSE_STATUS = "//span[@data-key='response-code']"
#     RESPONSE_BODY = "//pre[@data-key='output-response']"


# # # #

class AuthorizationPageLocators:

    SIGN_IN_TITLE = '''//h1[text(='Вход']'''
    LOGIN_INPUT = '//div[@id="userloginForm"]//input[@class="loginText mw-ui-input"]'
    PASSWORD_INPUT = '//div[@id="userloginForm"]//input[@class="loginPassword mw-ui-input"]'
    SUBMIT_BUTTON = '//div[contains(@class, "mw-content-container"]//button[@type="submit"]'
    SKIP_BUTTON = '//div[@id="userloginForm"]//button[@value="Пропустить"]'  # Кнопка 'Пропустить' при ошибке длины пароля
    CONFIRM_ACCOUNT_TITLE = '''//h1[text(='Подтвердите свою личность']'''  # Страница подтверждения личности
    EDIT_PASSWORD_INPUT = '//input[@name="password"]'  # Поле для ввода нового пароля при изменении пароля
    CONFIRM_EDIT_PASSWORD_INPUT = '//input[@name="retype"]'  # Поле для повторного ввода нового пароля при изменении пароля


class TopBarLocators:

    SIGN_IN_BUTTON = '//li[@id="pt-login-2"]/a'
    LOGO_LINK = '//a[contains(@class, "mw-logo"]'
    MONOBOOK_THEME_LOGO_LINK = '''//body[contains(@class, 'monobook']//a[contains(@class, "logo"]'''
    RUWIKI_THEME_LOGO_LINK = '''//body[contains(@class, 'skin-ruwiki']//a[contains(@class, "mw-logo"]'''
    YENISEY_THEME_LOGO_LINK = '''//body[contains(@class, 'skin-yenisey']//a[contains(@class, "mw-logo"]'''
    MONOBOOK_THEME_PERSONAL_INSTRUMENTS = '''//h3[text(='Персональные инструменты']/parent::div[@class="portlet"]'''
    RUWIKI_THEME_PERSONAL_INSTRUMENTS = '''//body[contains(@class, 'skin-ruwiki']//div[@id="p-personal"]'''
    YENISEY_THEME_PERSONAL_INSTRUMENTS = '''//body[contains(@class, 'skin-yenisey']//div[@id="p-personal"]'''
    OPEN_PROFILE_ACTIONS_BUTTON = '//li[@id="pt-userpage-2"]/a'
    OPEN_PROFILE_ACTIONS_BUTTON_COMMONS = '//div[@id="yenisey-user-links-dropdown"]/label'
    PERSONAL_DATA = '//li[@id="pt-userpage"]/a'
    DRAFT = '''//span[text(='Черновик']/parent::a/parent::li[@id="pt-sandbox"]'''
    SETTINGS = '''//span[text(='Настройки']/parent::a/parent::li[@id="pt-preferences"]'''
    CONTRIBUTION = '''//span[text(='Вклад']/parent::a/parent::li[@id="pt-mycontris"]'''
    WATCHLIST = '//li[@id="pt-watchlist"]/a'
    LOGOUT_BUTTON = '//li[@id="pt-logout"]/a'

    CREATE_ACCOUNT = '//li[@id="pt-createaccount-2"]/a'
    ACCOUNT_NAME = '//li[@id="pt-userpage-2"]/a'
    ALERTS = '//li[@id="pt-notifications-alert"]/a'
    ALL_ALERTS = '''//div[@class="mw-echo-ui-overlay"]/div[2]//span[text(='Все уведомления']/parent::a'''
    NOTIFICATIONS = '//li[@id="pt-notifications-notice"]/a'
    ALL_NOTIFICATIONS = '//div[4]/div[@class="oo-ui-popupWidget-popup"]//span/span[1]/a'
    SELECT_AS_UNREAD = '''//span[text(='Уведомления']/parent::div/parent::div/div[2]/div/a[1]/div[2]/span/a'''  # Пометить, как непрочитанное уведомление на странице 'Все уведомления'
    NOTIFICATION_ABOUT_GRATITUDE = '''//div[contains(text(, 'поблагодарил вас за правку на странице']'''
    NOTIFICATION_ABOUT_EDIT_TALK_MEMBER_PAGE = '''//strong[text(='вашей странице обсуждения']'''
    COUNT_1_UNREAD_NOTIFICATIONS = '//a[@data-counter-num="1"]'
    COUNT_0_UNREAD_NOTIFICATIONS = '//a[@data-counter-num="0"]'
    COUNT_N_UNREAD_NOTIFICATIONS = '''//a[contains(@data, '-counter-num']'''
    SELECT_ALL_AS_READ = '''//span[text(='Уведомления']/parent::div/span[5]/a'''
    MOVE_TO_NOTIFICATIONS_SETTINGS = '''//span[text(='Уведомления']/parent::div/parent::div/div[3]//span/span[2]/a'''  # Кнопка 'Настройки' в окне уведомлений
    COLOR_THEME_BUTTON = '//div[@data-selector="theme-picker__opener"]'
    SELECT_WIDE_FORMAT_THEME = '//label[@title="Переключить формат"]/div'
    SELECT_SMALL_FONT_THEME = '//label[@title="Мелкий"]/div'
    SELECT_LARGE_FONT_THEME = '//label[@title="Крупный"]/div'
    SERIF_DARK_COLOR_THEME = '//label[@title="Шрифт с засечками"]/div'
    SELECT_PAPER_COLOR_THEME = '//label[@title="Песочная тема"]/div'
    SELECT_DARK_COLOR_THEME = '//label[@title="Темная тема"]/div'

    SEARCH_ARTICLE_INPUT = '//form[@id="searchform"]//input[@name="search"]'  # Поле ввода названия статьи для поиска
    SEARCH_ARTICLE_BUTTON = '//form[@id="searchform"]//button'  # Кнопка 'Найти'


class MainPageLocators:

    MAIN_PAGE_TITLE = '''//span[text(='РУВИКИ — новая интернет-энциклопедия ']'''
    WIDE_FORMAT_PAGE = '''//html[contains(@class, 'theme-width-wide-enabled']'''
    SMALL_FONT_PAGE = '''//html[contains(@class, 'theme-size-small-enabled']'''
    LARGE_FONT_PAGE = '''//html[contains(@class, 'theme-size-large-enabled']'''
    SERIF_FONT_PAGE = '''//html[contains(@class, 'theme-font-paper-enabled']'''
    PAPER_COLOR_THEME = '''//html[contains(@class, 'theme-color-paper-enabled']'''
    DARK_COLOR_THEME = '''//html[contains(@class, 'theme-color-dark-enabled']'''
    RECOMMENDATIONS_SECTION = '//h3/span[@id="Рекомендации_для_Вас"]'
    FIRST_WIDGET_FROM_RECOMMENDATIONS = '//div[@class="recommendations"]//div[@class="article-card"][1]//a'
    FIRST_WIDGET_FROM_RECOMMENDATIONS_TITLE = '//div[@class="recommendations"]//div[@class="article-card"][1]//div[@class="article-card__block-title"]'
    SECOND_WIDGET_FROM_RECOMMENDATIONS = '//div[@class="recommendations"]//div[@class="article-card"][2]//a'
    SECOND_WIDGET_FROM_RECOMMENDATIONS_TITLE = '//div[@class="recommendations"]//div[@class="article-card"][2]//div[@class="article-card__block-title"]'
    DONT_SHOW_AGAIN_BUTTON = '//div[@class="recommendations"]//div[@class="article-card"][1]/div/div[1]/span'
    CONFIRM_SHOW_AGAIN_BUTTON = '//div[@class="article-card__categorys-tippy-top"]'
    FIRST_WIDGET_WITH_IMAGE = '//div[@class="recommendations"]//div[@class="article-card"][1]//div[@class="article-card__block-image"]'
    SECOND_WIDGET_WITH_IMAGE = '//div[@class="recommendations"]//div[@class="article-card"][2]//div[@class="article-card__block-image"]'
    THIRD_WIDGET_WITH_IMAGE = '//div[@class="recommendations"]//div[@class="article-card"][3]//div[@class="article-card__block-image"]'
    FORTH_WIDGET_WITH_IMAGE = '//div[@class="recommendations"]//div[@class="article-card"][4]//div[@class="article-card__block-image"]'


class SideBarLocators:

    # LEFT SIDE BAR LOCATORS
    OPEN_LEFT_SIDE_BAR_WITH_AUTHORIZATION = '//div[@class="mw-header__left"]/div'
    OPEN_LEFT_SIDE_BAR_WITH_AUTHORIZATION_DATA = '//label[@id="yenisey-main-menu-dropdown-label"]'
    MONOBOOK_THEME_LEFT_SIDE_BAR = '''//body[contains(@class, 'monobook']//div[@id="sidebar"]'''
    RUWIKI_THEME_LEFT_SIDE_BAR = '''//body[contains(@class, 'skin-ruwiki']//div[@class="mw-header__left"]'''
    YENISEY_THEME_LEFT_SIDE_BAR = '''//body[contains(@class, 'skin-yenisey']//input[@aria-label="Главное меню"]'''
    MAIN_PAGE_ARTICLE = '//div[@class="tippy-content"]//div[@id="ruwiki-main-menu"]//li[@id="n-mainpage-description"]/a'  # Кнопка 'Заглавная страница'
    CONTENT_ARTICLE = '//div[@class="tippy-content"]//div[@id="ruwiki-main-menu"]//li[@id="n-content"]/a'  # Ссылка Содержание
    CONTENT_TITLE_ARTICLE = '''//h1/span[text(='Рувики' and 'Содержание']'''
    FRESH_EDIT_ARTICLE = '//div[@class="tippy-content"]//div[@id="ruwiki-main-menu"]//li[@id="n-recentchanges"]/a'  # Ссылка Свежие правки
    NEW_PAGES_ARTICLE = '//div[@class="tippy-content"]//div[@id="ruwiki-main-menu"]//li[@id="n-newpages"]/a'  # Ссылка Новые страницы
    REFERENCE_ARTICLE = '//div[@class="tippy-content"]//div[@id="ruwiki-main-menu"]//li[@id="n-help"]/a'  # Ссылка Справка
    REFERENCE_TITLE_ARTICLE = '''//h1/span[text(='Рувики' and 'Справка']'''

    # RIGHT SIDE BAR LOCATORS
    INSTRUMENTS = '''//div[@class="page-tools-tippy__btn"]/span[text(='Инструменты']'''
    RU_DATA_ELEMENT = '''//div[@class="tippy-content"]//span[text(='Элемент РУВИКИ.Данных']/parent::a'''
    RENAME_ARTICLE = '//div[@class="tippy-content"]//li[@id="ca-move"]/a'  # Кнопка 'Переименовать'
    PROTECT_ARTICLE = '//div[@class="tippy-content"]//li[@id="ca-protect"]/a'  # Кнопка 'Защитить'
    UNPROTECT_ARTICLE = '//div[@class="tippy-content"]//li[@id="ca-unprotect"]/a'  # Кнопка 'Изменить защиту'
    DELETE_ARTICLE = '//div[@class="tippy-content"]//li[@id="ca-delete"]/a'  # Кнопка 'Удалить'
    UNDELETE_ARTICLE = '''//div[@class="tippy-content"]//li[@id="ca-undelete"]/a'''  # Кнопка 'Восстановить'
    STABILIZE_ARTICLE = '//div[@class="tippy-content"]//li[@id="ca-stabilize"]/a'
    UPLOAD_FILE_ARTICLE = '//div[@class="tippy-content"]//li[@id="t-upload"]/a'
    SERVICE_PAGES_ARTICLE = '//div[@class="tippy-content"]//li[@id="t-specialpages"]/a'
    # DATA SIDE BAR LOCATORS
    CREATE_NEW_ELEMENT = '''//span[text(='Создать новый элемент']/parent::a'''
    RENAME_ARTICLE_DATA = '//li[@id="ca-move"]/a'  # Кнопка 'Переименовать'
    CHANGE_PROTECT_SETTINGS = '''//div[@id="yenisey-page-tools-unpinned-container"]/div/div[2]/div[2]/ul/li[9]'''  # Настройка Изменить защиту
    DELETE_ARTICLE_DATA = '//li[@id="ca-delete"]/a'  # Кнопка 'Удалить'
    UNDELETE_ARTICLE_DATA = '''//li[@id="ca-undelete"]/a'''  # Кнопка 'Восстановить'


class ArticlePageLocators:

    # ARTICLE LOCATORS
    FAVOURITE_ARTICLE_CATEGORY = '''//div[text(='Эта статья входит в число ']/a[text(='избранных статей']'''
    SEE_ALSO_SECTION = '''//h4[text(='Смотрите также']'''  # Секция 'Смотрите также'
    FIRST_WIDGET_FROM_SEE_ALSO_SECTION = '//div[@class="relevant-articles"]//div[@class="swiper-wrapper"]/div[1]//a'
    CREATE_ARTICLE_TITLE = '''//h1[contains(text(, 'Создание страницы']'''
    ERROR_ARTICLE_TITLE = '''//h1[text(='Ошибка прав доступа']'''
    FILE_DATA_TITLE = '''//h1/span[text(='Файл']'''  # Название страницы описания файла
    FILE_DATA_TITLE_2 = '''//h1/span[text(='File']'''  # Название страницы описания файла
    STABLE_VERSION_TEXT = '''//a[text(='Стабильная версия']'''
    REDIRECT_FROM_LINK = '//span[@class="mw-redirectedfrom"]/a'  # Текст 'Перенаправлено с'
    TITLE_ARTICLE_1 = '//div[@class="article"]//h1'
    TITLE_ARTICLE_2 = '//div[@class="article"]//h1/i'
    TITLE_ARTICLE_3 = '//h1'
    RENAMED_FILE_TITLE = '''//h1[contains(text(, 'Renamed']'''
    RENAMED_FILE_TITLE_2 = '''//h1/span[contains(text(, 'Renamed']'''
    FIRST_SECTION_TEXT_ARTICLE = '''//div[@class="article"]//div[@class="vue-article-header"]/div[contains(@class, 'vue-article-header__text']/p'''
    FIRST_SECTION_SECOND_BLOCK_TEXT_ARTICLE = '''//div[@class="article"]//div[@class="vue-article-header"]/div[contains(@class, 'vue-article-header__text']/p[2]'''
    SECOND_SECTION_TEXT_ARTICLE = '//div[@class="article"]//div[@class="vue-article-body"][1]//p'
    THIRD_SECTION_TEXT_ARTICLE = '//div[@class="article"]//div[@class="vue-article-body"][2]//p'
    FIRST_LINK_TEXT_ARTICLE = '//div[@class="article"]/div/div/div[1]//p[1]/a'
    ARTICLE_WITHOUT_TEXT = '//div[@id="noarticletext"]'
    FIRST_TABLE_ARTICLE = '//div[@class="article"]//table[@class="wikitable"][1]'
    FIRST_LIST_ARTICLE = '//div[@class="article"]//ul[1]'
    FIRST_TEMPLATE_ARTICLE = '''//div[@class="article"]//a[contains(@title, 'Шаблон'][1]'''
    FIRST_HEADER_2_ARTICLE = '//div[@class="article"]//h2[1]'
    FIRST_HEADER_3_ARTICLE = '//div[@class="article"]//h3[1]'  # Первый подзаголовок 1
    FIRST_HEADER_4_ARTICLE = '//div[@class="article"]//h4[1]'
    FIRST_HEADER_5_ARTICLE = '//div[@class="article"]//h5[1]'
    FIRST_QUOTES_ARTICLE = '//div[@class="article"]//div[@class="ts-Цитата"][1]'
    BOLD_TEXT = '''//b[text(='Полужирное начертание']'''  # Текст 'Полужирное начертание' полужирным шрифтом
    BOLD_TEXT_2 = '''//div[@class="article"]//b[contains(text(, 'Автомобиль']'''  # Текст 'Автомобиль ' полужирным шрифтом
    NOTE = '''//li[@id="cite_note-1"]/span[text(='_Автомобили 60-_х']'''  # Примечание '_Автомобили 60-_х'
    EXTERNAL_LINK = '//a[contains(@class, "external-link"]'  # Внешняя ссылка в тексте
    SPEC_SYMBOLS = '''//p[contains(text(, 'Ė⇒']'''  # Специальные символы 'Ė⇒'
    SMALL_TEXT = '''//small[text(='Мелкий текст']'''  # Текст 'Мелкий текст' мелким шрифтом
    AUTOTEST_CATEGORY = '''//div[@id="catlinks"]//a[text(='Autotest Category']'''
    REDIRECT = '''//div[@class="redirectMsg"]//a'''
    FIRST_BUTTON_CREATE_ARTICLE = '//input[@value="Создать новую статью"]'
    FIRST_LINK_PYTHON = '//a[@title="Python"]'
    FIRST_IMAGE_FROM_ARTICLE = '''//div[contains(@class, "vue-article-body"]//div[@class="thumb tright"]/div[@class="thumbinner"]/a[@class="image"]/img/parent::a[contains(@href, 'jpg']'''  # Первое изображение в статье за пределами инфобокса
    FIRST_IMAGE_ARTICLE = '//td[@class="infobox-image"]//a'  # Первое изображение в статье в инфобоксе
    MOVE_TO_DATA_ABOUT_IMAGE = '''//a[text(='Подробнее']'''  # Переход к описанию файла после нажатия на кнопку 'Подробнее'
    AUTOTEST_NOTES = '''//span[text(='notes']/parent::li'''  # Примечание 'notes' в статье
    EXAMPLE_IMAGE = '//img[@alt="Example.jpg"]'  # Изображение Example.jpg в статье
    AUTOTEST_UPLOADED_IMAGE = '//img[@alt="Ruwiki-image.jpg"]'  # Изображение Ruwiki.jpg в статье
    VISUAL_EDITOR_ARTICLE_TITLE = '''//h1/span[text(='Визуальный редактор']'''  # Название 'Визуальный редактор'
    WELCOME_ARTICLE_TITLE_AFTER_CREATE_ACCOUNT = '''//h1[contains(text(, 'Добро пожаловать']'''  # Название приветственной статьи после создания аккаунта
    ACCOUNT_NAME_ARTICLE_TITLE = '''//h1/span[text(='Участник']'''  # Название страницы при переходе на страницу участника
    NUMBERED_LIST_TEST1_AFTER_CREATE_PERSONAL_PAGE = '''//ul/li[text(='test1']'''
    NUMBERED_LIST_TEST2_AFTER_CREATE_PERSONAL_PAGE = '''//ul/li[text(='test2']'''
    NUMBERED_LIST_TEST3_AFTER_CREATE_PERSONAL_PAGE = '''//ul/li[text(='test3']'''
    NUMBERED_LIST_TEST4_AFTER_CREATE_PERSONAL_PAGE = '''//ul/li[text(='test4']'''
    FIRST_ELEMENT_OF_LIST = '//div[contains(@class, "vue-article-body"][1]/div[2]/ul/li[1]'  # Первый элемент списка в статье
    SECOND_ELEMENT_OF_LIST = '//div[contains(@class, "vue-article-body"][1]/div[2]/ul/li[2]'  # Второй элемент списка в статье
    UPLOADED_IMAGE = '//div[@class="article-wrapper"]//img'  # Изображение в статье в режиме чтения
    UPLOADED_PERSONAL_IMAGE = '''//div[@class="mw-parser-output"]//a[contains(@href, 'Красивая машина.jpg']/img'''  # Собственное загруженное изображение
    UPLOADED_IMAGE_EDIT_ARTICLE = '//div[4]/div/div[@contenteditable="true"][1]//figure//img'  # Изображение в статье в режиме 'Править'
    CONFIRM_VERSION_BUTTON = '//input[@value="Подтвердить версию"]'
    SELECT_FIRST_FILE_LINKED_ARTICLE = '//ul[@class="mw-imagepage-linkstoimage"]/li[1]/a'

    # TOP BAR ARTICLE LOCATORS
    MEMBER_BUTTON = '''//li[@id="ca-nstab-user"]/a'''  # Кнопка 'Участник'
    ARTICLE_BUTTON = '//li[@id="ca-nstab-main"]/a'  # Кнопка 'Статья'
    TALK_ARTICLE = '//li[@id="ca-talk"]/a'  # Кнопка 'Обсуждение' в статье
    VIEW_CODE = '//li[@id="ca-viewsource"]/a'  # Кнопка "Просмотр кода"
    TALK_TITLE_ARTICLE = '''//h1/span[text(='Обсуждение Рувики']'''
    READ_ARTICLE = '//div[1]/div[@class="ruwiki-tools-menu"]//li[@id="ca-more-view"]/a'  # Кнопка 'Читать'
    HISTORY_ARTICLE = '//div[1]/div[@class="ruwiki-tools-menu"]//li[@id="ca-more-history"]/a'  # Кнопка 'История'
    EDIT_ARTICLE = '''//a[text(='Править']'''
    CREATE_ARTICLE = '''//a[text(='Создать']'''
    EDIT_CODE_ARTICLE = '//div[1]/div[@class="ruwiki-tools-menu"]//li[@id="ca-more-edit"]/a'
    ADD_TO_WATCHLIST_BUTTON = '''//span[text(='Следить']/parent::div/parent::div'''  # Кнопка добавления статьи в список наблюдения
    TEXT_ABOUT_ADD_ARTICLE_TO_WATCHLIST = '''//div[@role="status"]//div[text(='Страница «' and '» и её обсуждение были добавлены в ваш ']'''  # Текст о добавлении статьи в список наблюдения
    VIEW_ON_COMMONS_BUTTON = '//li[@id="ca-view-foreign"]/a'
    CREATE_ARTICLE_BUTTON = '//a[@href="/wiki/Рувики:Авторам"]'
    EDIT_ARTICLE_LOADER = '//div[@class="ve-init-mw-progressBarWidget"]'
    USING_FILE = '''//a[text(='Использование файла']'''  # Кнопка 'Использование файла'
    OPEN_VOICING_WINDOW_ARTICLE = '//div[@title="Послушать статью"]'
    START_LISTENING_ARTICLE = '//button[@title="Воспроизвести"]'
    PAUSE_LISTENING_ARTICLE = '//button[@title="Пауза"]'
    SELECT_FULL_VERSION_LISTENING_ARTICLE = '//label[@title="Полная версия статьи"]'
    SELECT_SHORT_VERSION_LISTENING_ARTICLE = '//label[@title="Сокращенная версия статьи"]'
    SELECT_FIRST_VERSION_LISTENING = '//div[1]/label[@title="Голос диктора"]'
    SELECT_SECOND_VERSION_LISTENING = '//div[2]/label[@title="Голос диктора"]'

    # EDIT ARTICLE LOCATORS
    EDIT_ARTICLE_PAGE_TITLE = '''//h1[text(='Править страницу']'''  # Служебная страница 'Править страницу'
    # Необходимо перепроверить локаторы поп-апов
    CLOSE_POPUP_AFTER_MOVE_TO_EDIT_ARTICLE = '''//div[2]/div[@class="oo-ui-popupWidget-popup"][1]//a[@title="Закрыть"]'''
    HIDDEN_CLOSE_POPUP_AFTER_MOVE_TO_EDIT_ARTICLE = '''//div[contains(@class, 'oo-ui-element-hidden'][2]/div[@class="oo-ui-popupWidget-popup"][1]//a[@title="Закрыть"]'''
    HIDDEN_POPUP_FIRST_OPEN_LINK = '''//div[4]/div/span/div[1]/div[contains(@class, 'oo-ui-element-hidden']/div[@class="oo-ui-popupWidget-popup"]'''
    NOT_HIDDEN_POPUP_FIRST_OPEN_LINK = '''//div[4]/div/span/div[1]/div[2]/div[@class="oo-ui-popupWidget-popup"]'''
    CLOSE_POPUP_FIRST_OPEN_LINK = '//div[4]/div/span/div[1]/div[2]/div[@class="oo-ui-popupWidget-popup"]//a'
    NOT_HIDDEN_POPUP_ABOUT_EXIST_PERSON = '//div[2]/div[2]/div[@class="oo-ui-popupWidget-popup"]'  # Поп-ап о том, что редактирование статьи существующего человека
    HIDDEN_POPUP_ABOUT_EXIST_PERSON = '''//div[2]/div[contains(@class, 'oo-ui-element-hidden'][2]/div[@class="oo-ui-popupWidget-popup"]'''
    CLOSE_POPUP_ABOUT_EXIST_PERSON = '//div[2]/div[2]/div[@class="oo-ui-popupWidget-popup"]/div[1]//a'
    POPUP_AFTER_CREATING_ACCOUNT = '''//div[2]/div[2]/div[@class="oo-ui-popupWidget-popup"]'''  # Поп-ап после перехода к созданию страницы участника
    HIDDEN_POPUP_AFTER_CREATING_ACCOUNT = '''//div[2]/div[contains(@class, 'oo-ui-element-hidden'][2]/div[@class="oo-ui-popupWidget-popup"]'''
    CLOSE_POPUP_AFTER_CREATING_ACCOUNT = '''//div[2]/div[2]/div[@class="oo-ui-popupWidget-popup"]//a[@title="Закрыть"]'''
    # Проверенные локаторы поп-апов
    CLOSE_POPUP_LINK_ARTICLE = '''//label[text(='Ссылка']/parent::div/parent::div/div[2]/span/span[1]/a'''  # Закрыть всплывающее окно при наведении на ссылку в статье
    CLOSE_WELCOME_POPUP_BUTTON = '''//span[text(='Переключиться на визуальный редактор']/parent::a'''  # Кнопка 'Перейти в визуальный редактор'
    WELCOME_POPUP = '''//label[contains(text(, 'Добро пожаловать в']/ancestor::div[@class="oo-ui-window-frame"]'''  # Приветственный Поп-ап
    CLOSE_WELCOME_CREATE_POPUP_BUTTON = '''//span[text(='Начать редактирование']/parent::a'''  # Кнопка 'Начать редактирование'
    CLOSE_SEARCH_TEMPLATE_WINDOW = '//div[2]/div[@class="oo-ui-window-frame"]/div[2]/div[1]/div/div[3]//a'
    # Элементы статьи при редактировании
    REQUEST_UNBLOCK = '''//span[text(='Запросить разблокировку']/parent::a'''  # Кнопка 'Запросить разблокировку'
    ADD_TEXT_EDIT_CODE_CREATE_ARTICLE_TEXTAREA = '//textarea[@aria-label="Редактор исходного вики-текста"]'
    BOLD_FONT = '//a[@title="Полужирный"]'  # Кнопка 'Полужирный шрифт'
    CHANGE_EDITOR_BUTTON = '//div[@title="Переключить редактор"]'  # Переключение редактора кода
    SELECT_EDIT_CODE = '//a[@title="Редактирование кода"]'  # Выбрать 'Редактирование кода'
    SELECT_VISUAL_EDITOR = '//a[@title="Визуальное редактирование"]'  # Выбрать 'Визуальный редактор'
    SELECT_INSERT_IMAGE = '''//div[@id="bodyContent"]//span/a/span[text(='Изображения и медиафайлы']/parent::a'''  # Выбрать вставку изображения
    MOVE_TO_UPLOAD_FILE_IMAGE = '''//span[text(='Загрузить']'''  # Переход к вкладке 'Загрузить' в модальном окне загрузки изображения
    UPLOAD_IMAGE_INPUT = '//input[@type="file"]'  # Поле для загрузки файлов
    UPLOAD_IMAGES_CHECKBOX = '''//label[text(='Это моя собственная работа']/parent::span/parent::div/span[1]//input'''  # Чекбокс 'Это моя собственная работа'
    UPLOAD_IMAGES_CHECKBOX_2 = '//input[@id="ooui-38"]'
    UPLOAD_IMAGES_CONFIRM = '''//span[text(='Загрузить']/parent::a'''  # Кнопка 'Загрузить'
    UPLOAD_IMAGES_TITLE = '''//label[text(='Имя']/parent::span/parent::div//input'''
    UPLOAD_IMAGES_DESCRIPTION = '''//label[text(='Описание']/parent::span/parent::div//textarea[1]'''  # Поле 'Описание' в окне загрузки изображений
    UPLOAD_IMAGES_SAVE = '''//span[text(='Сохранить']/parent::a'''  # Кнопка 'Сохранить'
    UPLOAD_IMAGES_USE_IMAGE = '''//span[text(='Использовать это изображение']/parent::a'''  # Кнопка Использовать это изображение
    UPLOAD_IMAGES_INSERT = '''//span[text(='Вставить']/parent::a'''  # Кнопка 'Вставить'
    SELECT_MINOR_EDITS = '//div[@class="ve-ui-mwSaveDialog-checkboxes"]/div[1]/div'
    OPEN_CHANGES_MADE = '''//span[text(='Внесённые изменения']/parent::a'''
    DISPLAY_TEXT_EDITS_BEFORE_SAVE = '''//p//ins[contains(text(, 'autotest']'''
    DISPLAY_BOLD_TEXT_EDITS_BEFORE_SAVE = '''//div[2]/div/p//b[contains(text(, 'autotest']'''
    DISPLAY_LINK_EDITS_BEFORE_SAVE = '''//p//ins/a[contains(text(, 'autotest']'''
    DISPLAY_EN_TEXT_EDITS_BEFORE_SAVE = '''//p[contains(text(, 'autotest']'''  # Добавленный текст autotest в поле En в предпросмотре
    DISPLAY_TEXT_CREATED_ARTICLE_BEFORE_SAVE = '''//div[@class="oo-ui-window-body"]//p[contains(text(, 'autotest']'''
    BACK_TO_SAVE_EDIT_FORM = '//div[@class="oo-ui-processDialog-actions-safe"]//a'
    CONFIRM_EDITS = '''//span[text(='Записать изменения']/parent::a'''
    CANCEL_EDITS = '''//span[text(='Отменить']/parent::a'''
    CONFIRM_CREATING = '''//span[text(='Записать страницу']/parent::a'''
    FIRST_NOTE = '//div[@id="Примечания"]//ul/li[1]'
    SECOND_NOTE = '//div[@id="Примечания"]//ul/li[2]'
    EDITED_TEXT_ARTICLE_1 = '//div[@id="Начало"]/div[3]/p[1]'
    EDITED_TEXT_ARTICLE_2 = '//div[@id="Начало"]/div[3]/p[2]'
    EDITED_TEXT_ARTICLE_3 = '//div[@id="Начало"]/div[3]/p[3]'
    TEXT_ARTICLE = '//div[@class="vue-article-header"]/div/p'
    SELECT_FIRST_ARTICLE_FROM_LINK_LIST = '//div[@class="oo-ui-searchWidget-results"]/div/div[1]'  # Выбор первой статьи из списка существующих статей для создания ссылки
    ADD_LINK_INPUT = '//div[@id="wikieditor-toolbar-link-dialog"]/fieldset/div[1]//input'
    CONFIRM_ADDING_LINK = '//div[@class="ui-dialog-buttonset"]/button[1]'
    BLOCKED_USER_LINK = '''//b[text(='Причина ' and 'блокировки']/parent::p/a[2]'''
    UNDELETE_ARTICLE_LINK = '''//a[text(='просмотреть/восстановить']'''  # Ссылка на восстановление страницы в статье
    DELETED_ARTICLE_PAGE_TEXT = '''//p[text(='В РУВИКИ ' and ' с таким названием.']/b[text(='нет статьи']'''
    DELETED_ARTICLE_PAGE_TEXT_FROM_EDIT = '''//p[contains(text(, 'В РУВИКИ нет статьи с таким названием']'''
    DELETED_ARTICLE_PAGE_TEXT_2 = '''//p[text(='В РУВИКИ ' and ' с таким названием.']/b[text(='нет страницы']'''
    DELETED_ARTICLE_PAGE_TEXT_3 = '''//p/b[text(='Страница участника']'''
    DELETED_ARTICLE_PAGE_EDIT_CODE_TEXT = '''//p/b[text(='Страница участника']'''
    BLOCK_CORRECTION_ON_PAGE_HISTORY = '''//section[@id="pagehistory"]'''
    TEXT_ABOUT_AUTOREVIEW = '''//section[@id="pagehistory"]/ul[1]/li[1]/span[@class="fr-hist-basic-auto plainlinks"]/a[text(='отпатрулирована автоматически']'''
    TEXT_ABOUT_WAITING_PATROL = '''//section[@id="pagehistory"]/ul[1]/li[1]/span/b/a[text(='ожидает проверки']'''
    USER_IN_CORRECTION_AUTOREVIEW = '''//section[@id="pagehistory"]/ul[1]/li[1]/span[@class="flaggedrevs-color-1"]/span[@class="history-user"]/a[bdi='CoolAssist']'''  # Пользователь в правке
    USER_IN_CASCADE_PROTECTION = '''//section[@id="pagehistory"]/ul[1]/li[1]/span[@class="flaggedrevs-color-1"]/span[@class="history-user"]/a[bdi='MagicMan']'''  # Пользователь в правке
    TEXT_ABOUT_CASCADE_PROTECT = '''//ul[@class="mw-contributions-list"][1]//span[text(='» ([Редактирование=администраторы и инженеры] (бессрочно [Переименование=администраторы и инженеры] (бессрочно [каскадная]']'''  # Текст наличия каскадной защиты в истории статьи
    TEXT_FOR_WAIT_CHECKING = '''//section[@id="pagehistory"]/ul[1]/li[1]/span[2]/b/a[text(='ожидает проверки']'''  # Правка с текстом "ожидает проверки"
    USER_IN_CHANGE_FOR_FAST_ROLLBACK = '''//section[@id="pagehistory"]/ul[1]/li[1]/span[@class="flaggedrevs-pending"]/span[@class="history-user"]/a[bdi='Shadowman']'''  # Пользователь в правке для быстрого отката
    TEXT_FOR_FAST_ROLLBACK = '''//section[@id="pagehistory"]/ul[1]/li[1]/span[1]/span[@class="mw-changeslist-links mw-pager-tools"]/span/span[@class="mw-rollback-link"]/a'''  # Правка с текстом "откатить 1 правку"
    BLOCKED_USER_TEXT = '''//*[contains(text(, 'У вас нет прав на выполнение действия «редактирование этой страницы» по следующей причине']'''
    EDIT_TAGS_TITLE = '''//h1[text(='Редактировать теги']'''
    SELECTED_TAG = '//ul[@class="chosen-choices"]/li[@class="search-choice"]'
    REMOVE_SELECTED_TAG = '//ul[@class="chosen-choices"]/li[@class="search-choice"]/a'
    TAG_INPUT = '//ul[@class="chosen-choices"]/li/input'
    ADD_TAG_CAUSE_INPUT = '//input[@name="wpReason"]'
    CONFIRM_ADD_TAG_BUTTON = '//input[@name="wpSubmit"]'
    TEXT_ABOUT_ADDED_TAG = '''//div[text(='Изменения были применены.']'''


class VisualEditorArticleLocators:

    ADD_LINKS_VISUAL_EDITOR = '//a[@title="Ссылка Ctrl+K"]'  # Кнопка добавления ссылок в статью в режиме 'Править'
    CLOSE_WELCOME_POPUP_FROM_LINKS = '''//div[@class="oo-ui-widget oo-ui-widget-enabled oo-ui-floatableElement-floatable oo-ui-popupWidget-anchored oo-ui-popupWidget oo-ui-popupWidget-anchored-top"]//span[text(='Окей, понятно']/parent::a'''
    MOVE_TO_EXTERNAL_LINKS_BUTTON = '''//span[text(='Внешний вебсайт']/parent::div'''  # Кнопка Внешний вебсайт в поп-апе при добавлении ссылки
    ADD_EXTERNAL_LINKS_VISUAL_EDITOR_INPUT = '//input[@aria-label="Внешний вебсайт"]'  # Поле для ввода текста внешней ссылки
    ADD_PARAGRAPH_TO_ARTICLE = '//div[@class="ve-ce-branchNode-slug ve-ce-branchNode-blockSlug"][1]'  # Кнопка добавления параграфа в статью в режиме Править
    ADD_2_PARAGRAPH_TO_ARTICLE = '//div[@class="ve-ce-branchNode-slug ve-ce-branchNode-blockSlug"][2]'  # Кнопка добавления второго параграфа в статью в режиме Править
    SELECT_FIRST_IMAGE = '//figure[1]/a'  # Выбрать первое изображение
    MOVE_TO_FILE_PAGE_FROM_POPUP = '//div[@class="oo-ui-window-body"]/div/div[3]//div[1]/fieldset[1]//a'
    TEXT_INPUT_ALL_ARTICLE = '//div/div/div[@contenteditable="true"][1]'
    ADD_TEXT_INPUT_1_ARTICLE = '//div/div/div[@contenteditable="true"]/p[1]'  # Поле для ввода текста в статью в режиме 'Править'
    ADD_TEXT_INPUT_2_ARTICLE = '//div/div/div[@contenteditable="true"]/p[2]'  # Второе поле для ввода текста в статью в режиме 'Править'
    ADD_TEXT_INPUT_3_ARTICLE = '//div/div/div[@contenteditable="true"]/p[3]'  # Третье поле для ввода текста в статью в режиме 'Править'
    ADD_TEXT_INPUT_4_ARTICLE = '//div/div/div[@contenteditable="true"]/p[4]'  # Четвертое поле для ввода текста в статью в режиме 'Править'
    ADD_TEXT_INPUT_5_ARTICLE = '//div/div/div[@contenteditable="true"]/p[5]'  # Пятое поле для ввода текста в статью в режиме 'Править'
    ADD_TEXT_INPUT_6_ARTICLE = '//div/div/div[@contenteditable="true"]/p[6]'  # Шестое поле для ввода текста в статью в режиме 'Править'
    ADD_TEXT_INPUT_7_ARTICLE = '//div/div/div[@contenteditable="true"]/p[7]'  # Седьмое поле для ввода текста в статью в режиме 'Править'
    ADD_TEXT_MARKED_LIST_1_INPUT = '//ul[@class="ve-ce-branchNode"]/li/p'  # Поле для ввода текста в маркированный список в статье
    ADD_TEXT_MARKED_LIST_2_INPUT = '//ul[@class="ve-ce-branchNode"]/li[2]/p'  # Второе поле для ввода текста в маркированный список в статье
    OPEN_STRUCTURE = '//div[@title="Структура"]/span'  # Открыть список 'Структура' в режиме 'Править'
    SELECT_NUMBERED_LIST = '''//span[text(='Нумерованный список']/parent::a'''  # Выбрать 'Нумерованный список' из списка 'Структура'
    SELECT_MARKED_LIST = '''//span[text(='Маркированный список']/parent::a'''  # Выбрать 'Маркированный список'
    OPEN_TEXT_STYLE_BUTTON = '//div[@title="Стиль текста"]/span'  # Открыть список 'Стиль текста' в режиме 'Править'
    SELECT_BOLD_FONT = '''//span[text(='Полужирный']/parent::a'''  # Выбрать полужирный шрифт
    OPEN_PARAGRAPH = '''//span[text(='Абзац']/parent::span'''  # Открыть список 'Абзац'
    ADD_SUBTITLE_1 = '''//span[contains(@class, 'heading3']/a'''  # Добавить 'Подзаголовок 1'
    OPEN_SPEC_SYMBOL = '//a[@tabindex="0" and @title="Специальный символ"]'  # Открыть список 'Специальный символ'
    SPEC_SYMBOL_1 = '''//div[text(='Ė']'''  # Выбрать специальный символ 'Ė'
    SPEC_SYMBOL_2 = '''//div[text(='⇒']'''  # Выбрать специальный символ '⇒'
    OPEN_PAGE_PARAMETERS = '''//span[text(='Параметры страницы']/parent::span'''  # Открыть список 'Параметры страницы'
    SELECT_SEARCH_REPLACE_WORD = '''//span[text(='Найти и заменить']/parent::a'''  # Выбрать 'Найти и заменить'
    SEARCH_WORD_INPUT = '//input[@placeholder="Найти"]'  # Поле для ввода заменяемого слова
    REPLACE_WORD_INPUT = '//input[@placeholder="Заменить"]'  # Поле для ввода заменяющего слова
    CONFIRM_REPLACE_BUTTON = '''//span[text(='Заменить']/parent::a'''  # Кнопка 'Заменить'
    CONFIRM_ALL_REPLACE_BUTTON = '''//span[text(='Заменить всё']/parent::a'''  # Кнопка 'Заменить все'
    EXIT_FROM_REPLACE_WORDS = '''//span[text(='Готово']/parent::a'''  # Кнопка 'Готово'
    OPEN_INSERT = '''//span[text(='Вставить']/parent::span'''  # Открыть список 'Вставить'
    SELECT_IMAGES_AND_MEDIA = '''//span[text(='Изображения и медиафайлы']/parent::a'''  # Выбрать 'Изображения и медиафайлы'
    MOVE_TO_UPLOAD_PERSONAL_IMAGES_BUTTON = '''//span[text(='Загрузить']'''  # Кнопка 'Загрузить' для перехода к загрузке собственного изображения
    IMAGES_GROUP_NAME_INPUT = '//input[@aria-label="Поиск мультимедиа"]'  # Поле для ввода названия изображения
    SELECT_FIRST_IMAGES = '//div[@aria-label="Результаты поиска медиа-файлов"]/div[1]/div[1]'  # Выбрать первое изображение среди доступных
    USE_THIS_IMAGE_BUTTON = '''//span[text(='Использовать это изображение']/parent::a'''  # Кнопка 'Использовать это изображение'
    DESCRIPTION_FILE_INPUT = '//div[@class="oo-ui-fieldLayout-field"]//div[@role="textbox"]'  # Поле для ввода описания изображения
    CONFIRM_UPLOAD_FILE = '''//span[text(='Вставить']/parent::a'''  # Кнопка Вставить
    ADD_NOTE = '//a[contains(@title, "Ctrl+Shift+K"]'  # Добавить примечание/цитату
    WELCOME_POPUP_FROM_FIRST_MOVE_TO_NOTE = '''//h3[text(='Источники']/parent::div/span/a'''
    MANUAL_ADD_NOTE_BUTTON = '''//span[text(='Вручную']'''  # Кнопка 'Вручную' на поп-апе при добавлении примечания
    SIMPLE_NOTE_BUTTON = '''//span[text(='Простая']/parent::div'''  # Кнопка 'Простая' в поп-апе при добавлении выборе добавления примечания 'Вручную'
    ADD_NOTE_TEXT_ON_MANUAL_SIMPLE_NOTE_INPUT = '//div[@aria-label="Напишите или скопируйте вашу сноску сюда, или вставьте шаблон источника."]'  # Поле для ввода текста примечания
    CONFIRM_ADDING_ELEMENTS_BUTTON = '''//span[text(='Вставить']/parent::a'''  # Кнопка 'Вставить' для добавления примечания/ссылок в поп-апе
    BACK_CHANGES_EDIT_ARTICLE = '//a[@title="Отменить Ctrl+Z"]'  # Отменить изменения в режиме 'Править'
    RETURN_CHANGES_EDIT_ARTICLE = '//a[@title="Вернуть Ctrl+Shift+Z, Ctrl+Y"]'  # Вернуть изменения в режиме 'Править'
    SAVE_PAGE_ARTICLE = '''//span[text(='Сохранить страницу…']/parent::a'''  # Кнопка сохранения создания страницы в режиме визуального редактирования
    SAVE_EDIT_ARTICLE = '''//span[text(='Сохранить изменения…']/parent::a'''  # Кнопка сохранения редактирования страницы в режиме визуального редактирования
    INSERT_CONSTRUCTIONS = '//div[3]/div/div/div[@class="oo-ui-toolbar-bar"]//div[@title="Вставить"]/span'  # Кнопка 'Вставить' в режиме 'Править'
    ADD_TEXT_INPUT_1_MEMBER = '//ul[@class="ve-ce-branchNode"]/li/p'  # Первый абзац для добавления текста на страницу участника
    DESCRIPTION_EDIT_INPUT = '//div[@class="oo-ui-window-body"]//textarea'  # Поле для описания сохраняемых изменений при сохранении в режиме визуального редактирования
    # Непроверенные Поп-апы
    HIDDEN_WELCOME_POPUP_AFTER_MOVE_TO_EDIT_ARTICLE = '''//div[contains(@class, 'hidden']/div[@class="oo-ui-window-frame"]'''  # Приветственный поп-ап при переходе по кнопке "Править"
    WELCOME_POPUP_AFTER_MOVE_TO_EDIT_ARTICLE = '''//div[@class="oo-ui-window-frame"]'''
    CLOSE_WELCOME_POPUP_AFTER_MOVE_TO_EDIT_ARTICLE = '''//div[@class="oo-ui-window-frame"]/div[2]/div[3]/div/span[2]'''
    HIDDEN_POPUP_AFTER_EXIT_EDIT_ARTICLE = '''//div[contains(@class, 'hidden']/div[@class="oo-ui-window-frame"]'''  # Поп-ап при выходе из режиме визуального редактирования с несохраненными изменениями
    POPUP_AFTER_EXIT_EDIT_ARTICLE = '''//div[@class="oo-ui-window-frame"]'''
    CLOSE_POPUP_EXIT_EDIT_ARTICLE = '''//div[@class="oo-ui-window-frame"]/div[2]/div[3]/div/span[1]'''
    HIDDEN_POPUP_AFTER_MOVE_TO_EDIT_ARTICLE_FROM_EDIT_CODE = '''//div[3]/div[contains(@class, 'oo-ui-element-hidden'][1]/div[@class="oo-ui-popupWidget-popup"]'''  # Поп-ап с текстом о переключении с 'Править код' на 'Править'
    NOT_HIDDEN_POPUP_AFTER_MOVE_TO_EDIT_ARTICLE_FROM_EDIT_CODE = '''//div[3]/div[1]/div[@class="oo-ui-popupWidget-popup"]'''  # Поп-ап с текстом о переключении с 'Править код' на 'Править'
    CLOSE_POPUP_AFTER_MOVE_TO_EDIT_ARTICLE_FROM_EDIT_CODE = '//div[3]/div[1]/div[@class="oo-ui-popupWidget-popup"]//a'  # Закрытие поп-апа с текстом о переключении с 'Править код' на 'Править'
    # Проверенные поп-апы
    ADD_LINK_POPUP = '''//label[text(='Добавить ссылку']/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div'''  # Поп-ап при добавлении ссылки в статью
    HIDDEN_ADD_LINK_POPUP = '''//label[text(='Добавить ссылку']/parent::div/parent::div/parent::div/parent::div/parent::div/parent::div[contains(@class, 'oo-ui-element-hidden']'''
    ADD_LINK_BUTTON = '''//label[text(='Добавить ссылку']/parent::div/parent::div/div[1]//a'''  # Кнопка 'Готово', добавляющая ссылку в статью
    ADD_LINK_TEXT_INPUT = '''//label[text(='Добавить ссылку']/ancestor::div[@class="oo-ui-window-frame"]/div[2]/div[2]//input[@type="search"]'''  # Поле для ввода текста ссылки
    SELECT_FIRST_SEARCHED_PAGE = '''//label[text(='Добавить ссылку']/ancestor::div[@class="oo-ui-window-frame"]/div[2]/div[2]//div[@role="listbox"]/div[1]'''  # Выбрать первую страницу
    # Другие элементы на странице при визуальном редактировании
    ACTIVE_ALERT_AFTER_MOVE_TO_EDIT_ARTICLE = '''//span[contains(@class, 'active']/a[@title="Уведомления для редактирующих"]'''  # Увемление после перехода к 'Править' при редактировании статьи
    HIDDEN_WD_PREAMBULA_POPUP = '''//label[text(='Правка: ВД-преамбула']/ancestor::div[contains(@class, 'element-hidden']'''
    WD_PREAMBULA_POPUP = '''//label[text(='Правка: ВД-преамбула']'''
    CLOSE_WD_PREAMBULA_POPUP = '''//label[text(='Правка: ВД-преамбула']/parent::div/parent::div/div[3]//a'''
    SELECT_RU_TITLE_CHECKBOX = '''//div[@role="listbox"]//label[text(='Русское название']/parent::div//input'''
    SELECT_ORIGINAL_TITLE_CHECKBOX = '''//div[@role="listbox"]//label[text(='Оригинальное название']/parent::div//input'''
    IMAGE_INPUT = '//input[@id="ooui-300"]'
    SELECT_FIRST_LINE_FROM_TITLE_LIST = '//input[@id="ooui-300"]/parent::div/div'


class CodeEditorArticleLocators:

    SAVE_EDIT_CODE_ARTICLE_INPUT = '//input[@value="Записать изменения"]'  # Кнопка сохранения изменений в существующей статье
    SAVE_EDIT_CODE_CREATE_ARTICLE_INPUT = '//input[@value="Записать страницу"]'  # Кнопка сохранения изменений в новой статье
    OPEN_CHANGES_MADE_EDIT_CODE = '//input[@name="wpDiff"]'  # Кнопка 'Внесенные изменения' на странице 'Править код'
    PREVIEW_INPUT = '//input[@value="Предварительный просмотр"]'  # Кнопка 'Предварительный просмотр' на странице 'Править код'
    DESCRIPTION_EDIT_CODE_ARTICLE_INPUT = '//input[@name="wpSummary"]'  # Поле для ввода описания сделанных изменений в режиме 'Править код'
    ADD_LINKS_EDIT_CODE_ARTICLE = '//a[@title="Ссылка"]'  # Кнопка добавления ссылок в статью в режиме 'Править код'
    ADVANCED_EDITOR_ARTICLE = '//a[@aria-controls="wikiEditor-section-advanced"]'  # Кнопка дополнительного редактирования
    ADD_REDIRECT_BUTTON = '//a[@title="Перенаправление"]'
    ADD_HEADER_TO_TEXT = '//div[@class="group group-heading"]/div/a'
    ADD_SMALL_TEXT = '//a[@title="Мелкий"]'
    ADD_HEADER_2 = '//a[@rel="heading-2"]'
    ADD_HEADER_3 = '//a[@rel="heading-3"]'
    ADD_HEADER_4 = '//a[@rel="heading-4"]'
    ADD_HEADER_5 = '//a[@rel="heading-5"]'
    ADD_TEXT_INPUT_BOX_CODE = '//textarea[@name="wpTextbox1"]'  # Первый абзац для добавления текста в поле кода
    SPEC_SYMBOLS_ARTICLE = '//span[@class="tab tab-characters"]/a'
    FIRST_SPEC_SYMBOLS = '//div[@class="pages"]//div[@dir="ltr"]/span[1]'
    # Поп-апы
    WELCOME_POPUP_AFTER_MOVE_TO_EDIT_CODE_DISCUSS = '//div[10]/div[1]/div[@class="oo-ui-window-frame"]'  # Поп-ап после перехода в 'Править код' в обсуждении
    CLOSE_WELCOME_POPUP_AFTER_MOVE_TO_EDIT_CODE_DISCUSS = '''//span[text(='Начать редактирование']/parent::a'''
    POPUP_AFTER_MOVE_TO_EDIT_CODE_FROM_EDIT = '//div/div[@class="oo-ui-popupWidget-popup"]'  # Поп-ап о переключении с 'Править' на 'Править код' при редактировании статьи
    HIDDEN_POPUP_AFTER_MOVE_TO_EDIT_CODE_FROM_EDIT = '''//div[contains(@class, 'oo-ui-element-hidden']/div[@class="oo-ui-popupWidget-popup"]'''  # Скрытый Поп-ап о переключении с 'Править' на 'Править код' при редактировании статьи
    CLOSE_POPUP_AFTER_MOVE_TO_EDIT_CODE_FROM_EDIT = '//div/div[@class="oo-ui-popupWidget-popup"]//a'  # Закрыть Поп-ап о переключении с 'Править' на 'Править код' при редактировании статьи


class ProtectPageLocators:

    OPEN_LIST_ROLES_EDITING = '//div[@id="mwProtect-level-edit"]/div'
    SELECT_WITHOUT_PROTECT_EDITING = '''//div[1]/div[1]//span[text(='без защиты']/parent::div'''
    SELECT_ONLY_ADMIN_ROLE_EDITING = '''//div[1]//span[text(='администраторы и инженеры']/parent::div'''
    OPEN_LIST_DATE_EDITING = '//div[@id="mwProtectExpirySelection-edit"]/div'
    SELECT_1_HOUR_DATE_EDITING = '''//div[3]/div[2]/span[text(='1 час']/parent::div'''
    SELECT_1_HOUR_DATE_EDITING_EDIT_PROTECT = '''//div[3]/div[3]/span[text(='1 час']/parent::div'''
    OPEN_RENAMING_CHECKBOX = '//span[@id="mwProtectUnchained"]/span'
    OPEN_LIST_ROLES_RENAMING = '//div[@id="mwProtect-level-move"]/div'
    SELECT_WITHOUT_PROTECT_RENAMING = '''//div[2]/div[1]/span[text(='без защиты']/parent::div'''
    SELECT_ONLY_ADMIN_ROLE_RENAMING = '''//div[2]/div[3]/span[text(='администраторы и инженеры']/parent::div'''
    OPEN_LIST_DATE_RENAMING = '//div[@id="mwProtectExpirySelection-move"]/div'
    SELECT_1_HOUR_DATE_RENAMING = '''//div[4]/div[2]/span[text(='1 час']/parent::div'''
    SELECT_1_HOUR_DATE_RENAMING_EDIT_PROTECT = '''//div[4]/div[3]/span[text(='1 час']/parent::div'''
    CHECKBOX_CASCADE_PROTECT = '''//input[@name="mwProtect-cascade"]/parent::span'''
    SUBMIT_BUTTON = '//button[@type="submit"]'


class HistoryArticleLocators:

    HISTORY_ARTICLE_TITLE = '''//h1[text(='История страницы']'''
    PROTECTED_TEXT = '''//span[text(='Защитил страницу ']'''
    UNPROTECTED_TEXT = '''//span[text(='Cнял защиту с «']'''
    CANCEL_RENAMING = '''//span[text(='Admin переименовал страницу ']/parent::span//span[@class="mw-history-undo"]'''
    ABOUT_HISTORY_PAGE_LINK = '''//a[text(='О странице истории изменений']'''
    GRATITUDE_CHANGES_SIMPLE_USER = '''//span[text(='notifications-description']/ancestor::span//a[text(='поблагодарить']'''  # Кнопка 'поблагодарить'
    CONFIRM_PUBLIC_GRATITUDE = '''//span[text(='Поблагодарить публично?']/parent::span/a[1]'''  # Кнопка 'Поблагодарить', подтвердить публичную благодарность
    GRATEFUL_SIMPLE_USER = '''//span[text(='поблагодарён']/parent::span/parent::span/span[text(='notifications-description']'''
    CANCEL_FIRST_CHANGE = '''//section[@id="pagehistory"]/ul[1]/li/span[1]//a[text(='отменить']'''  # Отменить последнюю правку
    TEXT_ABOUT_CANCEL_FIRST_CHANGE = '''//section[@id="pagehistory"]/ul[1]/li/span[1]/span[contains(text(, ' правки']/a[text(='отмена']'''
    TEXT_ABOUT_RENAMING_ARTICLE = '''//span[contains(text(, 'переименовал страницу']'''
    MOVE_TO_OLD_TITLE_ARTICLE_LINK = '''//span[contains(text(, 'переименовал страницу']/a[1]'''  # Переход на старое название статьи в правке о переименовании
    MOVE_TO_NEW_TITLE_ARTICLE_LINK = '''//span[contains(text(, 'переименовал страницу']/a[2]'''
    TEXT_ABOUT_AUTO_PATROL_CHANGES = '''//section[@id="pagehistory"]/ul[1]/li[1]//a[text(='отпатрулирована автоматически']'''  # Надпись 'отпатрулирована автоматически' у последней правки
    TEXT_ABOUT_PATROL_CHANGES = '''//ul[1]//span[@class="fr-hist-basic-user plainlinks" and text(=' участником ']/a[text(='отпатрулирована']'''
    UNTESTED_VERSION_FIRST_RECORD_MARK = '''//ul/li[@class="mw-tag-visualeditor selected before"][1]/span[2]//a[text(='ожидает проверки']'''  # Отметка 'непроверенная версия' у первой записи
    WAITING_REVIEW_BUTTON = '''//ul[1]/li//a[text(='ожидает проверки']'''  # Кнопка 'ожидает проверки'
    HIDING_EDITS_BUTTON = '''//span[text(='Скрытие правок']/parent::a'''  # Кнопка 'Скрытие правок'
    SELECT_LAST_RECORD_VERSION_CHECKBOX = '//ul[@class="mw-contributions-list"][1]/li/span[1]/input[3]'  # Выбрать последнюю версию
    CHANGE_SELECTED_VERSIONS_TAGS_BUTTON = '//form[2]/div/div[1]/button[@value="editchangetags"]'  # Кнопка 'Изменить теги выбранных версий'
    TEXT_ABOUT_STABILIZED_PAGE = '''//ul[@class="mw-contributions-list"][1]/li//span[contains(text(, 'Установлены настройки стабильной версии']'''  # Текст об установке настроек стабильной версии


class RenamePageLocators:

    MOVE_TO_LINKS_LIST_PAGE = '//p/a[contains(@title, "Служебная:Ссылки сюда"]'  # Кнопка 'Ссылки сюда'
    OPEN_NEW_NAME_SECTION_LIST = '//div[@id="wpNewTitleNs"]//span[@role="combobox"]'  # Открыть выпадающий список с секциями для нового имени
    SELECT_MEMBER_FROM_SECTION_LIST = '''//span[text(='Участник']/parent::div'''  # Выбрать секцию 'Участник'
    NEW_NAME_INPUT = '//input[@name="wpNewTitleMain"]'  # Поле для ввода нового названия страницы
    OPEN_REASON_SECTION_LIST = '//select[@id="wpReasonList"]'
    SELECT_ERROR_REASON_FROM_REASON_LIST = '''//*[text(='название с ошибкой']'''
    CONFIRM_RENAME_BUTTON = '//button[@type="submit"]'
    ADD_TITLE_TO_WATCHLIST_CHECKBOX = '//input[@name="wpWatch"]/parent::span/span'  # Чекбокс 'Включить эту страницу и её старое название в список наблюдения'
    LEAVE_REDIRECT_CHECKBOX = '//input[@name="wpLeaveRedirect"]/parent::span/input'  # Чекбокс 'Оставить перенаправление'
    LEAVE_REDIRECT_CHECKED_CHECKBOX = '//input[@name="wpLeaveRedirect"]/parent::span/input[@checked="checked"]'  # Включенный чекбокс 'Оставить перенаправление'
    OTHER_REASON_INPUT = '//input[@name="wpReason"]'  # Поле для ввода причины
    RENAMED_TITLE_TEXT = '''//h1[text(='Страница переименована']'''
    NEW_LINK_ARTICLE = '//a[@id="movepage-newlink"]'
    OLD_LINK_ARTICLE = '//a[@id="movepage-oldlink"]'
    REDIRECT_LINK = '//ul[@class="redirectText"]//a'
    REQUEST_RENAME_TEXT_FROM_SECTION = '//div[@class="ts-editnotice-text"]'  # Блок с текстом о необходимости отправить запрос на переименование страницы участника
    NEW_NAME_FILE_INPUT = '''//label[text(='Каким должно быть новое имя файла?']/parent::div/div[1]/input'''  # Поле для ввода нового названия страницы
    RENAME_REASON_INPUT = '''//label[text(='Почему вы хотите переименовать этот файл?']/parent::div/div[2]/input'''  # Поле для ввода причины
    CONFIRM_RENAME_FILE_BUTTON = '//div[@class="ui-dialog-buttonset"]/button[1]'


class UploadFileArticleLocators:

    UPLOAD_FILE_INPUT = '//input[@id="wpUploadFile"]'


class ServicePagesLocators:
    # Блок 'Списки страниц'
    SERVICE_PAGES_PAGE_TITLE = '''//h1[text(='Служебные страницы']'''
    LIST_PAGES_TITLE = '''//h2[text(='Списки страниц']'''
    ALL_PAGES_LINK = '//a[@title="Служебная:Все страницы"]'
    SEARCH_LINK = '''//a[@title="Служебная:Поиск"]'''
    REDIRECT_LIST_LINK = '//a[@title="Служебная:Список перенаправлений"]'
    INDEX_TO_THE_START_ARTICLE_LINK = '//a[@title="Служебная:Указатель по началу названия"]'
    # Блок 'Представиться / Зарегистрироваться'
    SIGN_UP_TITLE = '''//h2[text(='Представиться / Зарегистрироваться']'''
    SIGN_IN_LINK = '''//a[text(='Вход']'''
    CREATE_ACCOUNT_LINK = '''//a[text(='Создать учётную запись']'''
    ACCOUNT_MERGE_STATUS_LINK = '''//a[text(='Состояние объединения учётных записей']'''
    # Блок 'Участники и права'
    MEMBERS_AND_RIGHTS_TITLE = '''//h2[text(='Участники и права']'''
    AUTOBLOCKS_LINK = '//a[@title="Служебная:AutoblockList"]'
    BLOCK_USER_LINK = '''//a[text(='Блокировка участника']'''
    BLOCKED_USERS_LINK = '''//a[text(='Заблокированные участники']'''
    GROUPS_RIGHTS_LINK = '''//a[text(='Права групп участников']'''
    SETTINGS_LINK = '''//a[text(='Настройки']'''
    MEMBERS_RIGHTS_LINK = '''//a[text(='Права участника']'''
    MEMBERS_VERIFICATION_LINK = '''//a[text(='Проверить участника']'''
    UNBLOCK_USER_LINK = '''//a[text(='Разблокировка участника']'''
    LIST_MEMBERS_LINK = '''//a[text(='Список участников']'''
    # Блок 'Свежие правки и журналы'
    FRESH_EDITS_AND_LOGS_TITLE = '''//h2[text(='Свежие правки и журналы']'''
    NEW_FILES_GALLERY_LINK = '//a[@title="Служебная:Новые файлы"]'
    CHANGES_MARKS_LINK = '//a[@title="Служебная:Метки"]'
    CHECKS_LOG_LINK = '//a[@title="Служебная:Журнал проверок участников"]'
    LOG_EDIT_FILTERS_LINK = '//a[@title="Служебная:Журнал злоупотреблений"]'
    LOGS_LINK = '//a[@title="Служебная:Журналы"]'
    NEW_PAGES_LINK = '//a[@title="Служебная:Новые страницы"]'
    FRESH_EDITS_LINK = '//a[@title="Служебная:Свежие правки"]'
    WATCHLIST_LINK = '//a[@title="Служебная:Список наблюдения"]'
    # Блок 'Отчёты о медиаматериалах и загрузка'
    MEDIA_REPORTS_AND_DOWNLOADS_TITLE = '''//h2[text(='Отчёты о медиаматериалах и загрузка']'''
    UPLOAD_FILE_LINK = '''//a[text(='Загрузить файл']'''
    FILES_LIST_LINK = '//a[@title="Служебная:Список файлов"]'
    MEDIA_STATISTIC_LINK = '//a[@title="Служебная:MediaStatistics"]'
    # Блок 'Данные и инструменты'
    DATA_AND_INSTRUMENTS_TITLE = '''//h2[text(='Данные и инструменты']'''
    VERSION_LINK = '//a[@title="Служебная:Версия"]'
    VERSION_TITLE = '''//h1[text(='Версия']'''
    STATISTIC_LINK = '//a[@title="Служебная:Статистика"]'
    STATISTIC_TITLE = '''//h1[text(='Статистика']'''
    EDIT_FILTER = '''//a[@title="Служебная:Фильтр злоупотреблений"]'''  # Ссылка 'Фильтр правок'
    EDIT_SETTINGS_INTERWIKI = '//a[@title="Служебная:Интервики"]'  # Ссылка 'Просмотреть данные об интервики'
    # Блок 'Перенаправляющие служебные страницы'
    REDIRECTED_SERVICE_PAGES_TITLE = '''//h2[text(='Перенаправляющие служебные страницы']'''
    HISTORY_PAGE_LINK = '''//a[text(='История страницы']'''
    EDIT_PAGE_LINK = '''//a[text(='Править страницу']'''
    # Блок 'Интенсивно используемые страницы'
    FREQUENTLY_USED_PAGES_TITLE = '''//h2[text(='Интенсивно используемые страницы']'''
    MOST_USED_FILES_LINK = '//a[@title="Служебная:Самые используемые файлы"]'
    MOST_USED_FILES_TITLE = '''//h1[text(='Самые используемые файлы']'''  # Заголовок страницы Самые используемые файлы
    # Блок 'Инструменты для страниц'
    TOOLS_FOR_PAGES_TITLE = '''//h2[text(='Инструменты для страниц']'''
    PAGES_IMPORT_LINK = '//a[@title="Служебная:Импорт"]'
    MASS_DELETED_PAGES = '//a[@title="Служебная:Множественное удаление"]'
    # Блок 'Патрулирование'
    PATROL_TITLE = '''//h2[text(='Патрулирование']'''
    UNVERIFIED_PAGES_LINK = '//a[@title="Служебная:Непроверенные страницы"]'
    # Блок 'Другие служебные страницы'
    OTHER_SERVICE_PAGES_TITLE = '''//h2[text(='Другие служебные страницы']'''
    CONTRIBUTE_LINK = '//a[@title="Служебная:Contribute"]'
    CONTRIBUTE_TITLE = '''//h1[text(='Вклад участника']'''
    SEND_MASS_MESSAGE_LINK = '//a[@title="Служебная:MassMessage"]'
    # Служебная страница 'Сбросить токены'
    RESET_TOKENS_PAGE_TITLE = '''//h1[text(='Сбросить токены']'''


class SettingsLocators:

    SETTINGS_TITLE = '''//h1[text(='Настройки']'''
    MOVE_TO_PERSONAL_DATA = '''//div/span[text(='Личные данные']/parent::div'''
    MOVE_TO_APPEARANCE = '''//div/span[text(='Внешний вид']/parent::div'''  # Кнопка 'Внешний вид'
    MOVE_TO_NOTIFICATIONS = '''//div/span[text(='Уведомления']/parent::div'''  # Кнопка 'Уведомления'
    MOVE_TO_RECENT_CHANGES = '''//div/span[text(='Свежие правки']/parent::div'''  # Кнопка 'Свежие правки'
    MOVE_TO_EDITING_PAGE = '''//span[text(='Редактирование']/parent::div'''  # Кнопка 'Редактирование'
    MOVE_TO_WATCHLIST = '''//span[text(='Список наблюдения']/parent::div'''
    # Секция Личные данные
    MEMBER_NAME_FIELD = '//label[@id="mw-input-wpusername"]'  # Имя пользователя
    GROUP_STEWARD_MEMBERSHIP_LINK = '''//label/a[text(='Стюарды']'''  # Ссылка на группу 'Стюарды'
    GROUP_MEMBER_MEMBERSHIP_LINK = '''//label/a[text(='Участники']'''  # Ссылка на группу 'Участники'
    GROUP_ADMIN_MEMBERSHIP_LINK = '''//label/a[text(='Администраторы']'''  # Ссылка на группу 'Администраторы'
    GROUP_AUTO_PATROL_MEMBERSHIP_LINK = '''//label/a[text(='Автопатрулируемые']'''
    GROUP_ROLLBACK_MEMBERSHIP_LINK = '''//label/a[text(='Откатывающие']'''
    GROUP_SUMMARIZING_MEMBERSHIP_LINK = '''//label/a[text(='Подводящие итоги']'''
    GROUP_PATROL_MEMBERSHIP_LINK = '''//label/a[text(='Патрулирующие']'''
    GROUP_EXCLUSION_IP_BLOCK_MEMBERSHIP_LINK = '''//label/a[text(='Исключения из IP-блокировок']'''
    EDIT_PASSWORD_BUTTON = '''//span[text(='Изменить пароль']/parent::a'''  # Кнопка 'Изменить пароль'
    ACCOUNT_DATA_FROM_PROJECT = '//label[@id="mw-input-wpdownloaduserdata"]/a'  # Кнопка 'Данные моей учётной записи из этого проекта'
    MEDIA_WIKI_API_TITLE = '''//h1[text(='Результат MediaWiki API']'''  # Страница 'Результат MediaWiki API'
    RESET_SETTINGS_BUTTON = '//label[@id="mw-input-wprestoreprefs"]/a'  # Кнопка 'Сбросить настройки'
    CONFIRM_RESET_SETTINGS_CHECKBOX = '//input[@name="wpconfirm"]'  # Чекбокс подтверждения сброса настроек
    DISPLAY_INFORMATION_ABOUT_GLOBAL_ACCOUNT_BUTTON = '''//span[text(='Просмотр информации о глобальной учётной записи']/parent::a'''  # Кнопка 'Просмотр информации о глобальной учётной записи'
    DISPLAY_DATA_ABOUT_PERSON_FROM_GLOBAL_ACCOUNT_FIELD = '//input[@name="target"]'  # Поле для ввода логина пользователя для просмотра персональных данных
    GLOBAL_ACCOUNT_MANAGER_PAGE_TITLE = '''//h1[text(='Менеджер глобальной учётной записи']'''  # Название страницы 'Менеджер глобальной учётной записи'
    INFORMATION_ABOUT_GLOBAL_ACCOUNT = '''//legend[text(='Информация о глобальной учётной записи']'''  # Секция с информацией о глобальной учётной записи
    ACCOUNT_NAME_FROM_INFORMATION_ABOUT_GLOBAL_ACCOUNT = '''//strong[text(='Имя учётной записи:']/parent::li'''  # Поле 'Имя учетной записи' в секции с информацией о глобальной учётной записи
    DISABLED_TWO_FACTOR_AUTHENTICATION = '''//label[@id="mw-input-wpoathauth-module"]/div/label[text(='Не включена']'''  # Выключенная двухфакторная аутентификация
    INTERNATIONALIZATION_SECTION = '''//span[text(='Интернационализация']/parent::legend'''  # Секция 'Интернационализация'
    SIGNATURE_SECTION = '''//span[text(='Подпись']'''
    CURRENT_USERNAME_SIGNATURE = '//div[@id="mw-htmlform-signature"]/div[1]//div/label/a[1]'
    MOVE_TO_DISCUSS_PERSON = '//div[@id="mw-htmlform-signature"]/div[1]//div/label/a[2]'
    PERSONAL_WIKI_MARKUP_CHECKBOX = '//input[@name="wpfancysig"]'  # Чекбокс для включения собственной вики-разметки
    # Секция Внешний вид
    SELECT_MONOBOOK_THEME = '//input[@value="monobook"]/parent::span/span'  # Выбор темы Монобук
    SELECT_YENISEY_THEME = '//span/input[@value="yenisey"]/parent::span/span'  # Выбор темы Енисей
    DISPLAY_FORMULAS = '''//span[text(='Отображение формул']/parent::legend'''  # Раздел Отображение формул
    SAVE_SETTINGS = '//button[@type="submit"]'  # Кнопка 'Сохранить'
    SAVED_SETTINGS_NOTIFICATION = '''//div[@role="status"]/div[text(='Настройки сохранены.']'''  # Уведомление о сохранении настроек
    # Секция Уведомления
    OPEN_EMAIL_SETTINGS_LIST = '//select[@name="wpecho-email-frequency"]/parent::div/div'
    EMAIL_ADDRESS = '//label[@id="mw-input-wpecho-emailaddress"]'
    SELECT_WITHOUT_EMAIL_NOTIFICATIONS = '''//span[text(='Не присылать мне уведомления по эл. почте']/parent::div'''  # Выбрать 'не присылать мне уведомления по эл.почте'
    SELECTED_WITHOUT_EMAIL_NOTIFICATIONS = '''//span[text(='Не присылать мне уведомления по эл. почте']/parent::div[contains(@class, 'selected']'''  # Выбрано 'не присылать мне уведомления по эл.почте'
    SELECT_SEPARATE_EMAIL_NOTIFICATIONS = '''//span[text(='Отдельные уведомления по мере их поступления']/parent::div'''  # Выбрать 'отдельные уведомления по мере их поступления'
    SELECTED_SEPARATE_EMAIL_NOTIFICATIONS = '''//span[text(='Отдельные уведомления по мере их поступления']/parent::div[contains(@class, 'selected']'''  # Выбрано 'отдельные уведомления по мере их поступления'
    SELECT_EVERYDAY_EMAIL_NOTIFICATIONS = '''//span[text(='Ежедневная сводка уведомлений']/parent::div'''  # Выбрать 'ежедневная сводка уведомлений'
    SELECTED_EVERYDAY_EMAIL_NOTIFICATIONS = '''//span[text(='Ежедневная сводка уведомлений']/parent::div[contains(@class, 'selected']'''  # Выбрано 'ежедневная сводка уведомлений'
    SELECT_WEEKLY_EMAIL_NOTIFICATIONS = '''//span[text(='Еженедельная сводка уведомлений']/parent::div'''  # Выбрать 'еженедельная сводка уведомлений'
    SELECTED_WEEKLY_EMAIL_NOTIFICATIONS = '''//span[text(='Еженедельная сводка уведомлений']/parent::div[contains(@class, 'selected']'''  # Выбрано 'еженедельная сводка уведомлений'
    NOTIFY_ABOUT_EVENTS = '''//span[text(='Сообщать мне об этих событиях']/parent::legend'''  # Блок 'Сообщать мне об этих событиях'
    WEB_CANCEL_CHANGES_CHECKBOX = '//span[@id="mw-input-wpecho-subscriptions-web-reverted"]'  # Чекбокс 'Веб Отмена правок'
    EMAIL_CANCEL_CHANGES_CHECKBOX = '//span[@id="mw-input-wpecho-subscriptions-email-reverted"]'  # Чекбокс 'Эл. почта Отмена правок'
    APP_CANCEL_CHANGES_CHECKBOX = '//span[@id="mw-input-wpecho-subscriptions-push-reverted"]'  # Чекбокс 'Приложения Отмена правок'
    # Секция 'Свежие правки'
    DISPLAY_SETTINGS = '''//fieldset[@id="mw-prefsection-rc-displayrc"]//span[text(='Настройки отображения']/parent::legend'''  # Блок 'Настройки отображения'
    ADVANCED_SETTINGS = '''//fieldset[@id="mw-prefsection-rc-advancedrc"]//span[text(='Расширенные настройки']/parent::legend'''  # Блок 'Расширенные настройки'
    GROUP_CHANGES_CHECKBOX = '//input[@name="wpusenewrc"]/parent::span/span'  # Чекбокс 'Группировать изменения в свежих правках и списке наблюдения'
    SELECTED_GROUP_CHANGES_CHECKBOX = '//input[@name="wpusenewrc"]/parent::span/input[@checked="checked"]'
    DISPLAYED_CHANGES = '''//fieldset[@id="mw-prefsection-rc-changesrc"]//span[text(='Показанные изменения']/parent::legend'''  # Блок 'Показанные изменения'
    HIDE_SMALL_CHANGES_CHECKBOX = '//input[@name="wphideminor"]/parent::span/span'  # Чекбокс 'Скрывать малые изменения из списка свежих правок'
    SELECTED_HIDE_SMALL_CHANGES_CHECKBOX = '//input[@name="wphideminor"]/parent::span/input[@checked="checked"]'
    PATROL_SETTINGS = '''//fieldset[@id="mw-prefsection-rc-flaggedrevs-ui"]//span[text(='Патрулирование']/parent::legend'''  # Блок 'Патрулирование'
    DETAIL_STATUS_OF_PAGE_VALIDATION_RADIOBUTTON = '//div[1]/div/span//input[@name="wpflaggedrevssimpleui"]/parent::span/span'  # Радиобаттон 'Подробная информация о состоянии проверки страницы'
    SELECTED_STATUS_OF_PAGE_VALIDATION_RADIOBUTTON = '//div[1]/div/span//input[@name="wpflaggedrevssimpleui"]/parent::span/input[@checked="checked"]'
    ASSESSMENT_EDIT_RECENT_CHANGES = '''//fieldset[@id="mw-prefsection-rc-ores-rc"]//span[text(='Оценка изменений в Свежих правках и Связанных правках']/parent::legend'''  # Блок 'Оценка изменений в Свежих правках и Связанных правках'
    HIGHLIGHT_PROBABLY_PROBLEMATIC_EDITS_WITH_COLOR = '//div[1]/span/span//input[@name="wpores-damaging-flag-rc"]/parent::span/span'  # Чекбокс 'Выделять вероятно проблемные правки цветом и символом «?», означающим «требуется проверка»'
    SELECTED_HIGHLIGHT_PROBABLY_PROBLEMATIC_EDITS_WITH_COLOR = '//div[1]/span/span//input[@name="wpores-damaging-flag-rc"]/parent::span/input[@checked="checked"]'
    # Секция 'Редактирование'
    COMMON_PARAMETERS = '''//span[text(='Общие параметры']/parent::legend'''  # Блок Общие параметры
    EDIT_SECTIONS_CHECKBOX = '//input[@name="wpeditsectiononrightclick"]/parent::span/span'  # Чекбокс 'Править секции при правом щелчке мышью на заголовке'
    SELECTED_EDIT_SECTIONS_CHECKBOX = '//input[@name="wpeditsectiononrightclick"]/parent::span/input[@checked="checked"]'
    EDITOR = '''//span[text(='Редактор']/parent::legend'''  # Блок 'Редактор'
    EDIT_PANEL_CHECKBOX = '//input[@name="wpusebetatoolbar"]/parent::span/span'  # Чекбокс 'Включить панель редактирования'
    SELECTED_EDIT_PANEL_CHECKBOX = '//input[@name="wpusebetatoolbar"]/parent::span/input[@checked="checked"]'
    PREVIEW = '''//span[text(='Предпросмотр']/parent::legend'''  # Блок 'Предпросмотр'
    PLACE_PREVIEW_ABOVE_EDIT_WINDOW_CHECKBOX = '//input[@name="wppreviewontop"]/parent::span/span'  # Чекбокс 'Помещать предпросмотр над окном редактирования'
    SELECTED_PLACE_PREVIEW_ABOVE_EDIT_WINDOW_CHECKBOX = '//input[@name="wppreviewontop"]/parent::span/input[@checked="checked"]'
    AVAILABILITY = '''//span[text(='Доступность']/parent::legend'''  # Блок 'Доступность'
    SYNTAX_HIGHLIGHTING_CHECKBOX = '//input[@name="wpusecodemirror-colorblind"]/parent::span/span'  # Чекбокс 'Включить подсветку синтаксиса для дальтоников при редактировании вики-текста'
    SELECTED_SYNTAX_HIGHLIGHTING_CHECKBOX = '//input[@name="wpusecodemirror-colorblind"]/parent::span/input[@checked="checked"]'
    DISCUSS_PAGES = '''//span[text(='Страницы обсуждения']/parent::legend'''  # Блок 'Страницы обсуждения'
    SHOW_DISCUSS_ACTIVITY_CHECKBOX = '//input[@name="wpdiscussiontools-visualenhancements"]/parent::span/span'  # Чекбокс эВключить быстрый ответэ
    SELECTED_SHOW_DISCUSS_ACTIVITY_CHECKBOX = '//input[@name="wpdiscussiontools-visualenhancements"]/parent::span/input[@checked="checked"]'
    # Секция 'Список наблюдения'
    EDIT_WATCHLIST = '''//span[text(='Изменение списка наблюдения']/parent::legend'''  # Блок 'Изменение списка наблюдения'
    VIEW_AND_DELETE_TITLES_FROM_WATCHLIST_BUTTON = '''//span[text(='Просмотр и удаление названий из вашего списка наблюдения']/parent::a'''  # Кнопка 'Просмотр и удаление названий из вашего списка наблюдения'
    EDIT_WATCHLIST_AS_TEXT_BUTTON = '''//span[text(='Редактирование списка наблюдения как текста']/parent::a'''  # Кнопка 'Редактирование списка наблюдения как текста'
    CLEANING_WATCHLIST_BUTTON = '''//span[text(='Очистить список наблюдения']/parent::a'''  # Кнопка 'Очистить список наблюдения'
    WATCHLIST_DISPLAY_SETTINGS_SECTION = '''//fieldset[@id="mw-prefsection-watchlist-displaywatchlist"]//span[text(='Настройки отображения']/parent::legend'''  # Блок 'Настройки отображения'
    NUMBERS_OF_DAYS_TO_SHOW_CHANGES_INPUT = '//input[@name="wpwatchlistdays"]'  # Поле для ввода значения 'Количество дней, за которые показывать изменения'
    MAX_COUNT_CHANGES_FOR_DISPLAY_WATCHLIST_INPUT = '//input[@name="wpwllimit"]'  # Поле для ввода значения 'Максимальное количество правок для показа в списке наблюдения'
    WATCHLIST_ADVANCED_SETTINGS_SECTION = '''//fieldset[@id='mw-prefsection-watchlist-advancedwatchlist']//span[text(='Расширенные настройки']/parent::legend'''  # Блок 'Расширенные настройки'
    EXPAND_WATCHLIST_CHECKBOX = '//input[@name="wpextendwatchlist"]/parent::span/span'  # Чекбокс 'Расширить список наблюдения, включая все изменения, а не только последние'
    SELECTED_EXPAND_WATCHLIST_CHECKBOX = '//input[@name="wpextendwatchlist"]/parent::span/input[@checked="checked"]'
    WATCHLIST_DISPLAYED_CHANGES_SECTION = '''//fieldset[@id="mw-prefsection-watchlist-changeswatchlist"]//span[text(='Показанные изменения']/parent::legend'''  # Блок 'Показанные изменения'
    HIDE_SMALL_CHANGES_FROM_WATCHLIST_CHECKBOX = '//input[@name="wpwatchlisthideminor"]/parent::span/span'  # Чекбокс 'Скрывать малые правки из списка наблюдения'
    SELECTED_HIDE_SMALL_CHANGES_FROM_WATCHLIST_CHECKBOX = '//input[@name="wpwatchlisthideminor"]/parent::span/input[@checked="checked"]'
    MONITORED_PAGES_SECTION = '''//span[text(='Наблюдаемые страницы']/parent::legend'''  # Блок 'Наблюдаемые страницы'
    ADD_RENAMED_FILES_PAGES_TO_WATCHLIST_CHECKBOX = '//input[@name="wpwatchmoves"]/parent::span/span'  # Чекбокс 'Добавлять в список наблюдения переименованные мной страницы и файлы'
    SELECTED_ADD_RENAMED_FILES_PAGES_TO_WATCHLIST_CHECKBOX = '//input[@name="wpwatchmoves"]/parent::span/input[@checked="checked"]'
    TOKEN_SECTION = '''//span[text(='Токен']/parent::legend'''  # Блок 'Токен'
    TOKEN_MANAGEMENT = '''//span[text(='Управление токенами']/parent::a'''  # Кнопка 'Управление токенами'
    VERSION_SCORING_SECTION = '''//span[text(='Оценка версий в списке наблюдения и вкладах']/parent::legend'''  # Блок 'Оценка версий в списке наблюдения и вкладах'
    SHOW_ONLY_PROBABLY_PROBLEMATIC_PROBABLE_EDITS_CHECKBOX = '//input[@name="wporesHighlight"]/parent::span/span'  # Чекбокс 'Выделять вероятно проблемные правки цветом и символом «?», означающим «требуется проверка»'
    SELECTED_SHOW_ONLY_PROBABLY_PROBLEMATIC_PROBABLE_EDITS_CHECKBOX = '//input[@name="wporesHighlight"]/parent::span/input[@checked="checked"]'


class RecentChangesLocators:

    RECENT_CHANGES_TITLE = '''//h1[text(='Свежие правки']'''
    FILTER_NAMESPACE_BUTTON = '//a[@title="Результаты фильтра по пространствам имён"]'
    DISPLAY_SETTINGS = '//div[@class="mw-rcfilters-ui-filterWrapperWidget-bottom"]/div//a'
    DISPLAY_RESULTS_FILTER = '''//span[text(='Результаты для показа']'''
    ABBREVIATIONS_LIST = '//div[@class="mw-collapsible-content"]'
    WELCOME_POPUP_AFTER_MOVE_TO_RECENT_CHANGES = '//div[@id="gt-RcFiltersIntro-Welcome"]'  # Поп-ап после перехода на страницу 'Свежие правки' под пользователем
    CLOSE_POPUP_AFTER_MOVE_TO_RECENT_CHANGES = '//div[@id="gt-RcFiltersIntro-Welcome"]/div/div[2]/a'
    REMOVE_CREATE_ARTICLES_FILTERS = '''//a[@title="Удалить 'Создания страниц'"]'''
    REMOVE_PROTOCOL_ACTIONS_FILTERS = '''//a[@title="Удалить 'Протоколируемые действия'"]'''
    FIRST_LINE_WITH_CHANGES = '''//table[contains(@class, 'mw-tag'][1]'''
    CHANGELIST_DIFF_LINKS_1_LINE = '''//table[contains(@class, 'mw-tag'][1]//td[5]/span[2]//a[@class="mw-changeslist-diff"]'''
    CHANGELIST_HISTORY_LINKS_1_LINE = '''//table[contains(@class, 'mw-tag'][1]//td[5]/span[2]//a[text(='история']'''
    TITLE_ARTICLE_FROM_CHANGES_1_LINE = '''//table[contains(@class, 'mw-tag'][1]//td[5]//span[@class="mw-title"]//a'''
    SECOND_LINE_WITH_CHANGES = '''//table[contains(@class, 'mw-tag'][2]'''
    CHANGELIST_DIFF_LINKS_2_LINE = '''//table[contains(@class, 'mw-tag'][2]//td[5]/span[2]//a[@class="mw-changeslist-diff"]'''
    CHANGELIST_HISTORY_LINKS_2_LINE = '''//table[contains(@class, 'mw-tag'][2]//td[5]/span[2]//a[text(='история']'''
    TITLE_ARTICLE_FROM_CHANGES_2_LINE = '''//table[contains(@class, 'mw-tag'][2]//td[5]//span[@class="mw-title"]//a'''
    THIRD_LINE_WITH_CHANGES = '''//table[contains(@class, 'mw-tag'][3]'''
    CHANGELIST_DIFF_LINKS_3_LINE = '''//table[contains(@class, 'mw-tag'][3]//td[5]/span[2]//a[@class="mw-changeslist-diff"]'''
    CHANGELIST_HISTORY_LINKS_3_LINE = '''//table[contains(@class, 'mw-tag'][3]//td[5]/span[2]//a[text(='история']'''
    TITLE_ARTICLE_FROM_CHANGES_3_LINE = '''//table[contains(@class, 'mw-tag'][3]//td[5]//span[@class="mw-title"]//a'''


class SearchResultsLocators:

    SEARCH_RESULTS_PAGE_TITLE = '''//h1[text(='Результаты поиска']'''
    FIRST_LINK_AFTER_DISPLAY_RESULTS = '//ul[@class="mw-search-results"]/li[1]//div/a'
    KEYWORD_LINK = '//div[@id="keywords-popup-pseudolink-wrapper"]/a'
    KEYWORD_LIST = '//table[@id="keywords-table"]'
    HELP_LINK = '//div[@id="mw-indicator-mw-helplink"]/a'
    SEARCH_INPUT = '//input[@title="Искать статью"]'
    SEARCH_BUTTON = '//button[@type="submit"]'
    CREATE_ARTICLE = '//p[@class="mw-search-createlink"]/b/a'


class AdvancedSearchLocators:

    ADVANCED_SEARCH_TITLE = '''//h1[text(='Поиск']'''
    ADVANCED_SEARCH_INPUT = '//input[@title="Искать статью"]'
    ADVANCED_SEARCH_BUTTON = '//button[@type="submit"]'
    OPEN_ADVANCED_SEARCH = '//div[@class="mw-advancedSearch-container"]/div[1]/span/a'
    OPEN_SEARCH_WITH_NAMESPACE = '//div[@class="mw-advancedSearch-container"]/div[2]/span/a'
    ADD_NAMESPACE_INPUT_LIST = '//input[@placeholder="Добавить пространства имён…"]'
    MAIN_NAMESPACE_FROM_NAMESPACE_LIST = '//div[@class="oo-ui-defaultOverlay"]/div[1]/div[1]//div[@class="oo-ui-fieldLayout-body"]'


class WatchListLocators:

    WATCHLIST_TITLE = '''//h1[text(='Список наблюдения']'''
    DISPLAY_AND_EDIT_WATCHLIST_SECTION = '''//span[text(='Смотреть и редактировать список']/parent::a'''  # Вкладка 'Смотреть и редактировать список'
    FIRST_TEXT_ABOUT_EDIT_ARTICLE = '''//div[3]/table[1]//span[8]/span[text(='(watchlist-description'][1]'''
    HIDDEN_WELCOME_POPUP = '''//h1[text(='Добро пожаловать в улучшенную версию списка наблюдения']/parent::div/parent::div[contains(@style, 'display: none']'''
    WELCOME_POPUP = '//div[@id="gt-WlFiltersIntro-Welcome"]'
    CLOSE_WELCOME_POPUP = '''//a[text(='Да, всё понятно']'''
    SELECT_FIRST_ENTRY_FOR_DELETE_FROM_WATCHLIST = '//input[@id="ooui-php-1"]/parent::span/span'  # Выбор первой записи для удаления из списка наблюдения
    CONFIRM_DELETE_BUTTON = '//button[@type="submit"]'  # Кнопка 'Удалить записи'/'Сохранить список'/'Очистить спиоск наблюдения'
    TEXT_ABOUT_DELETED_1_ENTRY = '''//div[text(='Из вашего списка наблюдения была удалена 1 запись:']'''  # Текст 'Из вашего списка наблюдения была удалена 1 запись'
    MOVE_TO_FIRST_LINK_FROM_WATCHLIST = '//label[@for="ooui-php-1"]/a'  # Переход по первой записи в списке наблюдения
    ADD_ARTICLE_TO_WATCHLIST_AS_TEXT_INPUT = '//textarea[@name="wpTitles"]'  # Поле для ввода названия статьи для добавления в список наблюдения как текста
    TEXT_ABOUT_ADDED_1_ENTRY = '''//div[text(='Ваш список наблюдения сохранён. Была добавлена 1 запись:']'''  # Текст 'Ваш список наблюдения сохранён. Была добавлена 1 запись'
    TEXT_ABOUT_CLEANING_WATCHLIST = '''//div[text(='Ваш список наблюдения очищается. Это может занять некоторое время.']'''  # Текст 'Ваш список наблюдения очищается. Это может занять некоторое время'
    TEXT_ABOUT_CLEARED_WATCHLIST = '''//div[contains(text(, 'Ваш список наблюдения очищен.']'''  # Текст 'Ваш список наблюдения очищен'


class NewPagesLocators:

    NEW_PAGES_TITLE = '''//h1[text(='Новые страницы']'''
    MOVE_TO_CREATED_AUTOTEST_ARTICLE = '//div[@id="mw-content-text"]//li[1]/a[2]'
    MOVE_TO_NEW_ARTICLE = '//div[@id="mw-content-text"]/ul/li[1]/strong/a'  # Первая новая страница (статья


class ArticleMasterLocators:

    ARTICLE_MASTER_TITLE = '''//h1/span[text(='Авторам']'''
    ARTICLE_MASTER_TITLE_2 = '''//h1/span[text(='Мастер статей']'''
    NEXT_STEP_BUTTON = '''//span[text(='Следующий шаг']/parent::a'''  # Кнопка Следующий шаг (переход из 1 шага во 2
    WRITING_ABOUT_SOMETHING_BUTTON = '//a[@title="Рувики:Мастер статей/Общая значимость"]'  # Кнопка 'Я пишу о чем-то еще'
    ARTICLE_IS_NOT_ADVERTISEMENT_BUTTON = '//a[@title="Рувики:Мастер статей/Источники"]'  # Кнопка 'Моя статья - не реклама и описывает значимый объект'
    ARTICLE_WITH_GOOD_SOURCES_BUTTON = '//a[@title="Рувики:Мастер статей/Контент"]'  # Кнопка 'Моя статья подкреплена хорошими источниками'
    ARTICLE_IS_NEUTRAL_BUTTON = '//a[@title="Рувики:Мастер статей/Готово"]'  # Кнопка 'Моя статья нейтральна, показывает значимость и ниоткуда не скопирована'
    ARTICLE_NAME_INPUT = '//input[@value="Создать статью"]/parent::form/input[@name="title"]'  # Поле для ввода названия статьи
    CONFIRM_CREATE_ARTICLE_WITH_ARTICLE_MASTER = '//input[@value="Создать статью"]'  # Кнопка 'Создать статью'


class DiscussionPageLocators:

    DISCUSSION_PERSONAL_PAGE_TITLE = '''//span[text(='Обсуждение участника']/parent::h1'''
    CLEARED_DISCUSSION_MEMBER_PAGE_TITLE = '''//h3[contains(text(, 'Начать обсуждение с']'''  # Чистая страница обсуждения участника (при переходе на чужую страницу участника
    CLEARED_DISCUSSION_PERSONAL_PAGE_TITLE = '''//h3[text(='Добро пожаловать на вашу страницу обсуждения']'''  # Чистая личная страница обсуждения  (при переходе на личную страницу участника
    START_DISCUSSION = '//div[@class="ext-discussiontools-emptystate-text"]/span/a'  # Кнопка 'Начать обсуждение'
    THEME_DISCUSSION_INPUT_NEW = '//input[@placeholder="Тема"]'
    THEME_DISCUSSION_INPUT_OLD = '//input[@name="wpSummary"]'
    DESCRIPTION_INPUT = '//div[@aria-label="Описание"]'
    CONFIRM_ADDING_THEME = '//a[contains(@title, "Добавить тему"]'
    DISCUSSION_TITLE = '''//h1/span[text(='Обсуждение']'''
    DISCUSSION_TEXT = '''//p[contains(text(, 'autotest-discuss-description']'''
    DISCUSSION_EDITED_TEXT = '''//p[text(='autotest-discuss-description-- ']'''
    ANSWER_ON_DISCUSSION = '//div[@class="mw-parser-output"]/p/span[2]/span[1]/a'  # Кнопка 'Ответить'
    ANSWER_TO_ANSWER_ON_DISCUSSION = '//dl/dd[2]/span/span/a'  # Кнопка 'Ответить' для ответа на оставленный ранее ответ
    ANSWER_INPUT = '//div[@role="textbox"]'  # Поле для ввода текста для ответа
    CANCEL_ANSWER = '//a[@title="Отмена [Escape]"]'  # Кнопка 'Отмена'
    CONFIRM_CANCEL_BUTTON = '''//span[text(='Отменить сообщение']/parent::a'''  # Кнопка 'Отменить сообщение', подтверждающее отмену
    NOT_CONFIRM_CANCEL_BUTTON = '''//span[text(='Продолжить писать']/parent::a'''  # Кнопка 'Продолжить писать'
    CONFIRM_CANCEL_CREATE_NEW_THEME = '''//span[text(='Отменить тему']/parent::a'''  # Кнопка 'Отменить тему'
    CONFIRM_ANSWER = '//a[contains(@title, "Ответить [Ctrl+Enter]"]'  # Кнопка 'Ответить'
    ANSWER_SAVED_TEXT = '''//dd/dl/dd[text(='autotest-answer']'''
    MENTION_MEMBERS = '//a[@title="Упомянуть участника"]'  # Кнопка 'Упомянуть участника'
    SELECT_FIRST_MEMBER_FROM_LIST = '//div[1]/div[@class="oo-ui-popupWidget-popup"]/div/div/div[2]'  # Выбрать первого участника из списка
    MOVE_TO_VISUAL = '''//span[text(='Визуально']'''  # Кнопка 'Визуально'
    MOVE_TO_CODE = '''//span[text(='Код']'''  # Кнопка 'Код'
    BOLD_TEXT = '//a[@title="Полужирный Ctrl+B"]'  # Полужирный шрифт
    ITALIC_TEXT = '//a[@title="Курсив Ctrl+I"]'  # Курсивный шрифт
    TEXT_STYLE_BUTTON = '//div[@title="Стиль текста"]/span'  # Кнопка 'Стиль текста'
    UNDERLINED_TEXT = '''//span[text(='Подчёркнутый']/parent::a'''  # Подчеркнутый текст
    SPEC_SYMBOLS_LIST = '//a[@title="Специальный символ"]'  # Кнопка 'Специальный символ', отрывающая список специальных символов
    LATIN_SPEC_SYMBOLS = '//div[@class="oo-ui-menuLayout-menu"]/div/div/div[2]'  # Латинские специальные символы
    FIRST_LATIN_SYMBOL = '''//div[text(='Á']'''  # Первый латинский символ
    ADD_LINK = '//a[@title="Ссылка Ctrl+K"]'  # Кнопка 'Ссылка', добавляющая ссылки
    ADD_LINK_INPUT = '//input[@aria-label="Поиск внутренних страниц"]'  # Поле для вставки ссылок
    SELECT_FIRST_LINK_FROM_LIST = '//div[@class="oo-ui-searchWidget-results"]/div/div[1]'  # Выбрать первую ссылку
    TEXT_FROM_ANSWER = '''//dl/dd[2]/dl/dd[text(='autotest-answer-to-answer@']'''  # Текст 'autotest-answer-to-answer@' из ответа
    MENTION_FROM_ANSWER = '''//dl/dd[2]/dl/dd/a[text(='Shadowman']'''  # Упоминание пользователя из ответа
    BOLD_TEXT_FROM_ANSWER = '''//dl/dd[2]/dl/dd[2]/b[text(='bold']'''  # Текст полужирным шрифтом из ответа
    ITALIC_TEXT_FROM_ANSWER = '''//dl/dd[2]/dl/dd/i[text(='italic']'''  # Текст курсивным шрифтом из ответа
    UNDERLINED_TEXT_FROM_ANSWER = '''//dl/dd[2]/dl/dd/u[text(='underlined']'''  # Текст подчеркнутым шрифтом из ответа
    SPEC_SYMBOLS_FROM_ANSWER = '''//dl/dd[2]/dl/dd[text(='ÁÁÁÁÁ']'''  # Специальные символы ÁÁÁÁÁ из ответа
    SPEC_SYMBOLS_FROM_ANSWER_2 = '''//dl/dd[2]/dl/dd[text(='ÁÁÁÁ']'''  # Специальные символы ÁÁÁÁ из ответа
    LINK_FROM_ANSWER = '''//dl/dd[2]/dl/dd/a[contains(@title, "Автомобиль"]'''  # Ссылка на статью 'Автомобиль' из ответа
    ADD_THEME_BUTTON = '//a[@id="ca-addsection"]'  # Кнопка 'Добавить тему'
    OPEN_ADDITIONALLY_DESCRIPTION = '''//span[text(='Дополнительно']/parent::a'''  # Кнопка 'Дополнительно'
    ADDITIONALLY_DESCRIPTION_INPUT = '//div[contains(@class, "oo-ui-messageWidget"]/div/div/div[1]//input'  # Поле для ввода дополнительного описания
    HIDDEN_ADDITIONALLY_DESCRIPTION_INPUT = '//div[contains(@class, "oo-ui-messageWidget oo-ui-element-hidden"]/div/div/div[1]//input'
    NEW_THEME_TITLE = '//span[@id="New-Autotest-Theme?"]'
    NEW_THEME_DESCRIPTION = '''//p[text(='new-autotest-discuss-description ']'''
    USER_SIGNATURE_LINK = '//div[@class="mw-parser-output"]/p[1]/a[1]'


class AlertsPageLocators:

    FIRST_ALERT_ABOUT_MENTION = '''//span[text(='Оповещения']/parent::div/parent::div//a[1]//div[contains(text(, 'упомянул вас на странице обсуждения']'''  # Первое уведомление об упоминании


class AllPagesLocators:

    ALL_PAGES_TITLE = '''//h1[text(='Все страницы']'''
    DISPLAY_PAGES_INPUT = '//input[@name="from"]'  # Поле ввода 'Вывести страницы, начинающиеся на:'
    OPEN_NAMESPACE_LIST = '//span[@role="combobox"]'  # Открыть список 'Пространство имён'
    SELECT_MEMBER_FROM_NAMESPACE_LIST = '''//span[text(='Участник']/parent::div'''  # Выбрать в списке 'Пространство имён' значение 'Участник'
    CONFIRM_DISPLAY_PAGES_BUTTON = '//button[@type="submit"]'  # Кнопка 'Выполнить'
    MOVE_TO_FIRST_LINK = '//ul[@class="mw-allpages-chunk"]/li[1]/a'  # Выбрать первую ссылку на странице 'Все страницы'


class RedirectPageLocators:

    REDIRECT_PAGE_TITLE = '''//h1[text(='Список перенаправлений']'''


class IndexStartArticleLocators:

    INDEX_TO_THE_START_ARTICLE_TITLE = '''//h1[text(='Указатель по началу названий страниц']'''
    SEARCH_PAGE_INPUT = '//input[@name="prefix"]'  # Поле для ввода названия страниц
    CONFIRM_PAGE_BUTTON = '//button[@value="Показать"]'  # Кнопка 'Показать'
    FIRST_LINK_AFTER_SEARCH = '//ul[@class="mw-prefixindex-list"]/li[1]/a'  # Первая ссылка после поиска страниц


class CreateAccountLocators:

    CREATE_ACCOUNT_TITLE = '''//h1[text(='Создать учётную запись']'''
    ACCOUNT_NAME_INPUT = '//div[@id="userloginForm"]//input[@name="wpName"]'  # Поле 'Имя учётной записи'
    PASSWORD_INPUT = '//div[@id="userloginForm"]//input[@name="wpPassword"]'  # Поле 'Пароль'
    CONFIRM_PASSWORD_INPUT = '//input[@name="retype"]'  # Поле 'Подтвердите пароль'
    AGREE_POLITICS_CHECKBOX = '//input[@name="wptermsofservice-0"]'
    AGREE_PERSONAL_DATA_CHECKBOX = '//input[@name="wptermsofservice-1"]'
    AGREE_NO_PROHIBITIONS_CHECKBOX = '//input[@name="wptermsofservice-2"]'
    CONFIRM_CREATE_ACCOUNT_BUTTON = '//button[@name="wpCreateaccount"]'  # Кнопка 'Создать учетную запись'


class AccountMergeLocators:

    ACCOUNT_MERGE_TITLE = '''//h1[text(='Состояние объединения учётных записей']'''


class BlockUserLocators:

    BLOCK_USER_TITLE = '''//h1[text(='Блокировка участника']'''
    USER_NAME_IP_INPUT = '//input[@name="wpTarget"]'  # Поле для ввода имени пользователя, IP, диапазона IP-адресов
    CONFIRM_SEARCH_FROM_BLOCKED_USERS_BUTTON = '//button[@value="Найти"]'
    EDIT_PERSONAL_DISCUSS_PAGE_CHECKBOX = '//input[@name="wpDisableUTEdit"]/parent::span/span'  # Чекбокс 'Редактирование своей страницы обсуждения'
    OPEN_DATE_LIST = '//div[3]/div/div/span[@role="combobox"]'  # Открыть выпадающий список для выбора срока блокировки
    SELECT_1_DAY_FOR_BLOCK = '''//span[text(='1 день']/parent::div'''  # Выбрать значение 1 день из выпадающего списка для выбора срока блокировки
    SELECT_15_MIN_FOR_BLOCK = '''//span[text(='15 минут']/parent::div'''  # Выбрать значение 1 день из выпадающего списка для выбора срока блокировки
    OTHER_REASON_INPUT = '//input[@name="wpReason-other"]'  # Поле для ввода причины блокировки
    ADD_PERSONAL_PAGE_AND_DISCUSS_PAGE_TO_WATCHLIST_CHECKBOX = '//input[@name="wpWatch"]/parent::span/span'  # Чекбокс 'Добавить в список наблюдения личную страницу участника и его страницу обсуждения'
    BLOCK_IP_WITH_USER_CHECKBOX = '//span[@id="mw-input-wpAutoBlock"]/span'  # Чекбокс "Автоматически блокировать последний IP-адрес, использованный этим участником, и любые последующие IP-адреса, с которых он пытается редактировать, на период 1 день"
    BLOCK_USER_BUTTON = '//button[@type="submit"]'  # Кнопка 'Заблокировать этот адрес/участника'
    BLOCK_USER_SUCCESS_TITLE = '''//h1[text(='Блокировка произведена']'''
    CONFIRM_CREATE_REQUEST_FOR_UNBLOCK = '//input[@name="wpSave"]'
    REQUEST_ABOUT_UNBLOCK_TITLE = '//span[@id="Просьба_о_разблокировке"]'


class AutoLocksLocators:

    AUTO_LOCK_TITLE = '''//h1[text(='Автоблокировки']'''


class BlockedUsersLocators:

    USER_NAME_INPUT = '//input[@name="wpTarget"]'
    CONFIRM_SEARCH_USER_BUTTON = '//button[@type="submit"]'
    TEXT_ABOUT_USER_NOT_FIND_FROM_TABLE = '''//p[contains(text(, 'Не найдено блокировок']'''


class UnblockUserLocators:

    USER_NAME_INPUT = '//input[@name="wpTarget"]'
    UNBLOCK_REASON_INPUT = '//input[@name="wpReason"]'
    UNBLOCK_BUTTON = '//button[@type="submit"]'
    TEXT_ABOUT_UNBLOCKED_USER = '''//p[text(=' разблокирован. ']'''


class MemberVerificationLocators:

    USER_NAME_INPUT = '//input[@name="user"]'
    REASON_FOR_SEARCH_USER = '//input[@name="reason"]'
    SEARCH_MEMBER_BUTTON = '//button[@type="submit"]'
    FIRST_DISPLAY_USER_IP = '//a[@title="Служебная:Проверить участника"]/bdi'


class RightsGroupsMembersLocators:

    RIGHTS_GROUPS_TITLE = '''//h1[text(='Права групп участников']'''
    GROUP_COLUMN = '''//tr/th[text(='Группа']'''  # Столбец 'Группа' в таблице прав участников
    RIGHTS_COLUMN = '''//tr/th[text(='Права']'''  # Столбец 'Права' в таблице прав участников
    DISPLAY_USER_INPUT = '//input[@name="username"]'  # Поле для ввода логина в списке участников
    CONFIRM_SEARCH_BUTTON = '//button[@type="submit"]'  # Кнопка 'Показать'
    FIRST_MEMBER_NAME_AFTER_SEARCH = '//div[@id="mw-content-text"]/ul/li[1]/a/bdi'  # Имя первого участника в списке участников


class RightsMembersLocators:

    RIGHTS_MEMBERS_TITLE = '''//h1[text(='Права участника']'''
    DISPLAY_USER_RIGHTS_INPUT = '//input[@name="user"]'  # Поле для ввода имени участника, чьи права необходимо найти
    CONFIRM_DISPLAY_USER_RIGHTS = '//input[@value="Загрузить группы участника"]'  # Кнопка 'Загрузить группы участника'
    GROUP_LIST_WHICH_CAN_BE_EDITED = '''//th[text(='Группы, которые вы можете изменять']'''  # Список групп, которые можно изменить
    SELECT_AUTO_PATROL_ROLE_CHECKBOX = '//label[@for="wpGroup-autoreview"]'
    SELECTED_AUTO_PATROL_ROLE_CHECKBOX = '//label[@for="wpGroup-autoreview"]/parent::div/input[@checked="checked"]'
    OPEN_DURATION_RIGHTS_AUTO_PATROL_ROLE_LIST = '//select[@id="mw-input-wpExpiry-autoreview"]'
    SELECT_1_HOUR_DURATION_AUTO_PATROL_ROLE_RIGHTS = '//select[@id="mw-input-wpExpiry-autoreview"]/option[@value="1 day"]'
    SELECT_SUMMARIZING_ROLE_CHECKBOX = '//label[@for="wpGroup-closer"]'
    SELECTED_SUMMARIZING_ROLE_CHECKBOX = '//label[@for="wpGroup-closer"]/parent::div/input[@checked="checked"]'
    OPEN_DURATION_RIGHTS_SUMMARIZING_ROLE_LIST = '//select[@id="mw-input-wpExpiry-closer"]'
    SELECT_1_HOUR_DURATION_SUMMARIZING_ROLE_RIGHTS = '//select[@id="mw-input-wpExpiry-closer"]/option[@value="1 day"]'
    SELECT_PATROL_ROLE_CHECKBOX = '//label[@for="wpGroup-editor"]'
    SELECTED_PATROL_ROLE_CHECKBOX = '//label[@for="wpGroup-editor"]/parent::div/input[@checked="checked"]'
    OPEN_DURATION_RIGHTS_PATROL_ROLE_LIST = '//select[@id="mw-input-wpExpiry-editor"]'
    SELECT_1_HOUR_DURATION_PATROL_ROLE_RIGHTS = '//select[@id="mw-input-wpExpiry-editor"]/option[@value="1 day"]'
    SELECT_EXCLUSION_IP_BLOCK_ROLE_CHECKBOX = '//label[@for="wpGroup-ipblock-exempt"]'
    SELECTED_EXCLUSION_IP_BLOCK_ROLE_CHECKBOX = '//label[@for="wpGroup-ipblock-exempt"]/parent::div/input[@checked="checked"]'
    OPEN_DURATION_RIGHTS_EXCLUSION_IP_BLOCK_ROLE_LIST = '//select[@id="mw-input-wpExpiry-ipblock-exempt"]'
    SELECT_1_HOUR_DURATION_EXCLUSION_IP_BLOCK_ROLE_RIGHTS = '//select[@id="mw-input-wpExpiry-ipblock-exempt"]/option[@value="1 day"]'
    SELECT_ROLLBACK_ROLE_CHECKBOX = '//label[@for="wpGroup-rollbacker"]'
    SELECTED_ROLLBACK_ROLE_CHECKBOX = '//label[@for="wpGroup-rollbacker"]/parent::div/input[@checked="checked"]'
    OPEN_DURATION_RIGHTS_ROLLBACK_ROLE_LIST = '//select[@id="mw-input-wpExpiry-rollbacker"]'
    SELECT_1_HOUR_DURATION_ROLLBACK_ROLE_RIGHTS = '//select[@id="mw-input-wpExpiry-rollbacker"]/option[@value="1 day"]'
    SELECT_STEWARD_ROLE_CHECKBOX = '//label[@for="wpGroup-steward"]'  # Выбрать роль 'Стюард'
    SELECTED_STEWARD_ROLE_CHECKBOX = '//label[@for="wpGroup-steward"]/parent::div/input[@checked="checked"]'
    OPEN_DURATION_RIGHTS_STEWARD_ROLE_LIST = '//select[@name="wpExpiry-steward"]'  # Открыть список выбора времени длительности действия прав
    SELECT_1_HOUR_DURATION_STEWARD_ROLE_RIGHTS = '//select[@name="wpExpiry-steward"]/option[@value="1 day"]'  # Выбор длительности действия прав - 1 день
    SELECT_ADMIN_ROLE_CHECKBOX = '//label[@for="wpGroup-sysop"]'  # Выбрать роль 'Администратор'
    SELECTED_ADMIN_ROLE_CHECKBOX = '//label[@for="wpGroup-sysop"]/parent::div/input[@checked="checked"]'
    OPEN_DURATION_RIGHTS_ADMIN_ROLE_LIST = '//select[@name="wpExpiry-sysop"]'
    SELECT_1_HOUR_DURATION_ADMIN_ROLE_RIGHTS = '//select[@name="wpExpiry-sysop"]/option[@value="1 day"]'
    CONFIRM_EDIT_RIGHTS_BUTTON = '//input[@name="saveusergroups"]'  # Кнопка 'Сохранить группы участника'
    CURRENT_STEWARD_ON_USER_GROUP = '''//p[text(='Состоит в группах: ']/a[text(='Стюарды']'''  # Текст о том, что участник входит в группу 'Стюарды'
    TEXT_ABOUT_EDITED_RIGHTS = '''//li[text(=' изменил членство в группах для ']'''  # Текст об изменении прав участника
    DEFAULT_USER_RIGHTS_GROUP = '''//p[text(='Неявно состоит в группах: ']'''
    FIRST_USER_RIGHTS_GROUP = '''//p[text(='Состоит в группах: ']/a[1]'''  # Первая группа из списка групп, в которых состоит пользователь
    SECOND_USER_RIGHTS_GROUP = '''//p[text(='Состоит в группах: ']/a[2]'''  # Вторая группа из списка групп, в которых состоит пользователь


class MembersListLocators:

    MEMBERS_LIST_TITLE = '''//h1[text(='Список участников']'''
    FIRST_MEMBER_LINK = '//div[@id="mw-content-text"]/ul/li[1]/a'


class NewFilesGalleryLocators:

    USER_NAME_INPUT = '//input[@name="user"]'  # Поле для ввода имени участника
    CONFIRM_SEARCH_BUTTON = '//button[@value="Найти"]'  # Кнопка 'Найти'
    SELECT_FILE = '//div[@class="gallerytext"]/a[@title="Файл:Красивая машина.jpg"]'  # Файл 'Красивая машина'


class LogsLocators:

    LOGS_TITLE = '''//h1[text(='Все доступные журналы']'''
    OPEN_LOGS_LIST = '//div[@class="oo-ui-widget oo-ui-widget-enabled"]//span[@role="combobox"]'  # Открыть выпадающий список выбора журнала
    SELECT_STABILIZATION_LOG = '''//span[text(='Журнал стабилизаций']/parent::div[@role="option"]'''  # Выбрать 'Журнал стабилизаций' из выпадающего списка
    SELECT_BLACKLIST_LOG = '''//span[text(='Журнал списка запрещённых заголовков']/parent::div[@role="option"]'''  # Выбрать 'Журнал списка запрещённых заголовков' из выпадающего списка
    SELECT_BLACKLIST_LINKS_LOG = '''//span[text(='Журнал списка запрещённых ссылок']/parent::div[@role="option"]'''  # Выбрать 'Журнал списка запрещённых ссылок' из выпадающего списка
    SELECT_REGISTRATION_MEMBERS_LOG_CHECKBOX = '//input[@value="newusers"]/parent::span/span'  # Чекбокс 'Журнал регистрации участников'
    CONFIRM_SEARCH_BUTTON = '//button[@type="submit"]'  # Кнопка 'Показать'
    FIRST_LINK_FROM_LOGS_PAGE = '//ul[@class="mw-logevent-loglines"]/li[1]/a[3]'  # Выбрать первую ссылку после поиска
    SECOND_LINK_FROM_LOGS_PAGE = '//ul[@class="mw-logevent-loglines"]/li[2]/a[3]'  # Выбрать вторую ссылку после поиска
    FIRST_LINK_STABILIZE_PAGE = '''//li[text(=' стабилизировал страницу '][1]/a[3]'''
    FIRST_LINK_STABILIZE_FILE = '''//li[text(=' стабилизировал страницу '][1]/a[3][contains(@title, 'Файл']'''
    SECOND_LINK_STABILIZE_PAGE = '''//li[text(=' стабилизировал страницу '][2]/a[3]'''
    SELECT_FIRST_RECORD = '//ul[@class="mw-logevent-loglines"]/li[1]/input'
    SELECT_ALL_RECORDS = '//div[1]/a[@class="mw-checkbox-all"]'
    EDIT_TAGS_SELECTED_VERSIONS = '//button[@name="editchangetags"][1]'
    VIEW_HIDE_SELECTED_VERSIONS = '//button[@name="revisiondelete"][1]'
    RETURN_TO_LOGS = '''//a[text(='Показать журналы для этой страницы']'''
    BLACKLIST_LOG_TITLE = '''//h1[text(='Журнал списка запрещённых заголовков']'''
    BLACKLIST_LOG_LINKS_TITLE = '''//h1[text(='Журнал списка запрещённых ссылок']'''
    HIDE_TARGET_AND_OPTIONS_CHECKBOX = '//input[@name="wpHidePrimary"]/parent::span/span'  # Чекбокс "Скрыть цель и параметры"
    DESCRIPTION_CHANGES_CHECKBOX = '//input[@name="wpHideComment"]/parent::span/span'  # Чекбокс "Описание изменений"
    MEMBERS_NAME_OP_IP_CHECKBOX = '//input[@name="wpHideUser"]/parent::span/span'  # Чекбокс "Имя участника/IP-адрес"
    TEXT_ABOUT_SELECTED_VERSIONS_EDITED = '''//div[text(='Видимость события изменена.']'''
    TEXT_ABOUT_SELECTED_VERSIONS_EDITED_FROM_FIRST_RECORD_LOG = '''//ul[@class="mw-logevent-loglines"]/li[text(=' изменил видимость записи журнала для ' and ': содержание скрыто, описание правки скрыто и имя участника скрыто  '][1]'''


class UploadFileLocators:

    UPLOAD_FILE_PAGE_TITLE = '''//h1[text(='Загрузить файл']'''
    REQUEST_UNBLOCK_USER_BUTTON = '''//span[text(='Запросить разблокировку']/parent::a'''
    BLOCKED_MEMBER_TITLE = '''//h1[text(='Участник заблокирован']'''


class FilesListLocators:

    FILES_LIST_TITLE = '''//h1[text(='Список файлов']'''


class MediaStatisticLocators:

    MEDIA_STATISTIC_LOCATORS = '''//h1[text(='Статистика медиа']'''


class EditFilterLocators:

    EDIT_FILTER_TITLE = '''//h1[text(='Фильтр правок']'''


class PagesImportLocators:

    PAGES_IMPORT_TITLE = '''//h1[text(='Импорт страниц']'''


class MassDeleteLocators:

    MASS_DELETE_TITLE = '''//h1[text(='Множественное удаление']'''


class UnverifiedPagesLocators:

    UNVERIFIED_PAGES_TITLE = '''//h1[text(='Непроверенные страницы']'''


class SendMassMessageLocators:

    SEND_MASS_MESSAGE_TITLE = '''//h1[text(='Отправить массовое сообщение']'''


class NotificationsPageLocators:

    NOTIFICATION_PAGE_TITLE = '''//h1[text(='Уведомления']'''


class DeleteUndeleteArticleLocators:

    DELETING_ARTICLE_PAGE_TITLE = '''//h1[contains(text(, 'удаление']'''  # Страница удаления статей
    SUCCESS_DELETE_TITLE = '''//h1[text(='Действие выполнено']'''  # Заголовок страницы после удаления статьи/файла/отката правки
    DELETE_SECTION = '''//span[text(='Удаление']/parent::legend'''
    OPEN_DELETE_CAUSE_LIST = '//div[@id="ooui-php-2"]/div/span'  # Открыть список причин для удаления статьи
    SELECT_TEST_PAGE_CAUSE = '''//span[text(='О2: тестовая страница']/parent::div'''  # Выбрать причину 'тестовая страница'
    SELECT_TEST_FILE_CAUSE = '''//span[text(='О2: тестовый файл']/parent::div'''  # Выбрать причину 'тестовый файл'
    DELETE_LINKED_TALK_PAGE = '//input[@name="wpDeleteTalk"]/parent::span/span'  # Чекбокс 'Удалить соответствующую страницу обсуждения'
    FOLLOW_PAGE_CHECKBOX = '//input[@name="wpWatch"]/parent::span/span'  # Чекбокс 'Следить за страницей'
    CONFIRM_DELETE_ARTICLE = '//button[@type="submit"]'  # Кнопка 'Удалить страницу' на странице удаления статьи
    TEXT_ABOUT_DELETED_ARTICLE = '''//p[text(='Страница «' and text(='» удалена']'''  # Текст о том, что страница удалена после удаления статьи
    TEXT_ABOUT_DELETED_ARTICLE_AND_LINKED_TALK = '''//p[contains(text(, 'Страницы' and contains(text(, 'были удалены']'''  # Текст о том, что страница и соответствующее обсуждение удалены после удаления статьи
    TEXT_ABOUT_DELETED_FILE = '''//div[@class="ts-editnotice-text"]/p[text(='Файл ' and text(=' был удалён (']'''  # Текст о том, что файл удален после
    TEXT_ABOUT_DELETED_DATA_ELEMENT = '''//p[contains(text(, 'has been deleted']'''  # Текст об удалении элемента в Рувики.Данные
    MOVE_TO_DELETE_LOG = '//a[@title="Служебная:Журналы/delete"]'  # Ссылка на журнал удалений после удаления элемента в Рувики.Данные
    UNDELETE_ARTICLE_PAGE_TITLE = '''//h1[text(='Просмотр и восстановление удалённых страниц']'''  # Страница восстановления статьи
    USER_LINKED_DELETED_ARTICLE = '//li[@data-mw-logaction="delete/delete"]//bdi'
    RESTORE_VERSIONS_SECTION = '''//span[text(='Восстановить версии']/parent::legend'''  # Блок 'Восстановить версии'
    CAUSE_INPUT = '//input[@name="wpComment"]'  # Поле для ввода причины восстановления
    REASON_INPUT = '//input[@name="wpReason"]'  # Поле для ввода причины удаления
    RESTORE_BUTTON = '//button[@type="submit"]'  # Кнопка 'Восстановить'
    INVERT_SELECTION_BUTTON = '//button[@name="invert"]'  # Кнопка 'Обратить выделение'
    HISTORY_SECTION = '''//h2[text(='История']'''  # Блок 'История'
    TEXT_ABOUT_UNDELETED_ARTICLE = '''//p/strong[text(='Страница «' and text(='» была восстановлена.']'''  # Текст о том, что страница восстановлнеа после удаления статьи
    MOVE_TO_RESTORED_ARTICLE_LINK = '//p/strong/a'


class InterwikiSettingsLocators:

    INTERWIKI_SETTINGS_TITLE = '''//h1[text(='Просмотр и изменение настроек интервики']'''  # Название 'Просмотр и изменение настроек интервики'
    ADD_PREFIX_BUTTON = '//a[@title="Служебная:Интервики/add"]'  # Кнопка 'Добавить языковой или интервики-префикс'
    PREFIX_NAME_INPUT = '//input[@name="prefix"]'  # Поле для ввода названия префикса
    URL_INPUT = '//input[@name="wpInterwikiURL"]'  # Поле для ввода URL
    CONFIRM_ADD_PREFIX_BUTTON = '//button[@type="submit"]'  # Кнопка 'Добавить'
    TEXT_ABOUT_ADDING_PREFIX = '''//div[@id="mw-content-text"]/p[contains(text(, 'добавлен в таблицу интервики.']'''  # Текст 'Префикс «***» добавлен в таблицу интервики.'
    BACK_TO_INTERWIKI_PAGE_LINK = '//a[@title="Служебная:Интервики"]'  # Ссылка на возврат к служебной странице 'Интервики'
    INTERWIKI_PREFIX_FROM_TABLE = '''//tr[@class="mw-interwikitable-row"]/td[text(='en']/parent::tr/td[text(='https://en.wikipedia.org/wiki/$1']'''  # Префикс и его URL в таблице
    EDIT_PREFIX_BUTTON = '''//tr[@class="mw-interwikitable-row"]/td[text(='en']/parent::tr/td[text(='https://en.wikipedia.org/wiki/$1']/parent::tr/td[5]/a[1]'''  # Кнопка 'Править'
    REPLY_CHECKBOX = '//input[@name="local"]'  # Чекбокс 'Пересылка'
    TURN_ON_CHECKBOX = '//input[@name="trans"]'  # Чекбокс 'Включение'
    REASON_INPUT = '//input[@name="wpInterwikiReason"]'  # Поле для ввода 'Причина'
    CONFIRM_EDIT_PREFIX = '//button[@type="submit"]'  # Кнопка 'Править'
    TEXT_ABOUT_EDITING_PREFIX = '''//div[@id="mw-content-text"]/p[contains(text(, 'изменён в таблице интервики.']'''  # Текст 'Префикс «***» изменён в таблице интервики.'
    DELETE_PREFIX_BUTTON = '''//tr[@class="mw-interwikitable-row"]/td[text(='en']/parent::tr/td[text(='https://en.wikipedia.org/wiki/$1']/parent::tr/td[5]/a[2]'''  # Кнопка 'Удалить'
    DELETE_REASON_INPUT = '//input[@name="reason"]'  # Поле для ввода причины для удаления префикса
    CONFIRM_DELETE_BUTTON = '//button[@type="submit"]'  # Кнопка 'Удалить'
    TEXT_ABOUT_DELETING_PREFIX = '''//div[@id="mw-content-text"]/p[contains(text(, 'удалён из таблицы интервики.']'''  # Текст 'Префикс «***» удалён из таблицы интервики.'


class ContributionLocators:

    CONTRIBUTION_TITLE = '''//h1[contains(text(, 'Вклад участника']'''
    TEXT_ABOUT_EDIT_AS_BOT = '''//p[text(='Откачены правки ']/span/bdi[contains(text(, 'imported']'''


class DebugBarLocators:

    BOTTOM_BAR = '//div[@id="mw-debug-toolbar"]/div[@style="display: block;"]'
    CONSOLE_SECTION = '//div[@id="mw-debug-console"]'


class ChangesFilterLogLocators:

    CHANGES_FILTER_LOG_TITLE = '''//h1[text(='Журнал фильтра правок']'''  # Название 'Журнал фильтра правок'
    MOVE_TO_LIST_BUTTON = '''//span[text(='Список']/parent::a'''  # Кнопка 'Список'
    MOVE_TO_CHANGES_FILTERS = '''//span[text(='Последние изменения фильтров']/parent::a'''  # Кнопка 'Последние изменения фильтров'
    MOVE_TO_STUDY_OF_EDITS = '''//span[text(='Изучение правок']/parent::a'''  # Кнопка 'Изучение правок'
    MOVE_TO_OPERATIONS_LOG = '''//span[text(='Журнал срабатываний']/parent::a'''  # Кнопка 'Журнал срабатываний'
    MOVE_TO_FILTER_WITH_LIMIT_ACTIONS = '''//td[text(='Предупреждение, Отклонение']/parent::tr/td[2]/a'''  # Переход к первому фильтру с ограничивающими действиями
    MOVE_TO_FILTER_WITH_OTHER_LIMIT_ACTIONS = '''//td[text(='Отклонение']/parent::tr/td[2]/a'''  # Переход к первому фильтру с ограничивающими действиями
    TAKING_ACTION_AFTER_WARNING_MEMBER_CHECKBOX = '''//label[text(='Принимать эти меры после предупреждения участника']'''  # Чекбокс 'Принимать эти меры после предупреждения участника'
    CONFIRM_EDIT_FILTER = '//input[@value="Сохранить фильтр (если вы знаете, что делаете"]'  # Кнопка 'Сохранить фильтр'
    TEXT_ABOUT_EDIT_FILTER = '''//p[text(='Вы успешно сохранили изменения в ']'''  # Текст об успешном изменении фильтра
    CREATE_NEW_FILTER_BUTTON = '''//span[text(='Создать новый фильтр']/parent::a'''
    MOVE_TO_FIRST_FILTER = '//tbody/tr[@class="mw-abusefilter-list-enabled"][1]/td[2]/a'  # Переход к первому фильтру
    MOVE_TO_OPERATIONS_FIRST_FILTER = '//tbody/tr[@class="mw-abusefilter-list-enabled"][1]/td[7]/a'  # Переход к срабатываниям первого фильтра
    MOVE_TO_DETAILS_OPERATIONS_FIRST_FILTER = '''//ul[@class="plainlinks"]/li[1]/a[text(='подробности']'''  # Переход к подробностям первого срабатывания первого фильтра
    CONFIRM_DISPLAY_PRIVATE_DATA_BUTTON = '//button[@type="submit"]'  # Кнопка 'Просмотр личных данных'
    PRIVATE_DATA_SECTION = '''//legend[text(='Личные данные в журнале']'''  # Блок с личными данными
    SEARCH_SETTINGS_DISPLAY = '//legend[@role="button"]'  # Кнопка 'Настройки поиска'
    SELECT_VIEW_DELETED_FILTERS_RADIOBUTTON = '//input[@value="show"]/parent::span/span'  # Радиобаттон "Показывать удаленные фильтры"
    SELECTED_VIEW_DELETED_FILTERS_RADIOBUTTON = '//input[@value="show" and @checked="checked"]'  # Выбранный радиобаттон "Показывать удаленные фильтры"
    SELECT_HIDE_DELETED_FILTERS_RADIOBUTTON = '//input[@value="hide"]/parent::span/span'  # Радиобаттон "Скрывать удаленные фильтры"
    SELECTED_HIDE_DELETED_FILTERS_RADIOBUTTON = '//input[@value="hide" and @checked="checked"]'  # Выбранный радиобаттон "Скрывать удаленные фильтры"
    SELECT_HIDE_DISABLED_FILTERS_CHECKBOX = '//input[@value="hidedisabled"]/parent::span/span'  # Чекбокс "Скрыть отключенные фильтры"
    SELECTED_HIDE_DISABLED_FILTERS_CHECKBOX = '//input[@value="hidedisabled" and @checked="checked"]'  # Выбранный чекбокс "Скрыть отключенные фильтры"
    UPDATE_FILTER_SETTINGS = '//button[@value="Обновить"]'  # Кнопка "Обновить"
    MOVE_TO_FIRST_ID_ONLY_ADMIN_FILTER = '''//td[text(='Только администраторы']/parent::tr/td[1]/a'''  # Переход к фильтру по ID первой записи с видимостью "Только администраторы"
    MOVE_TO_FIRST_LAST_CHANGE_ONLY_ADMIN_FILTER = '''//td[text(='Только администраторы']/parent::tr/td[5]/a[1]'''  # Переход к последним изменениям первой записи с видимостью "Только администраторы"
    MOVE_TO_FIRST_ACTIVATIONS_ONLY_ADMIN_FILTER = '''//td[text(='Только администраторы']/parent::tr/td[7]/a'''  # Переход к срабатываниям первой записи с видимостью "Только администраторы"
    MOVE_TO_FIRST_DESCRIPTION_ONLY_ADMIN_FILTER = '''//td[text(='Только администраторы']/parent::tr/td[2]/a'''  # Переход к общедоступному описанию первой записи с видимостью "Только администраторы"
    MOVE_TO_FIRST_RECORD_DELETED_FILTER = '//tbody/tr[1]/td[1]/a'  # Переход к первой записи удаленного фильтра
    MOVE_TO_FIRST_LAST_CHANGE_DELETED_FILTER = '//tbody/tr[1]/td[5]/a[1]'  # Переход к первой записи последних изменений удаленного фильтра
    MOVE_TO_FIRST_RECORD_FILTER = '//tbody/tr[1]/td[1]/a'
    MOVE_TO_AUTHOR_LAST_CHANGE_FIRST_FILTER = '//tbody/tr[1]/td[3]/a'
    MOVE_TO_CHANGES_FILTER = '''//tr[1]//a[text(='Изменения']'''  # Переход по ссылке 'Изменения' в таблице
    DIFFERENCE_BETWEEN_VERSIONS_FILTER_LOG_TITLE = '''//div/h2[text(='Различия между версиями']'''
    EDIT_CHANGES_FILTER_TITLE = '''//h1[text(='Редактировать фильтр правок']'''  # Название страницы 'Редактировать фильтр правок'
    RETURN_TO_PREVIOUS_EDIT = '''//span[text(='Предыдущая правка']/parent::a'''  # Кнопка Возврат к предыдущей правке
    RETURN_TO_EDIT_FILTER = '''//span[text(='Возврат к редактированию фильтра']/parent::a'''  # Кнопка "Возврат к редактированию фильтра"
    ID_FROM_DESCRIPTION_FILTER = '''//td[text(='ID фильтра:']/parent::tr/td[2]'''  # ID фильтра на странице общедоступного описания фильтра
    SEARCH_FILTER_ID_FIELD = '//input[@name="filter"]'  # Поле для ввода ID фильтра на странице 'Изменения фильтров'
    SEARCH_FILTER_AUTHOR_FIELD = '//input[@name="user"]'  # Поле для ввода имени участника на странице 'Изменения фильтров'
    SEARCH_SUBMIT_BUTTON = '//button[@value="Уточнить"]'  # Кнопка 'Уточнить'
    HISTORY_EDIT_FILTER_TITLE = '''//h1[contains(text(, 'История изменений фильтра #']'''
    RETURN_TO_FILTER_HISTORY_BUTTON = '''//span[text(='Вернуться к истории фильтра']/parent::a'''  # Кнопка 'Вернуться к истории фильтра'
    SEARCH_INDIVIDUAL_CHANGES_MEMBER_FIELD = '//input[@name="wpSearchUser"]'
    SEARCH_INDIVIDUAL_CHANGES_DATE_UNTIL_FIELD = '//div[2]/div[@class="oo-ui-fieldLayout-body"][1]/div/div/span/span[3]/input[1]'
    SEARCH_INDIVIDUAL_CHANGES_DATE_AFTER_FIELD = '//div[3]/div[@class="oo-ui-fieldLayout-body"][1]/div/div/span/span[3]/input[1]'
    SEARCH_INDIVIDUAL_CHANGES_BUTTON = '''//span[text(='Найти']/parent::button'''
    FIRST_RECORD_BY_MEMBER_INDIVIDUAL_CHANGES = '''//ul[1]/li[1]//span[@class="mw-changeslist-line-inner"]//bdi[text(='CoolAssist']'''
    EDIT_FILTER_DESCRIPTION_INPUT = '//input[@name="wpFilterDescription"]'
    EDIT_FILTER_CONDITIONS_INPUT = '//div[@class="ace_cursor"]'
    EDIT_FILTER_NOTES_INPUT = '//textarea[@name="wpFilterNotes"]'
    EDIT_FILTER_DISABLED_CHECKBOX = '//input[@name="wpFilterEnabled"]/parent::span/span'
    EDIT_FILTER_DELETED_CHECKBOX = '//input[@name="wpFilterDeleted"]/parent::span/span'
    EDIT_FILTER_MEASURES_AFTER_WARNING_CHECKBOX = '//input[@name="wpFilterActionWarn"]/parent::span/span'
    EDIT_FILTER_OTHER_MESSAGE_INPUT = '//input[@name="wpFilterWarnMessageOther"]'
    EDIT_FILTER_CONFIRM_BUTTON = '//input[@value="Сохранить фильтр (если вы знаете, что делаете"]'
    SUCCESS_EDIT_FILTER_TEXT = '''//p[text(='Вы успешно сохранили изменения в ']'''
    MOVE_TO_CREATED_FILTER = '''//p[text(='Вы успешно сохранили изменения в ']/a'''
    CURRENT_FILTER_ID = '//tr[1]/td[@class="mw-input"]'


class RuDataLocators:

    CREATE_ELEMENT_TITLE_FIELD = '//input[@name="label"]'  # Поле "Название" в форме создания элемента
    CREATE_ELEMENT_DESCRIPTION_FIELD = '//input[@name="description"]'  # Поле "Описание" в форме создания элемента
    CREATE_ELEMENT_ALIAS_FIELD = '//input[@name="aliases"]'  # Поле "Псевдонимы" в форме создания элемента
    CONFIRM_CREATE_ELEMENT_BUTTON = '//button[@type="submit"]'  # Кнопка "Создать" в форме создания элемента
    ELEMENT_TITLE = '//h1/span/span[1]'  # Название элемента
    ELEMENT_ID = '//h1/span/span[2]'  # ID элемента
    ADD_STATEMENT = '//a[@title="Добавить новое утверждение"]'  # Кнопка "Добавить новое уверждение"
    PROPERTY_FIELD = '//div[@class="listview-item wikibase-statementgroupview wb-new"][1]//div[@class="wikibase-snakview-property"]/input'
    SELECT_FIRST_RECORD_FROM_PROPERTY_FIELD = '//ul[@dir="ltr"][2]/li[1]/a'
    FIRST_STATEMENT_FIELD = '//div[3]/div[@class="wikibase-listview"]/div[1]//textarea'
    SELECT_FIRST_RECORD_FROM_STATEMENT = '//ul[@dir="ltr"][3]/li[1]/a'
    FIRST_QUALIFIER_BUTTON = '''//div[1]/div[@class="wikibase-statementlistview"]//a[text(='квалификатор']'''
    QUALIFIER_CATEGORY_INPUT = '//div[@class="listview-item wikibase-statementgroupview wb-new"][1]//div[@class="listview-item wikibase-snakview wb-edit"]//input[@placeholder="свойство"]'
    SELECT_FIRST_CATEGORY_FROM_QUALIFIER = '//ul[@dir="ltr"][4]/li[1]/a'
    QUALIFIER_FIRST_STATEMENT_VALUE = '//div[@class="listview-item wikibase-statementgroupview wb-new"][1]//div[@class="wikibase-statementview-qualifiers"]//textarea[@dir="ltr"]'
    SECOND_STATEMENT_FIELD = '//div[3]/div[@class="wikibase-listview"]/div[2]//textarea'
    SECOND_QUALIFIER_BUTTON = '''//div[2]/div[@class="wikibase-statementlistview"]//a[text(='квалификатор']'''
    QUALIFIER_SECOND_STATEMENT_VALUE = '//div[@class="listview-item wikibase-statementgroupview wb-new"][2]//div[@class="wikibase-statementview-qualifiers"]//textarea[@dir="ltr"]'
    THIRD_STATEMENT_FIELD = '//div[3]/div[@class="wikibase-listview"]/div[3]//textarea'
    THIRD_QUALIFIER_BUTTON = '''//div[3]/div[@class="wikibase-statementlistview"]//a[text(='квалификатор']'''
    QUALIFIER_THIRD_STATEMENT_VALUE = '//div[@class="listview-item wikibase-statementgroupview wb-new"]//div[@class="wikibase-statementview-mainsnak-container"]//div[@class="wikibase-listview"]//textarea'
    FORTH_STATEMENT_FIELD = '//div[@class="listview-item wikibase-statementgroupview wb-new"]//textarea'
    FORTH_QUALIFIER_BUTTON = '''//div[4]/div[@class="wikibase-statementlistview"]//a[text(='квалификатор']'''
    FORTH_QUALIFIER_CATEGORY_INPUT = '//div[@class="listview-item wikibase-statementgroupview wb-new"][4]//div[@class="listview-item wikibase-snakview wb-edit"]//input[@placeholder="свойство"]'
    QUALIFIER_FORTH_STATEMENT_VALUE = '//div[@class="listview-item wikibase-statementgroupview wb-new"][4]//div[@class="wikibase-statementview-qualifiers"]//textarea[@dir="ltr"]'
    SPECIAL_CASE_STATEMENT = '''//h2[text(='Утверждения']/parent::div/div[3]/div/div//a[text(='это частный случай понятия']'''
    SUBCLASS_STATEMENT = '''//h2[text(='Утверждения']/parent::div/div[3]/div/div//a[text(='подкласс от']'''
    IMAGE_STATEMENT = '''//h2[text(='Утверждения']/parent::div/div[3]/div/div//a[text(='изображение']'''
    SAVE_STATEMENT_BUTTON = '''//a[text(='сохранить']'''
    EDIT_ELEMENT_BUTTON = '''//div[@class="wikibase-sitelinkgrouplistview"]//a[text(='править']'''
    LANGUAGE_WIKI_INPUT = '//input[@class="noime ui-suggester-input"]'
    PAGE_INPUT = '//input[@placeholder="страница"]'
    CONFIRM_EDIT_ELEMENT = '''//div[@class="wikibase-sitelinkgrouplistview"]//a[text(='сохранить']'''
    PAGE_LINK_FROM_ELEMENT = '//a[@hreflang="ru"]'


class RuCommonsLocators:

    FILE_TITLE = '''//h1/span[text(='Файл']'''
    FILE_TITLE_2 = '''//h1/span[text(='File']'''
    FILE_NAME_FROM_TITLE = '//h1/span[@class="mw-page-title-main"]'
    FILE_NAME_FROM_TITLE_2 = '//h1'
    COMMONS_LOGO = '//a[@class="mw-logo mw-logo-commons"]'


class ChangeMarksLocators:

    CREATE_MARK_TITLE_INPUT = '//input[@name="wpTag"]'
    CREATE_MARK_CAUSE_INPUT = '//input[@name="wpReason"]'
    CREATE_MARK_BUTTON = '//button[@value="Создать"]'
    CREATED_MARK = '''//tr/td/code[text(='markpr22']'''
    DISABLE_MARK_CAUSE_INPUT = '//input[@name="wpReason"]'
    CONFIRM_DISABLE_MARK_BUTTON = '//button[@value="Отключить"]'
    DELETE_MARK_CAUSE_INPUT = '//input[@name="wpReason"]'
    CONFIRM_DELETE_MARK = '//button[@value="Безвозвратно удалить эту метку"]'


class StabilizePageLocators:

    SELECT_STABLE_OR_LAST_VERSION_RADIOBUTTON = '//input[@id="default-stable"]'  # Радиобаттон 'Стабильная версия; если такой нет, то последняя версия'
    OTHER_REASON_INPUT = '//input[@name="wpReason"]'  # Поле 'Другая причина'
    PATROL_CURRENT_VERSION_CHECKBOX = '//label[@for="wpReviewthis"]'  # Чекбокс 'Отпатрулировать текущую версию'
    CONFIRM_STABILIZE_VERSION = '//input[@value="Подтвердить"]'
    TEXT_ABOUT_PAGE_STABILIZED = '''//ul[@class="mw-logevent-loglines"]/li[text(=' стабилизировал страницу '][1]'''


class LinkedPagesLocators:

    LINKED_PAGE_TITLE = '''//h1[contains(text(, 'Страницы, ссылающиеся на']'''


class ChecksLogLocators:

    CHECKS_LOG_TITLE = '''//h1[text(='Журнал проверки участников']'''
    USERNAME_INPUT = '//input[@name="cuSearch"]'  # Поле для ввода 'Цель'
    CONFIRM_CHECK_BUTTON = '//button[@type="submit"]'  # Кнопка 'Найти'


class CategoryPageLocators:

    MOVE_TO_FIRST_ARTICLE_FROM_CATEGORY_PAGE_LINK = '//div[@class="mw-category"]//ul/li[1]/a'
