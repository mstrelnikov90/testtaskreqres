import time
import allure
from typing import NoReturn
# from selenium.webdriver.common.action_chains import ActionChains
from .base_page import BasePage
from selenium.webdriver import Keys
from .locators import (SettingsLocators, TopBarLocators, NotificationsPageLocators, RecentChangesLocators,
                       SideBarLocators, ContributionLocators, MainPageLocators)


class TopBarElements(BasePage):
    # def __init__(self, browser) -> None:
    #     self.action = ActionChains(browser)
    #     super().__init__(browser)

    def __init__(self, page):
        super().__init__(page, "")

    @allure.step("Переход на страницу {link} в верхней панели")
    def move_to_links_from_top_bar(self, link):
        time.sleep(2)
        if link == 'Профиль':
            time.sleep(1)
            self.move_to_element_and_click(TopBarLocators.OPEN_PROFILE_ACTIONS_BUTTON)
            time.sleep(2)
        elif link == 'Страница участника':
            self.waiting_element_and_click(TopBarLocators.ACCOUNT_NAME)
        elif link == 'Заглавная страница':
            self.waiting_element_and_click(TopBarLocators.LOGO_LINK)
        elif link == 'Список уведомлений':
            time.sleep(10)
            self.move_to_element_and_click(TopBarLocators.NOTIFICATIONS)
            time.sleep(5)
        elif link == 'Создание УЗ':
            self.waiting_element_and_click(TopBarLocators.CREATE_ACCOUNT)
        elif link == 'Оповещения':
            time.sleep(10)
            self.waiting_element_and_click(TopBarLocators.ALERTS)
            time.sleep(5)
        elif link == 'Вход':
            self.waiting_element_and_click(TopBarLocators.SIGN_IN_BUTTON)
        elif link == 'Изменение темы':
            self.waiting_element_and_click(TopBarLocators.COLOR_THEME_BUTTON)
        time.sleep(2)
        self.create_screenshot()

    @allure.step("Переход на страницу {link} в верхней панели в теме 'Енисей'")
    def move_to_links_from_top_bar_yenisey(self, link):
        time.sleep(2)
        if link == 'Профиль':
            time.sleep(1)
            self.move_to_element_and_click(TopBarLocators.OPEN_PROFILE_ACTIONS_BUTTON_COMMONS)
            time.sleep(2)

    @allure.step("Выбор {theme} в окне смены цветографической темы")
    def change_color_theme(self, theme: str):
        if theme == 'Широкий формат':
            self.waiting_element_and_click(TopBarLocators.SELECT_WIDE_FORMAT_THEME)
        elif theme == 'Мелкий шрифт':
            self.waiting_element_and_click(TopBarLocators.SELECT_SMALL_FONT_THEME)
        elif theme == 'Крупный шрифт':
            self.waiting_element_and_click(TopBarLocators.SELECT_LARGE_FONT_THEME)
        elif theme == 'Бумажная тема':
            self.waiting_element_and_click(TopBarLocators.SELECT_PAPER_COLOR_THEME)
        elif theme == 'Темная тема':
            self.waiting_element_and_click(TopBarLocators.SELECT_DARK_COLOR_THEME)
        elif theme == 'Шрифт с засечками':
            self.waiting_element_and_click(TopBarLocators.SERIF_DARK_COLOR_THEME)
        self.create_screenshot()

    @allure.step("Переход к странице '{action}' через список пользовательских действий в верхней панели")
    def move_to_pages_from_opened_profile_actions(self, action: str):
        if action == 'Настройки':
            self.waiting_element_and_click(TopBarLocators.SETTINGS)
            time.sleep(2)
            if not self.is_element_present(SettingsLocators.SETTINGS_TITLE):
                self.waiting_element_and_click(TopBarLocators.OPEN_PROFILE_ACTIONS_BUTTON)
                time.sleep(2)
                self.waiting_element_and_click(TopBarLocators.SETTINGS)
                time.sleep(2)
        elif action == 'Личный кабинет':
            self.waiting_element_and_click(TopBarLocators.PERSONAL_DATA)
        elif action == 'Логаут':
            self.waiting_element_and_click(TopBarLocators.LOGOUT_BUTTON)
            time.sleep(2)
        elif action == 'Вклад':
            self.waiting_element_and_click(TopBarLocators.CONTRIBUTION)
            time.sleep(2)
            if not self.is_element_present(ContributionLocators.CONTRIBUTION_TITLE):
                self.waiting_element_and_click(TopBarLocators.OPEN_PROFILE_ACTIONS_BUTTON)
                time.sleep(2)
                self.waiting_element_and_click(TopBarLocators.CONTRIBUTION)
        elif action == 'Список наблюдения':
            self.waiting_element_and_click(TopBarLocators.WATCHLIST)
            time.sleep(7)
        self.create_screenshot()

    @allure.step("Поиск статьи")
    def search_article(self, text: str, type_of_search=None) -> NoReturn:
        time.sleep(3)
        if type_of_search == "quotes":
            self.element_send_keys(TopBarLocators.SEARCH_ARTICLE_INPUT, f'"{text}"')
        elif type_of_search == "tilda":
            self.element_send_keys(TopBarLocators.SEARCH_ARTICLE_INPUT, f'~{text}')
        elif type_of_search is None:
            self.element_send_keys(TopBarLocators.SEARCH_ARTICLE_INPUT, text)
        time.sleep(3)
        self.action.send_keys(Keys.ENTER).perform()
        time.sleep(5)
        self.create_screenshot()

    @allure.step("Переход ко всем уведомлениям")
    def move_to_all_notifications(self):
        if not self.is_element_present(NotificationsPageLocators.NOTIFICATION_PAGE_TITLE):
            self.waiting_element_and_click(TopBarLocators.ALL_NOTIFICATIONS)
        time.sleep(2)
        self.create_screenshot()

    @allure.step("Переход ко всем оповещениям")
    def move_to_all_alerts(self):
        if not self.is_element_present(NotificationsPageLocators.NOTIFICATION_PAGE_TITLE):
            self.waiting_element_and_click(TopBarLocators.ALL_ALERTS)
        time.sleep(2)
        self.create_screenshot()

    @allure.step("Указать первое уведомление, как непрочитанное")
    def select_notifications_as_unread(self):
        time.sleep(2)
        self.waiting_element_and_click(TopBarLocators.SELECT_AS_UNREAD)
        self.waiting_element_and_click(TopBarLocators.NOTIFICATIONS)
        self.create_screenshot()

    @allure.step("Указать все уведомления, как прочитанные")
    def select_notifications_as_read(self):
        self.waiting_element_and_click(TopBarLocators.SELECT_ALL_AS_READ)
        self.waiting_element_and_click(TopBarLocators.NOTIFICATIONS)
        time.sleep(2)
        self.create_screenshot()

    @allure.step("Переход к настройкам через уведомления в верхней панели")
    def move_to_settings_from_notifications_top_bar(self):
        time.sleep(2)
        self.waiting_element_and_click(TopBarLocators.MOVE_TO_NOTIFICATIONS_SETTINGS)
        self.create_screenshot()

    def assert_that_count_unread_notifications_increased(self):
        self.assert_that_element_not_present(TopBarLocators.COUNT_0_UNREAD_NOTIFICATIONS, "Счетчик количества непрочитанных (0 непрочитанное)")

    def assert_that_count_unread_notifications_is_null(self):
        if not self.is_element_present(TopBarLocators.COUNT_0_UNREAD_NOTIFICATIONS):
            self.reload()
            time.sleep(2)
        self.assert_that_element_present(TopBarLocators.COUNT_0_UNREAD_NOTIFICATIONS, "Счетчик количества непрочитанных (0 непрочитанное)")

    def assert_that_notification_about_gratitude_present(self):
        if not self.is_element_present(TopBarLocators.NOTIFICATION_ABOUT_GRATITUDE):
            time.sleep(2)
        self.assert_that_element_present(TopBarLocators.NOTIFICATION_ABOUT_GRATITUDE, "Уведомление о благодарности")

    def assert_that_notification_about_create_talk_present(self):
        """ Проверка, что уведомление о создании обсуждения отображается """
        self.assert_that_element_present(TopBarLocators.NOTIFICATION_ABOUT_EDIT_TALK_MEMBER_PAGE, "Уведомление об изменении страницы обсуждения")

    def assert_that_color_theme_present(self, theme: str):
        """ Проверка, что выбранная цветографическая тема отображается """
        if theme == 'Широкий формат':
            self.assert_that_element_present(MainPageLocators.WIDE_FORMAT_PAGE, "Выбранная тема 'Широкий формат'")
        elif theme == 'Мелкий шрифт':
            self.assert_that_element_present(MainPageLocators.SMALL_FONT_PAGE, "Выбранная тема 'Мелкий шрифт'")
        elif theme == 'Крупный шрифт':
            self.assert_that_element_present(MainPageLocators.LARGE_FONT_PAGE, "Выбранная тема 'Крупный шрифт'")
        elif theme == 'Бумажная тема':
            self.assert_that_element_present(MainPageLocators.PAPER_COLOR_THEME, "Выбранная тема 'Бумажная тема'")
        elif theme == 'Темная тема':
            self.assert_that_element_present(MainPageLocators.DARK_COLOR_THEME, "Выбранная тема 'Темная тема'")
        elif theme == 'Шрифт с засечками':
            self.assert_that_element_present(MainPageLocators.SERIF_FONT_PAGE, "Выбранная тема 'Шрифт с засечками'")


class SideBarElements(BasePage):
    # def __init__(self, browser) -> None:
    #     self.action = ActionChains(browser)
    #     super().__init__(browser)

    def __init__(self, page):
        super().__init__(page, "")

    @allure.step("Открыть левое боковое меню под авторизованным пользователем")
    def open_left_bar_with_authorization(self):
        self.waiting_element_and_click(SideBarLocators.OPEN_LEFT_SIDE_BAR_WITH_AUTHORIZATION)
        self.create_screenshot()

    @allure.step("Открыть левое боковое меню под авторизованным пользователем в теме 'Енисей'")
    def open_left_bar_with_authorization_yenisey(self):
        self.waiting_element_and_click(SideBarLocators.OPEN_LEFT_SIDE_BAR_WITH_AUTHORIZATION_DATA)
        self.create_screenshot()

    @allure.step("Переход на случайную страницу")
    def move_to_random_article(self):
        url = "https://ru.crw083.ru"
        self.page.goto(f"{url}/wiki/Служебная:Случайная_страница")
        # self.browser.get(f"{url}/wiki/Служебная:Случайная_страница")
        # self.create_attachment(self.browser.current_url, 'URL')
        self.create_attachment(self.page.url, 'URL')
        self.create_screenshot()

    @allure.step("Переход на страницу '{link}' через левую боковую панель")
    def move_to_left_bar_links(self, link: str) -> NoReturn:
        if link == 'Содержание':
            self.waiting_element_and_click(SideBarLocators.CONTENT_ARTICLE)
        elif link == 'Свежие правки':
            self.waiting_element_and_click(SideBarLocators.FRESH_EDIT_ARTICLE)
            time.sleep(5)
            if self.is_element_present(RecentChangesLocators.WELCOME_POPUP_AFTER_MOVE_TO_RECENT_CHANGES):
                self.waiting_element_and_click(RecentChangesLocators.CLOSE_POPUP_AFTER_MOVE_TO_RECENT_CHANGES)
        elif link == 'Новые страницы':
            self.waiting_element_and_click(SideBarLocators.NEW_PAGES_ARTICLE)
        elif link == 'Справка':
            self.scroll_to_element(SideBarLocators.REFERENCE_ARTICLE)
            self.waiting_element_and_click(SideBarLocators.REFERENCE_ARTICLE)
            time.sleep(3)
        elif link == 'Заглавная страница':
            self.waiting_element_and_click(SideBarLocators.MAIN_PAGE_ARTICLE)
            time.sleep(1)
        elif link == 'Создать новый элемент':
            self.waiting_element_and_click(SideBarLocators.CREATE_NEW_ELEMENT)
            time.sleep(1)
        self.create_screenshot()

    @allure.step("Переход на страницу '{link}' через правую боковую панель")
    def move_to_right_bar_links(self, link: str) -> NoReturn:
        self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
        time.sleep(2)
        if link == 'Служебные страницы':
            self.scroll_to_element(SideBarLocators.SERVICE_PAGES_ARTICLE)
            self.waiting_element_and_click(SideBarLocators.SERVICE_PAGES_ARTICLE)
        elif link == 'Удаление страницы':
            self.waiting_element_and_click(SideBarLocators.DELETE_ARTICLE)
            time.sleep(2)
        elif link == 'Восстановление страницы':
            if self.is_element_present(SideBarLocators.UNDELETE_ARTICLE):
                self.waiting_element_and_click(SideBarLocators.UNDELETE_ARTICLE)
            else:
                self.reload()
                time.sleep(2)
        elif link == 'Загрузить файл':
            self.waiting_element_and_click(SideBarLocators.UPLOAD_FILE_ARTICLE)
        elif link == 'Переименовать':
            if not self.is_element_present(SideBarLocators.RENAME_ARTICLE):
                self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
            self.waiting_element_and_click(SideBarLocators.RENAME_ARTICLE)
        elif link == 'Изменить защиту':
            self.waiting_element_and_click(SideBarLocators.CHANGE_PROTECT_SETTINGS)
        elif link == 'Стабилизировать':
            if not self.is_element_present(SideBarLocators.STABILIZE_ARTICLE):
                self.waiting_element_and_click(SideBarLocators.INSTRUMENTS)
            self.waiting_element_and_click(SideBarLocators.STABILIZE_ARTICLE)
        elif link == 'Защитить':
            self.waiting_element_and_click(SideBarLocators.PROTECT_ARTICLE)
        elif link == 'Элемент РУВИКИ.Даннных':
            self.scroll_to_element(SideBarLocators.RU_DATA_ELEMENT)
            self.waiting_element_and_click(SideBarLocators.RU_DATA_ELEMENT)
            time.sleep(1)
        self.create_screenshot()

    @allure.step("Переход на страницу '{link}' через правую боковую панель в теме 'Енисей'")
    def move_to_right_bar_links_yenisey(self, link: str) -> NoReturn:
        if link == 'Удаление страницы':
            self.waiting_element_and_click(SideBarLocators.DELETE_ARTICLE_DATA)
            time.sleep(2)
        elif link == 'Восстановление страницы':
            self.waiting_element_and_click(SideBarLocators.UNDELETE_ARTICLE_DATA)
            time.sleep(2)
        elif link == 'Переименовать':
            self.waiting_element_and_click(SideBarLocators.RENAME_ARTICLE_DATA)
        self.create_screenshot()

    @allure.step("Переход на страницу '{link}' через правую боковую панель с помощью горячих клави")
    def move_to_right_bar_links_with_hot_keys(self, link: str) -> NoReturn:
        if link == 'Переименовать':
            self.action.key_down(Keys.ALT).key_down(Keys.SHIFT).key_down('M').key_up(Keys.ALT).key_up(Keys.SHIFT).key_up('M').perform()
        self.create_screenshot()
