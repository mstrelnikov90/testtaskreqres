#FROM python:3.11.0-alpine
#
#ENV PIP_ROOT_USER_ACTION=ignore
#
#COPY ./requirements.txt ./wiki/autotests/requirements.txt
#
#WORKDIR ./wiki/autotests
#
#RUN pip3 install --upgrade pip
#
#RUN pip3 install -r requirements.txt
#
#RUN apk upgrade --no-cache
#
#RUN apk add gcc xvfb chromium chromium-chromedriver curl bash

FROM python:3.11.0

ENV PIP_ROOT_USER_ACTION=ignore

RUN apt-get update

RUN apt-get install -y curl gcc

RUN apt-get install -y xvfb

RUN apt-get install -y bash

RUN apt-get install -y chromium

RUN apt-get install -y chromium-driver

COPY ./requirements.txt ./dinero/autotests/requirements.txt

WORKDIR ./dinero/autotests

RUN pip3 install --upgrade pip

RUN pip3 install -r requirements.txt

RUN playwright install chromium

RUN playwright install-deps
